<?php
session_start();
//Define Charset UTF-8
header("Content-Type: text/html; charset=UTF-8",true);
//define('PATH_ROOT', dirname(__FILE__)."/");
require_once 'system/constants.php';

require_once(PATH_SYSTEM.'system.php');
require_once (PATH_SYSTEM.'controller.php');
require_once (PATH_SYSTEM.'model.php');

require_once(PATH_SYSTEM.'class.phpmailer.php');

function __autoload($file){
    if(file_exists(MODELS.$file.'.php')){
                require_once(MODELS . $file .'.php');
    }
    else if(file_exists(HELPERS.$file.'.php')){
        require_once(HELPERS.$file.'.php');
    }
    else{
        die("model ou helper não encontrado");
    }
 }
    
  $start = new System;
  $start->run();
