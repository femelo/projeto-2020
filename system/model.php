<?php

class Model{
    protected $db;
    public $_tabela;
    public $_tabela2;
    public function __construct(){
        $this->db = new PDO('mysql:host=localhost;dbname=fs2di674_projeto2020','fs2di674_geralus','femelo1981');
       
    }
    public function insert(Array $dados){
        
             $campos = implode(", ", array_keys($dados));
             $valores = "'".implode("', '", array_values($dados))."'";
           
            $res =  $this->db->query("INSERT INTO {$this->_tabela} ({$campos}) VALUES({$valores})");
            if($res->rowCount()==0){
                $valor = "0";
            }else{
                $valor = $this->db->lastInsertId().",".$res->rowCount();
            }
            return $valor;
    }
    public function read($where = null, $limit = null, $offset = null, $orderby = null){
    
        $where = ($where != null ? "WHERE {$where}" : "");
        $limit = ($limit != null ? "LIMIT {$limit}" : "");
        $offset = ($offset != null ? "OFFSET {$offset}" : "");
        $orderby = ($orderby != null ? "ORDER BY {$orderby}" : "");
        $q = $this->db->query("SELECT * FROM {$this->_tabela} {$where} {$orderby} {$limit} {$offset} ");
        $q->setFetchMode(PDO::FETCH_ASSOC);
        return $q->fetchAll();
    }
    public function update(Array $dados, $where){
        foreach ($dados as $inds => $vals){
            $campos[] = "{$inds} = '{$vals}'";
        }
          $campos = implode(", ", $campos);
            $res =  $this->db->query("UPDATE {$this->_tabela} SET {$campos} WHERE {$where}"); 
            return $res->rowCount();
        
    }
    public function delete($where){
        return $this->db->query("DELETE FROM {$this->_tabela} WHERE {$where}");
    }
    
    public function deleteSeguro(Array $dados){
        $this->db->beginTransaction();
        $i=0;
        $sql=array();
        $erro=0;
        foreach($dados as $tabela => $coluna){
            $sql[$i] = $this->db->query("DELETE FROM {$tabela} WHERE {$coluna}");
            if(!$sql[$i]){
                $erro++;                
            }           
            $i++;
        }
        if($erro == 0){
                $this->db->commit();
                $mensagem=1;
        }else{
            $this->db->rollBack();
            $mensagem=0;
        }
        return $mensagem;
    }
}
