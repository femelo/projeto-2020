<?php
class Index extends Controller{
    public $conteudo;
    public function index_action(){
      $projetos = new ProjetoModel();  
      $this->conteudo["titulo"] = "home";
      $lista_projetos = $projetos->listaProjetosTipo();
      $banner = $projetos->listaProjetosTipoDestaque();
      $this->conteudo['projetos'] = $banner;
      $this->conteudo['projeto'] = $lista_projetos;
      $listaportipo = $projetos->listaByTipo("tipo = 1 and ativo = 1", "id");
      $this->conteudo['tipoinicio'] = $listaportipo;
      $this->view("index",$this->conteudo);
    }
}

