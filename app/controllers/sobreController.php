<?php

class Sobre extends Controller{
     public $conteudo;
     
     public function init(){
        
         $this->conteudo = $this->getParam();
         
     }
    public function index_action(){    
                           
        $this->conteudo["titulo"] = "Sobre A Big";
        $this->conteudo["meta"] = "";
        $this->view("sobre",$this->conteudo);
    }
    public function politica(){                               
        $this->conteudo["titulo"] = " - Politica de Privacidade";
        $this->conteudo["meta"] = "";
        $this->view("politica",$this->conteudo);
    }
    public function certificacoes(){                               
        $this->conteudo["titulo"] = " - Certificações";
        $this->conteudo["meta"] = "";
        $this->view("certificacoes",$this->conteudo);
    }
    public function qualidade(){                               
        $this->conteudo["titulo"] = " - Política de Qualidade Big";
        $this->conteudo["meta"] = "";
        $this->view("politica-qualidade-big",$this->conteudo);
    }
   
}

