<?php
class Admin extends Controller{
    private $auth, $db, $secao;
    
    public function init(){
        $this->auth = new authHelper();
        $this->auth->setLoginControllerAction('admin', 'login')
                ->checkLogin('redirect');        
        $this->db = new AdminModel();
        $this->secao = $this->getParam('secao'); 
        
    }
    
    public function index_action(){
        $redirect = new RedirectorHelper();
        $redirect->setUrlParameter('secao', 'projetos')
                ->goToAction(gerenciar);
    }
    
    public function login(){
        if($this->getParam('acao')){
          $valor=  $this->auth->setTableName('admin')
                    ->setUserColumn('user')
                    ->setPassColumn('senha')
                    ->setUser($_POST['login'])
                    ->setPass(md5($_POST['senha']))
                    ->setLoginController('admin')
                    ->login();
            $conteudo['erro']= $valor;
            $this->view("painel/adminForm",$conteudo);
        }else{      
        $this->view("painel/adminForm");
        }
    }
    
    public function logout(){
        $this->auth->setLogoutControllerAction("admin", "login")
                ->logout();
        
    }
    
    public function gerenciar(){
            
        if($this->secao == 'projetos'){          
            $this->db->_tabela = $this->secao;
            $sql = $this->db->listaDados('id');
                   
            $conteudo['sql']=$sql; 
            $this->view("painel/adminGerenciar",$conteudo);
        }
        else if($this->secao == 'tipo'){
            $this->db->_tabela = "tipo_projeto";
            $sql = $this->db->listaDados('tipoprj_id');
            $conteudo['sql'] = $sql;
            $this->view("painel/GerenciarTipo",$conteudo);
            
        }
        else if($this->secao == 'lazer'){
             $this->db->_tabela = "area_lazer";
             $sql = $this->db->listaDados('lazer_id');
             $conteudo['sql'] = $sql;
            $this->view("painel/GerenciarLazer",$conteudo);
        }
        else if($this->secao == 'paleta'){
            $this->db->_tabela = "dados_paleta";
            $sql = $this->db->listaDados('paleta_id');
            $conteudo['sql'] = $sql;
            $this->view("painel/GerenciarPaleta",$conteudo);
        }
        else if($this->secao == 'ficha'){
              $id = $this->getParam("c");
             $conteudo['ficha'] = $this->db->listaFichaByID($id);
            $this->view("painel/GerenciarFicha",$conteudo);
        }
        else if($this->secao == 'imagens'){
            $id = $this->getParam("c");
            $conteudo['data'] = $this->db->listaDataById($id);
            $conteudo['imagens'] = $this->db->listaImagensById($id);
            $this->view("painel/GerenciarImg",$conteudo);
        } 
        else if($this->secao == "modulo"){
            $this->db->_tabela = "modulos_projeto";
            $sql = $this->db->listaModulos();
            $conteudo['sql']=$sql; 
            $this->view("painel/GerenciarModulo",$conteudo);
        } 
        
    }
    
    public function cadastrar(){
         
        if($this->secao == "projetos"){
            $conteudo['acao_form'] ='Cadastrar';
            $this->db->_tabela = "tipo_projeto";
            $sql = $this->db->listaDados('tipoprj_id');
            $conteudo['tipo']=$sql;
            $conteudo['estagios'] = $this->db->listaEstagios('estagios_id');
            $conteudo['lazer'] = $this->db->listaLazer('lazer_id');
            $conteudo['paleta'] = $this->db->listaPaleta("paleta_id");
            if($this->getParam('do')){
                $this->db->_tabela = "projetos";
                function date_converter($_date = null) {
                    $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
                    if ($_date != null && preg_match($format, $_date, $partes)) {
                        return $partes[3].'-'.$partes[2].'-'.$partes[1];
                    }
                    return false;
                }
                $cp1 = addslashes($_POST['titulo']);
                $cp2 = addslashes($_POST['subtitulo']);
                $cp3 = $_POST['tipo'];
                $cp4 = $_POST['dormitorio'];
                $cp5 = $_POST['area'];
                $cp6 = $_POST['valor'];
                $cp7 = $_POST['endereco'];
                $cp8 = addslashes($_POST['titulo']).",".addslashes($_POST['subtitulo']).",".$_POST["nomeslazer"].$_POST['tags'];
                $cp9 = $_POST['lazer'];
                $cp10 = date_converter($_POST['inicio']);
                $cp11 = date_converter($_POST['entrega']);
                $cp12 = $_POST["descricao"];
                $cp13 = $_POST["apresentacao"];
                $cp14 = $_POST['estagio']; 
                $cp15 = $_POST['paleta'];
                $cp16 = $_POST['flag'];
                $cp17 = $_POST['showroom'];
                $cp18 = $_POST['tourvirtual'];
                $cp19 = $_POST['fase'];
                $inserido =  $this->db->insereProjetos($cp1, $cp2, $cp3, $cp4, $cp5, $cp6, $cp7, $cp8, $cp9, $cp10, $cp11, $cp12, $cp13, $cp14,$cp15,$cp16,$cp17,$cp18,$cp19);
                $dado = $inserido;
                if($inserido != "0"){
                    $inserido = explode(",",$inserido);
                    if($cp19 == 1){
                        $this->db->insereStatus($inserido[0],0);
                    }
                    
                    $this->db->insereMapa($inserido[0]);
                }
                echo $dado.$inserido[0];
            }
            $this->view('painel/cad_projetos',$conteudo);   
        }
        else if($this->secao == "tipo"){
            $conteudo['acao_form'] ='Cadastrar';                           
            if($this->getParam('do')){                      
                $this->db->_tabela = "tipo_projeto";
                $inserido =  $this->db->insereTipoProjeto($_POST['nometipo']);
                $conteudo['mensagem'] = $inserido;
                $conteudo['page']= "cadastrar/secao/".$this->secao;
                $this->view('painel/result',$conteudo);
            }
            $this->view('painel/cad_tipo',$conteudo);
        }
        else if($this->secao =="lazer"){
            $conteudo['acao_form'] ='Cadastrar';
             
            if($this->getParam('do')){
                $this->db->_tabela = "area_lazer";
                if(isset($_FILES["imgarea"])){
                    $foto = $_FILES["imgarea"];
                    preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
 
                    // Gera um nome único para a imagem
                    $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
              
                    move_uploaded_file($_FILES["imgarea"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/ico_lazer/".$nome_imagem);
                }
                $inserido = $this->db->insereAreaLazer(addslashes($_POST['tituloarea']), $nome_imagem);
                
                $conteudo['mensagem'] = $inserido;
                $conteudo['page']= "cadastrar/secao/".$this->secao;
                $this->view('painel/result',$conteudo);
            }
            $this->view('painel/cad_lazer',$conteudo);
        }
        else if($this->secao =="ficha"){
            $conteudo['acao_form'] ='Cadastrar';
            $this->db->_tabela = "projetos";
            $sql = $this->db->listaDados("id");
            $conteudo['content']= $sql;
            if($this->getParam('do')){
                $this->db->_tabela = "itens_ficha";
                $inserido = $this->db->insereFicha($_POST['nome'], $_POST['valor'], $_POST['codigo']);
                echo $inserido;
            }
            $this->view('painel/cad_ficha',$conteudo);
        }
        else if($this->secao =="imagens"){
            $conteudo['acao_form'] ='Cadastrar';
            $conteudo['tipoimg'] = $this->db->listaTipoImg('tipoimg_id');
            if($this->getParam('do')){
                $this->db->_tabela = "imagens_prj";
                $pasta;
                switch($_POST['sel_tipoimg']){
                    case 1:$pasta = "perspectivas";
                    break;
                    case 2:$pasta = "plantas";
                    break;
                    case 3:$pasta = "decorado";
                    break;
                    case 4:$pasta = "implantacao";
                    break;
                    case 5:$pasta = "obras";
                    break;
                }
                if(isset($_FILES["img_adicional"])){
                    $foto = $_FILES["img_adicional"];
                    preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
                    
                    // Gera um nome único para a imagem
                    $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
                    move_uploaded_file($_FILES["img_adicional"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/".$pasta."/".$nome_imagem);
                }
                function montadata($data){
                    $data= explode('/',$data);
                    return $data[1]."-".$data[0]."-01";
                }

                (isset($_POST['legenda'])?$legenda=$_POST['legenda']:$legenda="");

                (!isset($_POST['data']))?$datas=null:$datas = montadata($_POST['data']); 

                $inserido = $this->db->insereImgAdicional($pasta."/".$nome_imagem,$legenda,$_POST['sel_tipoimg'],$datas,$_POST['sel_projeto'],$_POST['sel_modulo']);

                $conteudo['mensagem'] = $inserido;
                $conteudo['page']="cadastrar/secao/".$this->secao."/c/".$_POST['sel_projeto']."/t/".$_POST['sel_tipoimg']."/m/".$_POST['sel_modulo'];
                $this->view('painel/result',$conteudo);
            }
            else if($this->getParam('c')){
                $tipo = $this->getParam('t');
                $modulo = $this->getParam('m');
                $projeto = $this->getParam('c');
                $this->db->_tabela = "projetos";
                $sql = $this->db->dadosAtuais("id=".$projeto);
                $sql2 = $this->db->listaTipoImgById($tipo);
                $conteudo['tipo'] = $sql2;
                $conteudo['modulo'] = $modulo;
                $conteudo['projeto'] = $sql;
                $this->view('painel/cad_imagens3',$conteudo);
            }
            else if($this->getParam('t')){
                $this->db->_tabela = "projetos";
                $tipo = $this->getParam('t');
                if($tipo == 5){
                    $sqlmodulo = $this->db->listaProjetosJoinModulos();
                    $conteudo['content'] = $sqlmodulo;
                    $conteudo['tipo'] = $tipo;
                    $this->view('painel/cad_imagens2',$conteudo);
                }else{
                    $this->db->_tabela = "projetos";
                    $sql2 = $this->db->listaDados("id");
                    
                    $conteudo['content'] = $sql2;
                    $conteudo['tipo'] = $tipo;
                    $this->view('painel/cad_imagens2',$conteudo);
                } 
            }
            
            $this->view('painel/cad_imagens1',$conteudo); 
        }
        else if($this->secao =="paleta"){
            $conteudo['acao_form'] ='Cadastrar';
             
            if($this->getParam('do')){
                $this->db->_tabela = "dados_paleta";
                if(isset($_FILES["imgpaleta"])){
                    $upload = new UploadHelper();
                    $upload->setFile($_FILES['imgpaleta'])
                            ->setPath(PATH_ROOT."web-files/img/paleta/")
                        ->upload();
                }
                $inserido = $this->db->inserePaleta(addslashes($_POST['nomecor']), $_FILES["imgpaleta"]["name"],$_POST['cortitulo'],$_POST['corsub'],$_POST['coractive'],$_POST['corheader'],$_POST['coricone'],$_POST['corfinanciamento'],$_POST['corbg']);
                
                $conteudo['mensagem'] = $inserido;
                $conteudo['page']= "cadastrar/secao/".$this->secao;
                $this->view('painel/result',$conteudo);
            }
            $this->view('painel/cad_paleta',$conteudo);
        }
        else if($this->secao =="modulo"){
            $conteudo['acao_form'] ='Cadastrar';
            $sql = $this->db->listaProjetosModulos();
            $conteudo['projetos']= $sql;
            if($this->getParam('do')){
                $this->db->_tabela = "modulos_projeto";
                function date_converter($_date = null) {
                    $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
                    if ($_date != null && preg_match($format, $_date, $partes)) {
                        return $partes[3].'-'.$partes[2].'-'.$partes[1];
                    }
                    return false;
                }
                $cp1 = addslashes($_POST['nome']);
                $cp2 = date_converter($_POST['inicio']);
                $cp3 = date_converter($_POST['entrega']);
                $cp4 = $_POST['codigo'];
                $inserido = $this->db->insereModulo($cp1,$cp2,$cp3,$cp4);
                $dado = $inserido;
                if($inserido != "0"){
                    $inserido = explode(",",$inserido);
                    $this->db->insereStatus($cp4,$inserido[0]);

                }
                echo $dado.$inserido[0];
            }
            $this->view('painel/cad_modulo',$conteudo);
        } 
    }
    
    public function deletar(){
        $id = $this->getParam("id");
        
        if($this->secao =="projetos"){
          
          $apaga = $this->db->apagaProjeto($id);
          if($apaga==0){
              $conteudo['mensagem'] = $apaga;
            $conteudo['page']= "gerenciar/secao/".$this->secao;
             $this->view('painel/result',$conteudo);
          }else{         
          $redirect= new RedirectorHelper();
          $redirect->setUrlParameter("secao", $this->secao)
                ->goToAction( "gerenciar");
          }
        }
        else if($this->secao =="tipo"){
          $this->db->_tabela = "tipo_projeto";
          $this->db->deletaDados("tipoprj_id=".$id);
          $redirect= new RedirectorHelper();
          $redirect->setUrlParameter("secao", $this->secao)
                ->goToAction( "gerenciar");
        }
        else if($this->secao =="lazer"){
          $this->db->_tabela = "area_lazer";
          $this->db->deletaDados("lazer_id=".$id);
          $redirect= new RedirectorHelper();
          $redirect->setUrlParameter("secao", $this->secao)
                ->goToAction( "gerenciar");
        }
        else if($this->secao =="paleta"){
          $this->db->_tabela = "dados_paleta";
          $this->db->deletaDados("paleta_id=".$id);
          $redirect= new RedirectorHelper();
          $redirect->setUrlParameter("secao", $this->secao)
                ->goToAction( "gerenciar");
        }
        else if($this->secao =="ficha"){
          $this->db->_tabela = "itens_ficha";
          $this->db->deletaDados("item_id=".$id);
          $redirect= new RedirectorHelper();
          $redirect->setUrlParameter("secao", $this->secao)
                ->goToAction( "gerenciar");
        }
        else if($this->secao =="imagens"){
            $proj = $this->getParam("p");
          $this->db->_tabela = "imagens_prj";
          $this->db->deletaDados("img_id=".$id);
          $redirect= new RedirectorHelper();
          $redirect->setUrlParameter("secao", $this->secao."/c/".$proj)
                ->goToAction( "gerenciar");
        }
               
    }
    
    public function editar(){
        $id = $this->getParam("id");
        $conteudo['acao_form'] ='Editar';
        if($this->secao == "projetos"){  
            $this->db->_tabela = $this->secao;            
            $sql = $this->db->listaTipo('tipoprj_id');
            $conteudo['tipo']=$sql;
            $conteudo['estagios'] = $this->db->listaEstagios('estagios_id');
            $conteudo['lazer'] = $this->db->listaLazer('lazer_id');
            $conteudo['paleta'] = $this->db->listaPaleta("paleta_id");
            $sql2 = $this->db->dadosAtuais("id=".$id);
            $conteudo['content']=$sql2;
            if($this->getParam('do')){
                $this->db->_tabela = "projetos";
                function date_converter($_date = null) {
                    $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
                    if ($_date != null && preg_match($format, $_date, $partes)) {
                        return $partes[3].'-'.$partes[2].'-'.$partes[1];
                    }
                    return false;
                }
                $cp0 = $_POST['codigo']; 
                $cp1 = addslashes($_POST['titulo']);
                $cp2 = addslashes($_POST['subtitulo']);
                $cp3 = $_POST['tipo'];
                $cp4 = $_POST['dormitorio'];
                $cp5 = $_POST['area'];
                $cp6 = $_POST['valor'];
                $cp7 = $_POST['endereco'];
                $cp8 = $_POST['tags'];
                $cp9 = $_POST['lazer'];
                $cp10 = date_converter($_POST['inicio']);
                $cp11 = date_converter($_POST['entrega']);
                $cp12 = $_POST["descricao"];
                $cp13 = $_POST["apresentacao"];
                $cp14 = $_POST['estagio'];                 
                $cp15 = $_POST['paleta'];
                $cp16 = $_POST['flag'];
                $cp17 = $_POST['showroom'];
                $cp18 = $_POST['tourvirtual'];
                // $cp19 = $_POST['fase'];
                $inserido =  $this->db->alteraProjetos($cp0,$cp1, $cp2, $cp3, $cp4, $cp5, $cp6, $cp7, $cp8, $cp9, $cp10, $cp11, $cp12, $cp13, $cp14,$cp15,$cp16,$cp17,$cp18);
                echo $inserido;
            }
            $this->view('painel/cad_projetos',$conteudo);   
        }
        else if($this->secao == "tipo"){
           
            $this->db->_tabela = "tipo_projeto";
            $sql = $this->db->dadosAtuais("tipoprj_id=".$id);
            $conteudo['content']=$sql;
            $this->view('painel/cad_tipo',$conteudo);
            if($this->getParam('do')){
                                  
                $inserido =  $this->db->alteraTipoProjeto($id,$_POST['nometipo']);
                echo $inserido;              
            }
        }
        else if($this->secao == "lazer"){
            $this->db->_tabela = "area_lazer";
            $sql = $this->db->dadosAtuais("lazer_id=".$id);
            $conteudo['content']=$sql;
            
            if($this->getParam('do')){
                if($_FILES["imgarea"]['size']!=0){
                    $foto = $_FILES["imgarea"];
                    preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
                    // Gera um nome único para a imagem
                    $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
                    move_uploaded_file($_FILES["imgarea"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/ico_lazer/".$nome_imagem);
                }  
                else{$nome_imagem="";}
                $inserido =  $this->db->alteraAreaLazer($id,addslashes($_POST['tituloarea']),$nome_imagem);
                $conteudo['mensagem'] = $inserido;
                $conteudo['page']="gerenciar/secao/".$this->secao;
                $this->view('painel/result',$conteudo);             
            }
            $this->view('painel/cad_lazer',$conteudo);
        }
        else if($this->secao == "paleta"){
            $this->db->_tabela = "dados_paleta";
            $sql = $this->db->dadosAtuais("paleta_id=".$id);
            $conteudo['content']=$sql;
            
            if($this->getParam('do')){
                if(isset($_FILES["imgpaleta"])){
                    $upload = new UploadHelper();
                    $upload->setFile($_FILES['imgpaleta'])
                            ->setPath(PATH_ROOT."web-files/img/paleta/")
                        ->upload();
                }                
                $inserido =  $this->db->alteraPaleta($id,$_POST['nomecor'], $_FILES["imgpaleta"]["name"],$_POST['cortitulo'],$_POST['corsub'],$_POST['coractive'],$_POST['corheader'],$_POST['coricone'],$_POST['corfinanciamento'],$_POST['corbg']);
                $conteudo['mensagem'] = $inserido;
                $conteudo['page']="gerenciar/secao/".$this->secao;
                $this->view('painel/result',$conteudo);             
            }
            $this->view('painel/cad_paleta',$conteudo);
        }
        else if($this->secao == "status"){
            $conteudo['content'] = $this->db->listaStatus();
            //$conteudo['modulos'] = $this->db->listaModulos();
            if($this->getParam('do')){
                $this->db->_tabela = $this->secao;
                $inserido = $this->db->alteraStatus($_POST['st1'], $_POST['st2'], $_POST['st3'], $_POST['st4'], $_POST['st5'],  $_POST['st6'],  $_POST['codigo']);
                echo $inserido;
            }
            $this->view('painel/edita_status',$conteudo); 
        }
        else if($this->secao == "ficha"){
            $this->db->_tabela = "itens_ficha";
            $sql = $this->db->dadosAtuais("item_id=".$id);
            $conteudo['content']=$sql;
            if($this->getParam('do')){
                $inserido = $this->db->alteraFicha($_POST['codigo'], $_POST['nome'], $_POST['valor']);
                echo $inserido;
            }
            $this->view('painel/edita_ficha',$conteudo);
        }
        else if($this->secao == "destaque"){
           $this->db->_tabela = "projetos";
           if($_FILES["destaque"]['size']!=0){
               $foto = $_FILES["destaque"];
               preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext); 
            // Gera um nome único para a imagem
               $nome_imagem_destaque = md5(uniqid(time())) . "." . $ext[1];
               move_uploaded_file($_FILES["destaque"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/imgprojeto/".$nome_imagem_destaque);
            }else{
                $nome_imagem_destaque="";
                
            }
           if($_FILES["thumb"]['size']!=0){
                 $foto = $_FILES["thumb"];
               preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext); 
            // Gera um nome único para a imagem
               $nome_imagem_thumb = md5(uniqid(time())) . "." . $ext[1];
               move_uploaded_file($_FILES["thumb"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/imgprojeto/".$nome_imagem_thumb);
            }else{
                $nome_imagem_thumb="";
                
            }
            if($_FILES["banner"]['size']!=0){
                    $foto = $_FILES["banner"];
               preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext); 
            // Gera um nome único para a imagem
               $nome_imagem_banner = md5(uniqid(time())) . "." . $ext[1];
               move_uploaded_file($_FILES["banner"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/imgprojeto/".$nome_imagem_banner);
            }else{
                $nome_imagem_banner="";
                
            }
            if($_FILES["bg"]['size']!=0){
                   $foto = $_FILES["bg"];
               preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext); 
            // Gera um nome único para a imagem
               $nome_imagem_bg = md5(uniqid(time())) . "." . $ext[1];
               move_uploaded_file($_FILES["bg"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/imgprojeto/".$nome_imagem_bg);
            }else{
                $nome_imagem_bg="";
                
            }
            $inserido = $this->db->alteraImagens($_POST['codigo'], $nome_imagem_destaque, $nome_imagem_thumb,$nome_imagem_banner,$nome_imagem_bg);
            $conteudo['mensagem'] = $inserido;
            $conteudo['page']="gerenciar/secao/projetos";
                $this->view('painel/result',$conteudo);
        }
        else if($this->secao == "imagens"){
            $this->db->_tabela = "imagens_prj";
            $proj = $this->getParam("p");
            $pasta;
                switch($_POST['tipo']){
                    case "1":$pasta = "perspectivas";
                        break;
                    case "2":$pasta = "plantas";
                        break;
                    case "3":$pasta = "decorado";
                        break;
                    case "4":$pasta = "implantacao";
                        break;
                    case "5":$pasta = "obras";
                        break;
                 }
            
            if($_FILES["img"]['size']!=0){
                $foto = $_FILES["img"];
                preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
 
            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
                move_uploaded_file($_FILES["img"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].PATH_ROOT."web-files/img/".$pasta."/".$nome_imagem);
                $caminho = $pasta."/".$nome_imagem;
            }else{
                $caminho="";
            }
            
            function montadata($data){
                $data= explode('/',$data);
                return $data[1]."-".$data[0]."-01";
            }
            if(isset($_POST['data'])){
                $dataImg = montadata($_POST["data"]);
            }else{$dataImg=null;}
            $inserido = $this->db->alteraImgAdicional($id, $caminho, $_POST['legenda'],$dataImg);
            $conteudo['mensagem'] = $inserido;
            $conteudo['page']="gerenciar/secao/".$this->secao."/c/".$proj;
            $this->view('painel/result',$conteudo);
        }
        else if($this->secao == "mapa"){
           $this->db->_tabela =  $this->secao;
           $sql = $this->db->dadosAtuais("projeto_id=".$id);
           $conteudo['content'] = $sql;
           if($this->getParam('do')){
               if(isset($_FILES["thumb"])){
                    $upload = new UploadHelper();
                    $upload->setFile($_FILES['thumb'])
                            ->setPath(PATH_ROOT."web-files/img/ico_mapa/")
                        ->upload();
                }
            $inserido = $this->db->alteraMapa($id, $_POST['latitude'], $_POST['longitude'], $_FILES['thumb']['name']);
            $conteudo['mensagem'] = $inserido;
            $conteudo['page']="editar/secao/mapa/id/".$_POST['codigo'];
                $this->view('painel/result',$conteudo);
           }
           $this->view('painel/edita_mapa',$conteudo);
        }
        else if($this->secao == "modulo"){
            $id = $this->getParam("id");
            $conteudo['acao_form'] ='Editar';
            $this->db->_tabela = "modulos_projeto";
            $sql = $this->db->dadosAtuais("id_modulo=".$id);
            $conteudo['content']=$sql;
            
            $this->view('painel/edita_modulo',$conteudo);
            if($this->getParam('do')){
                function date_converter($_date = null) {
                    $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
                    if ($_date != null && preg_match($format, $_date, $partes)) {
                        return $partes[3].'-'.$partes[2].'-'.$partes[1];
                    }
                    return false;
                } 
                $cp1 = addslashes($_POST['nome_modulo']);
                $cp2 = date_converter($_POST['inicio']);
                $cp3 = date_converter($_POST['entrega']);                
                $inserido =  $this->db->alteraModulo($cp1,$cp2,$cp3,$id);
                echo $inserido;              
            }   
        }
    }           
}