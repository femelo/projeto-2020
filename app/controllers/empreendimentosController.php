<?php

class Empreendimentos extends Controller{
     public $conteudo;
     
     public function init(){
        
         $this->conteudo = $this->getParam();
         
     }
    public function index_action(){    
            $projetos = new ProjetoModel();
            $projeto= $projetos->listaProjetosTipo2();
            $banner = $projetos->listaProjetosTipoDestaque();
            $this->conteudo["titulo"] = "Empreendimentos";
            $this->conteudo["meta"] = "";
            $this->conteudo['projeto'] = $projeto;
            $this->conteudo['projetos'] = $banner;
            $this->view("projetos",$this->conteudo);        
    }
    public function filtro(){
        $redirect = new RedirectorHelper();
        if(isset($_POST['valdorms']) || isset($_POST['valestagio'])){
            (isset($_POST['valdorms']))? $dorm = $_POST['valdorms']: $dorm ="";
            (isset($_POST['valestagio']))? $estagio = $_POST['valestagio']: $estagio ="";
            $projetos = new ProjetoModel();
            if($estagio==""){
                $result = $projetos->listaProjetosTipoLikeD($dorm);
                $conteudo['resultado'] = $result;
                $my = json_encode($conteudo['resultado']);  
            echo($my);
            }
            else if($dorm==""){
                $result = $projetos->listaProjetosTipoLikeE($estagio);
                 $conteudo['resultado'] = $result;
                $my = json_encode($conteudo['resultado']);  
            echo($my);
            }else{
                $result = $projetos->listaProjetosTipoLikeDE($dorm,$estagio);
                 $conteudo['resultado'] = $result;
                $my = json_encode($conteudo['resultado']);  
            echo($my);
            
        }
        }else{
            
            $redirect->goToController("empreendimentos");
            
        }
    }
    public function search(){
        if(isset($this->conteudo["val"])){
            $projetos = new ProjetoModel();
            $projeto= $projetos->listaProjetosTipoLike(utf8_encode($this->conteudo['val']));
            $this->conteudo["titulo"] = "Empreendimentos";
            $this->conteudo["meta"] = "";
            $this->conteudo['projeto'] = $projeto;
            $this->view("projetos",$this->conteudo); 
        }
    }
    public function imovel(){
        if(isset($this->conteudo["c"])){
            $id = $this->conteudo["c"];
            //$this->conteudo['nome']=$this->conteudo["nome"];
            $this->conteudo["titulo"] = "Empreendimentos";
            $projetos = new ProjetoModel();
            $lazer = new LazerModel();
            $fichas = new FichaModel();
            $imagens = new ImagensModel();
            $mapa = new MapaModel();
            $status = new StatusModel();
            $modulos = new ModulosModel();

            $projeto = $projetos->listaProjetosTipoByid($id);
            $listaLazer = $lazer->listaLazer("lazer_id");
            $itensFicha = $fichas->listaFichaById("id_projeto=".$id, "item_id");
            $listaModulos = $modulos->listaModulosById("projeto_id=".$id,"id_modulo");
            $galeria = $imagens->listaImagensGaleria("projeto_id=".$id." and img_tipo <> 5", "img_id");
            $obras = $imagens->listaImagensObra("projeto_id=".$id." and img_tipo = 5", "img_id");
            $map = $mapa->listaMapa("projeto_id=".$id, "mapa_id");
            $listaStatus = $status->listaStatus("projeto_id=".$id, "status_id");
            $dataObra = $imagens->listaDataObra($id);
            $this->conteudo["proj"]= $projeto;
            $this->conteudo['lazer'] = $listaLazer;
            $this->conteudo['ficha'] = $itensFicha;
            $this->conteudo['galeria'] = $galeria;
            $this->conteudo['img_obras'] = $obras;
            $this->conteudo['conf_map']=$map;
            $this->conteudo['info_status'] = $listaStatus;
            $this->conteudo['data_obra'] = $dataObra;
            $this->conteudo['conversao'] = $id;
            $this->conteudo['modulos'] = $listaModulos;
            $this->view("projetos_interna",$this->conteudo);
        }
        if(isset($this->conteudo["ajax"])){
            $imagens = new ImagensModel();
            $id = $_POST['id_projeto'];
            $modulo = $_POST['modulo_selecionado'];
            $dataObra = $imagens->listaDataObraModulo($id,$modulo);
            $conteudo['resultado'] = $dataObra;
            $my = json_encode($conteudo['resultado']);  
            echo($my);
        }
    }
   
}