<?php
class Mobile extends Controller{
    public $conteudo;
    public function init(){
        
         $this->conteudo = $this->getParam();
         
     }
    public function index_action(){
      $projetos = new ProjetoModel();  
      $this->conteudo["titulo"] = "home";
      $lista_projeto = $projetos->listaProjetosTipo();
      $this->conteudo['projetos'] = $lista_projeto;
      $listaportipo = $projetos->listaByTipo("tipo=1 and ativo = 1", "id");
      $this->conteudo['tipoinicio'] = $listaportipo;
      $this->view("mobile/mobile",$this->conteudo);
    }
    public function empreendimentos(){
      $projetos = new ProjetoModel();  
      $this->conteudo["titulo"] = "empreendimentos";
      $lista_projeto = $projetos->listaProjetosTipo();
      $this->conteudo['projetos'] = $lista_projeto;
      $listaportipo = $projetos->listaByTipo("tipo=1", "id");
      $this->conteudo['tipoinicio'] = $listaportipo;
      $this->view("mobile/mobile",$this->conteudo);
    }
    public function imovel(){
         if(isset($this->conteudo["c"])){
            $id = $this->conteudo["c"];
            $projetos = new ProjetoModel();
            $lazer = new LazerModel();
            $imagens = new ImagensModel();
            $status = new StatusModel();
            $fichas = new FichaModel();
            $coords = new MapaModel();
            $projeto= $projetos->listaProjetosTipoByid($id);
            $listaLazer = $lazer->listaLazer("lazer_id");         
            $galeria = $imagens->listaImagensGaleria("projeto_id=".$id." and img_tipo <> 5", "img_id");
            $obras = $imagens->listaImagemModuloObra($id);
            $valStatus = $status->listaStatusModulo($id);
            $itensFicha = $fichas->listaFichaById("id_projeto=".$id, "item_id");
            $lat_lng = $coords->listaMapa("projeto_id=".$id, "mapa_id");
            $this->conteudo["proj"]= $projeto;
            $this->conteudo['lazer'] = $listaLazer;            
            $this->conteudo['galeria'] = $galeria;   
            $this->conteudo['obras'] = $obras;
            $this->conteudo['status'] = $valStatus;
            $this->conteudo['ficha'] = $itensFicha;
            $this->conteudo['mapa'] = $lat_lng;
            $this->view("mobile/mobile_interna",$this->conteudo);
         }
         
    }
    public function sobre(){
        $this->conteudo["titulo"] = "sobre";
        $this->view("mobile/mobile",$this->conteudo);
    }
    public function visite(){
        $this->conteudo["titulo"] = "visite";
        $this->view("mobile/mobile",$this->conteudo);
    }
    public function contato(){
        $this->conteudo["titulo"] = "contato";
        $projeto = new ProjetoModel();
        $proj_lista = $projeto->listaProjetos("id");
        $this->conteudo["contato"] = $proj_lista;
        $this->view("mobile/mobile",$this->conteudo);
    }
    public function ligamos(){
        $this->conteudo["titulo"] = "ligamos";
        if(isset($this->conteudo["do"])){
            $p = "<p style'=color:#000;font-size:12px;font-family:Arial, Helvetica, Verdana, sans-serif;'>";
            $assunto = "Nós Ligamos";
            $nome = $_POST['nome'];
            if(isset($nome) ){
            $telefone = $_POST['tel'];
            $msg_recipient = $p."Nome: ".$nome."</p>
                        ".$p."Telefone: ".$telefone."</p>";
            
                $emailfrom = 'corretor@bigconstrutora.com.br'; 
    /* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta. Não alterar */
            if (PATH_SEPARATOR == ";"){
                $quebra_linha = "\r\n";
            }        
        //Se for Windows
            else{
                $quebra_linha = "\n"; //Se "não for Windows"
            }
            $title = utf8_encode($assunto." - Mobile");
    //HEADERS
            $header = "MIME-Version: 1.1" . $quebra_linha;
            $header .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
            $header .= "From: sitecontato@bigconstrutora.com.br" . $quebra_linha;
            $header .= "Reply-To: " . $email . $quebra_linha;

    //ENVIO PARA O EMAIL: toddler@toddlerdi.com
            if (!$envio_recipient = mail($emailfrom, utf8_decode($title), $msg_recipient, $header, "-r" . $emailfrom)) { // Se for Postfix
                $header .= "Return-Path: " . $emailfrom . $quebra_linha; // Se "não for Postfix"
                $envio_recipient = mail($emailfrom, utf8_decode($title), $msg_recipient, $header);
            }
            if($envio_recipient){
                $result = trim("ok");
                $erro = trim("erro");
                echo $result;
            }
            else{
                echo $erro;
            }
            }else{echo "erro";}
        }else{
            $this->view("mobile/mobile",$this->conteudo); 
        }
    }
    public function envio(){
        $p = "<p style'=color:#000;font-size:12px;font-family:Arial, Helvetica, Verdana, sans-serif;'>";
       $assunto = $_POST['assunto_contato'];
       $nome = $_POST['nome'];
       $email = $_POST['email'];     
       $telefone = $_POST['tel'];
       $mensagem = $_POST['mensagem'];
       if(isset($nome) ){
       if($assunto == "Informações Sobre Empreendimento"){
           $empreendimento = $_POST['empreendimento'];
           
           $msg_recipient = $p."Nome: ".$nome."</p>
            ".$p."E-mail: ".$email."</p>
            ".$p."Telefone: ".$telefone."</p>
            ".$p."Empreendimento: ".$empreendimento."</p>
            ".$p."Mensagem: ".$mensagem."</p>";
       }
       else if($assunto == "Relacionamento Com o Cliente"){
           $empreendimento = $_POST['empreendimento'];
           $torre = $_POST['torre'];
           $unidade = $_POST['unidade'];
           $msg_recipient = $p."Nome: ".$nome."</p>
            ".$p."E-mail: ".$email."</p>
            ".$p."Telefone: ".$telefone."</p>
            ".$p."Empreendimento: ".$empreendimento."</p>
            ".$p."Torre: ".$torre."</p>
            ".$p."Unidade: ".$unidade."</p>
            ".$p."Mensagem: ".$mensagem."</p>";
       }else{
           $msg_recipient = $p."Nome: ".$nome."</p>
            ".$p."E-mail: ".$email."</p>
            ".$p."Telefone: ".$telefone."</p>
            ".$p."Mensagem: ".$mensagem."</p>";
       }
       
        $emailfrom = 'contato@bigconstrutora.com.br'; 
        //$emailfrom = "devteste@" . $_SERVER[HTTP_HOST];
        //    Na linha acima estamos forçando que o remetente seja 'webmaster@seudominio',
        // Você pode alterar para que o remetente seja, por exemplo, 'contato@seudominio'.
    /* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta. Não alterar */
    if (PATH_SEPARATOR == ";"){
        $quebra_linha = "\r\n";
    }        
//Se for Windows
    else{
        $quebra_linha = "\n"; //Se "não for Windows"
    }
    $title = utf8_encode($assunto." - Mobile");
    //HEADERS
    $header = "MIME-Version: 1.1" . $quebra_linha;
    $header .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
    $header .= "From: sitecontato@bigconstrutora.com.br" . $quebra_linha;
    $header .= "Reply-To: " . $email . $quebra_linha;

    //ENVIO PARA O EMAIL: toddler@toddlerdi.com
    if (!$envio_recipient = mail($emailfrom, utf8_decode($title), $msg_recipient, $header, "-r" . $emailfrom)) { // Se for Postfix
        $header .= "Return-Path: " . $emailfrom . $quebra_linha; // Se "não for Postfix"
        $envio_recipient = mail($emailfrom, utf8_decode($title), $msg_recipient, $header);
    }
     if($envio_recipient){
        echo "ok";
    }
    else{
        echo "erro";
    }
    }else{echo "erro";}
    }
}

