<?php
if ($view_acao_form == "Cadastrar"):
    $page1 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';
?>
<div class="well">
    <strong>Projetos - <?php echo $view_acao_form; ?></strong>
</div>
<?php

if (isset($view_content)):
    foreach ($view_content as $vContent):
        $id = $vContent['id'];
        $titulo = $vContent['titulo'];
        $subtitulo = $vContent['subtitulo'];
        $tipo = $vContent['tipo'];
        $dormitorio = $vContent['dormitorio'];
        $area = $vContent['area'];
        $valor = $vContent['valor'];
        $endereco = $vContent['endereco'];
        $tags = $vContent['tags'];
        $lazer = $vContent['lazer'];
        $inicio = date("d/m/Y",strtotime($vContent['inicio']));
        $entrega = date("d/m/Y",strtotime($vContent['entrega']));
        $descricao = str_replace("<br>",chr(13),$vContent['descricao']);
        $apresentacao = str_replace("<br>",chr(13),$vContent['apresentacao']);
        $estagio = $vContent['estagio'];
        $paleta = $vContent['paleta'];
        $flag_destaque = $vContent['flag'];
        $showroom = $vContent['showroom'];
		$tourvirtual = $vContent['tourvirtual'];
        $fase = $vContent['fase'];
        
    endforeach;
else:
    $id = "";
    $titulo = "";
    $subtitulo = "";
    $tipo = "";
    $dormitorio = "";
    $area = "";
    $valor = "";
    $endereco = "";
    $tags = "";
    $inicio = "";
    $entrega = "";
    $descricao = "";
    $apresentacao = "";
    $estagio = "";
    $lazer = "";
    $paleta = "";
    $flag_destaque= "";
    $showroom = "";
	$tourvirtual = "";
    $fase = "";
endif;

($id == "") ? $getid = "" : $getid = "id/" . $id;
?>
<form action="" method="post" class="form-horizontal" name="atributo_form">
    <input type="hidden" name="codigo" id="codigo" value="<?php echo $id; ?>">
    <div class="control-group">
        <label class="control-label" for="titulo">Título <span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="titulo" name="titulo" value="<?php echo $titulo;?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="subtitulo">Subtítulo <span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="subtitulo" name="subtitulo" value="<?php echo $subtitulo; ?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="codtipoatributo">Tipo de Projeto:</label>
        <div class="controls">
            <select name="tipo" id="tipo" class="required">
                <option value="0" class="text-warning">Escolha um tipo</option>
                <?php                
                foreach ($view_tipo as $valTipo):
                    if ($tipo == ""):?>                        
                        <option value="<?php echo $valTipo['tipoprj_id'] ?>"><?php echo $valTipo['tipoprj_nome'] ?></option>                        
                    <?php else: ?>
                        <option value="<?php echo $valTipo['tipoprj_id'] ?>" <?php echo ($valTipo['tipoprj_id'] == $tipo) ? "selected='selected'" : "" ?>><?php echo $valTipo['tipoprj_nome'] ?></option>
                    <?php
                    endif;
                endforeach;
                ?>
            </select>           
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="dormitorio">Dormitórios <span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="dormitorio" name="dormitorio" value="<?php echo $dormitorio;?>" required="required" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode)))
                                                    return true;
                                                else
                                                    return false;">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="area">Área privativa<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="area" name="area" value="<?php echo $area;?>" required="required" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode)))
                                                    return true;
                                                else
                                                    return false;">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="valor">Valor dos imóveis<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="valor" name="valor" value="<?php echo $valor;?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="endereco">Endereço do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="endereco" name="endereco" value="<?php echo $endereco;?>" required="required" class="span5">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="tags">Tags de busca:</label>
        <div class="controls">
            <input type="text" id="tags" name="tags" value="<?php echo $tags;?>" class="span5">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inicio">Início do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="inicio" name="inicio" value="<?php echo $inicio;?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="entrega">Entrega do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="entrega" name="entrega" value="<?php echo $entrega;?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="descricao">Descrição do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <textarea id="descricao" name="descricao" required="required" rows="8" class="span5"><?php echo $descricao;?></textarea>
            
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="apresentacao">Apresentação do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <textarea id="apresentacao" name="apresentacao" required="required" rows="6" class="span5"><?php echo $apresentacao;?></textarea>
            
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="codtipoatributo">Estágio do Projeto:</label>
        <div class="controls">
            <select name="estagio" id="estagio" class="required">
                <option value="0" class="text-warning">Escolha um estágio</option>
                <?php                
                foreach ($view_estagios as $valEstagio):
                    
                    if ($estagio == ""):?>                        
                        <option value="<?php echo $valEstagio['estagios_id'] ?>"><?php echo $valEstagio['estagios_nome'] ?></option>                        
                    <?php else: 
                        ?>
                        <option value="<?php echo $valEstagio['estagios_id'] ?>" <?php echo ($valEstagio['estagios_id'] == $estagio) ? "selected='selected'" : "" ?>><?php echo $valEstagio['estagios_nome'] ?></option>
                    <?php
                    endif;
                endforeach;
                ?>
            </select>           
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="lazer">Áreas de Lazer<span class="required">*</span>:</label>
        <div class="controls">
            <?php            
            if(isset($view_lazer) && sizeof($view_lazer)!=0):
                $nomes_lazer ="";
                $counter=1;
                $selecionado = 0;
                $flag = false;
                $lazer = explode(",", $lazer);
                
                foreach($view_lazer as $itemLazer):
                    
                    while($selecionado <=  (sizeof($lazer)-1)): 
                        
                        if($itemLazer['lazer_id']==$lazer[$selecionado]):  
                            $nomes_lazer.=$itemLazer['lazer_titulo'].",";
                            $flag=true;
                        ?>               
                            <label class="checkbox inline"><input type="checkbox" class="lazer" value="<?php echo $itemLazer['lazer_id']?>" <?php echo "checked='checked'"?>>
                            <?php echo $itemLazer['lazer_titulo']?></label><?php if($counter==4){echo "<br>";$counter=0;}?>            
            <?php       else:
                            if(!$flag && $selecionado==(sizeof($lazer)-1)):
                                $flag=true;?>
                            <label class="checkbox inline"><input type="checkbox" class="lazer" value="<?php echo $itemLazer['lazer_id']?>">
                            <?php echo $itemLazer['lazer_titulo']?></label><?php if($counter==4){echo "<br>";$counter=0;}?>
            <?php 
                            endif;
                        endif;
                        
                        $selecionado++;
                    endwhile;
                    $flag=false;
                    $selecionado=0;
                    $counter++;
                endforeach; 
                echo "<input type='hidden' value='".$nomes_lazer."' name='nomeslazer'/>";
            else:?>
            <label>Não há área de lazer cadastrada</label>
            <?php endif;?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="paleta">Paletas de cor<span class="required">*</span>:</label>
        <div class="controls">
            <?php if(isset($view_paleta) && sizeof($view_paleta)!=0):
                
                      foreach ($view_paleta as $itempaleta):?>
        <label class="radio inline">
            <input type="radio" name="paletasel" value="<?php echo $itempaleta['paleta_id']?>" <?php echo ($paleta==$itempaleta['paleta_id'])?"checked":"";?> required="required">
            <img src="<?php echo PATH_ROOT?>web-files/img/paleta/<?php echo $itempaleta['paleta_img']?>" width="50"/>
        </label>
            <?php endforeach;
            else:?>
                <label>Não há paletas cadastradas</label>
        <?php endif;?>
        </div>
    </div>
    <div class="control-group">
        <label>Destaque para o Banner:<br>
            <input type="checkbox" id="flag" value="s"<?php
                if($flag_destaque=="s"){echo "checked";}?>/>
        </label>
    </div>
    <div class="control-group">
        <label>Venda no ShowRoom:<br>
            <input type="checkbox" id="showroom" value="s"<?php
                if($showroom=="s"){echo "checked";}?>/>
        </label>
    </div>
    <div class="control-group">
        <label>Possui tour virtual:<br>
            <input type="checkbox" id="tourvirtual" value="s"<?php
                if($tourvirtual=="s"){echo "checked";}?>/>
        </label>
    </div>
    <div class="control-group">
        <label class="control-label" for="fase">Fases do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="fase" name="fase" value="<?php echo $fase;?>" <?php echo ($view_acao_form =='Editar'? 'readonly': 'required="required"');?>>
            
        </div>
        <div class="alert alert-info" role="alert" style="margin-top:20px;width: 60%">
            <p>1: Fase Única(sem módulos)</p>
            <p>2: Com Módulos</p>
        </div>
    </div>
    <div class="well" style="width: 60%">
        <button type="submit" class="btn" id="bt_projeto">Salvar</button>
    </div>
</form>

<?php @include 'include/footer_painel.php'; ?>  
<script>
   function retiraUltimoCaractere(str){
        // Executamos a linha abaixo para termos certeza de que o parâmetro a ser tratado será uma string
        var str=new String(str);
        // Enfim, aplicamos a função substring(), para retornar a string do primeiro até o penúltimo caractere
        return str.substring(0,(str.length - 1));
    }
    $(document).ready(function(){
        $('#valor').maskMoney({thousands:'.',decimal:','});
        $("#inicio,#entrega").mask("99/99/9999");
        
        $("#bt_projeto").click(function(e){
            e.preventDefault();
            var codigo = $("#codigo").val();
            var flag = $("#flag:checked").val();
            if(flag!="s"){
                flag="n";
            }
            var showroom = $("#showroom:checked").val();
            if(showroom!="s"){
                showroom="n";
            }
			var tourvirtual = $("#tourvirtual:checked").val();
            if(tourvirtual!="s"){
                tourvirtual="n";
            }
            var titulo = $("#titulo").val();
            var subtitulo = $("#subtitulo").val();
            var tipo = $("#tipo option:selected").val();
            var dormitorio = $("#dormitorio").val();
            var area = $("#area").val();  
            var valor = $("#valor").val();
            var endereco = $("#endereco").val();
            var tags = $("#tags").val();
            var inicio = $("#inicio").val();
            var entrega = $("#entrega").val();
            var descricao = $("#descricao").val().replace(/\n/g,"<br>");
            var apresentacao = $("#apresentacao").val().replace(/\n/g,"<br>");
            var estagio = $("#estagio option:selected").val();
            var qtd_lazer = $(".lazer").length;
            var paleta = $("input:radio[name=paletasel]:checked").val();
            var total_lazer="";
            for(var i=0;i < qtd_lazer;i++){
                    if($(".lazer:checked").eq(i).val()!=null){
                        total_lazer+=$(".lazer:checked").eq(i).val()+",";
                    }
                }
             var lazer =  retiraUltimoCaractere(total_lazer);  
            var fase = $("#fase").val();
            var urlDirect = '<?php echo PATH_ROOT . "admin/" . strtolower($view_acao_form) . "/secao/projetos/do/action/" . $getid ?>';
           
            if(titulo ==""){
                alert("Digite o titulo do Projeto");
            }            
            else if(subtitulo==""){
                alert("Digite o subtitulo do Projeto");
            }
            else if(tipo==0){
                alert("Escolha um tipo");
            }
            else if(dormitorio ==""){
                alert("Digite a quantidade de dormitórios");
            } 
            else if(area == ""){
                alert("Digite a área privativa");
            }
            else if(valor == ""){
                alert("Digite o valor do projeto");
            }
            else if(endereco == ""){
                alert("Digite o endereço do projeto");
            }
            else if(inicio == ""){
                alert("Digite o ínicio do projeto");
            }
            else if(entrega == ""){
                alert("Digite a entrega do projeto");
            }
            else if(descricao == ""){
                alert("Digite a descrição técnica do projeto");
            }
            else if(apresentacao == ""){
                alert("Digite a apresentação");
            }
            else if(estagio ==0){
                alert("Escolha um estágio");
            }
            else if(paleta==undefined){
                alert("Escolha uma paleta de cor");
            }
            else if(fase == ""){
                alert("Digite a fase do projeto");
            }
            else{
                $.ajax({                        					
                    type:"post",
                    url:urlDirect,
                    data:{codigo:codigo,titulo:titulo,subtitulo:subtitulo,tipo:tipo,dormitorio:dormitorio,area:area,valor:valor,endereco:endereco,tags:tags,lazer:lazer,inicio:inicio,entrega:entrega,descricao:descricao,apresentacao:apresentacao,estagio:estagio,paleta:paleta,flag:flag,showroom:showroom,tourvirtual:tourvirtual,fase:fase},					
        			success:function(data){	                                  
                        if(data==0){						
                            alert('Erro ao cadastrar!');
        				}
                        else{
                            alert('sucesso!');
                            document.location.href="<?php echo PATH_ROOT?>admin/gerenciar/secao/projetos";
                        }                                                
                    }										
                });
            }
        });
    });
</script>
</body>
</html>