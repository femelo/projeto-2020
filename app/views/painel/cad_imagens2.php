<?php
    if ($view_acao_form == "Cadastrar"):
    $page8 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Imagens do Projeto - <?php echo $view_acao_form;?></strong>
</div>
<?php

if (isset($view_content) && sizeof($view_content)!=0):  
 ?>
<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="control-group">
        <label class="control-label" for="titulo">Selecione o Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <select name="sel_projeto" id="sel_projeto">
                <?php foreach ($view_content as $vContent):
                    $modulo = "";
                    ($vContent['nome_modulo'] ==""?$modulo = "":$modulo = " - ".$vContent['nome_modulo']);
                    $idModulo = 0;
                    ($vContent['id_modulo'] ==""?$idModulo = 0:$idModulo = $vContent['id_modulo']);
                    ?>
                <option value="<?php echo $vContent["id"]."-".$idModulo ?>"><?php echo $vContent['titulo'].$modulo;?></option>
                <?php endforeach;?>
            </select>
            
            <input type="hidden" value="<?php echo $view_tipo;?>" id="tipo"/>
        </div>
    </div>
    <div class="well">
        <button type="submit" class="btn bt_imgadicional" id="bt_img_passo2">Prosseguir</button>
    </div>
</form>
<?php endif;
if(sizeof($view_content)==0):?>
<div class="alert alert-info">Não há projetos Disponíveis</div>
<?php endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
        $("#bt_img_passo2").click(function(e){
            e.preventDefault();
            var selecionado = $("#sel_projeto option:selected").val().split("-");
            var modulo = selecionado[1];
            var projeto = selecionado[0];
            var tipo = $("#tipo").val();
            document.location.href = "<?php echo PATH_ROOT?>admin/cadastrar/secao/imagens/c/"+projeto+"/t/"+tipo+"/m/"+modulo;
        });
        
    });
</script>
</body>
</html>

