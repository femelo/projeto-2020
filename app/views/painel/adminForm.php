<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador de Conteúdo - Big</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo PATH_ROOT ?>web-files/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo PATH_ROOT ?>web-files/css/painel.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

   
    
  </head>

  <body id="bg_login">
      
    <div class="container_admin">

        <form class="form-signin" action="<?php echo PATH_ROOT?>admin/login/acao/logar" method="post" name="formlogin">
        <h4 class="text-info">Administrador de Conteúdo</h4>
        <h2 class="form-signin-heading">Big Construtora</h2>
        <div>
        <input type="text" class="form-control" placeholder="Login" name="login">
        <input type="password" class="form-control" placeholder="Senha" name="senha">
        </div>
        <input class="btn btn-small btn-success" type="submit" value="Acessar"/>
        <input type="reset" value="limpar" class="btn btn-small btn-success"/><br><br>
        <a href="<?php echo PATH_ROOT;?>" class="btn btn-primary">Voltar ao site</a>
        <span><?php if(isset($view_erro)){ 
            echo $view_erro;}
            else{echo "";}?></span>
           
      </form>
       
    </div> <!-- /container -->

    
  </body>
</html>

