<?php 
$page2 = PAINEL_MENU;
 @include 'include/header_painel.php';?>
<div class="well">
    <strong>Lista de Projetos</strong>
</div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th class="centraliza">#</th>
                    <th>Projeto</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="centraliza">Adicionais:<br>Galeria</th>
                    <th class="centraliza">Adicionais:<br>Ficha Técnica</th>                    
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <?php if(!$view_sql):?>
            <tr>
                <td colspan="9" class="centraliza">Não há dados cadastrados</td>
            </tr>
            <?php
            else:
                $counter=1;
            foreach($view_sql as $projeto):?>
         
            <tr>
                <td class="centraliza span1"><?php echo $counter;?></td>
                <td><?php echo $projeto['titulo'];?></td>
                <td class="centraliza span2">
                    <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo PATH_ROOT."admin/editar/secao/destaque/id/".$projeto['id']?>">
                        <input type="hidden" value="<?php echo $projeto['id']?>" name="codigo" class="codigo"/>
                        <?php if($projeto['img_destaque']==null):?>
                        <label class="text-info">Inserir imagem<br>DESTAQUE:
                            <?php else:?>
                            <label class="text-success">ALTERAR imagem<br>DESTAQUE:
                                <?php endif;?>
                            <input type="file" class="filestyle img_destaque" data-input="false" name="destaque" ></label>
                </td>
                <td class="centraliza span2">
                    <?php if($projeto['thumb']==null):?>
                    <label class="text-info">Inserir<br>THUMB:
                         <?php else:?>
                        <label class="text-success">ALTERAR<br/> THUMB:
                            <?php endif;?>
                        <input type="file" class="filestyle img_thumb" data-input="false" name="thumb"></label>
                </td>
                <td class="centraliza span2">
                    <?php if($projeto['thumb']==null):?>
                    <label class="text-info">Inserir<br>BANNER:
                         <?php else:?>
                        <label class="text-success">ALTERAR<br/> BANNER:
                            <?php endif;?>
                        <input type="file" class="filestyle img_banner" data-input="false" name="banner"></label>
                </td>
                <td class="centraliza span2">
                    <?php if($projeto['thumb']==null):?>
                    <label class="text-info">Inserir<br>BG:
                         <?php else:?>
                        <label class="text-success">ALTERAR<br/> BG:
                            <?php endif;?>
                        <input type="file" class="filestyle img_bg" data-input="false" name="bg"></label>
                </td>
                <td class="centraliza span1">
                    <button type="submit" class="btn btn-info bt_img">Enviar<br>imagens</button></form>
                </td>
                <td class="centraliza span2"><a href="<?php echo PATH_ROOT."admin/editar/secao/mapa/id/".$projeto['id'];?>" class="btn btn-success">Editar<br>Localização</a></td>
                <td class="centraliza span2"><a href="<?php echo PATH_ROOT."admin/gerenciar/secao/imagens/c/".$projeto['id'];?>" class="btn btn-warning">Alterar<br>Imagens<br>galeria</a></td>
                <td class="centraliza span2"><a href="<?php echo PATH_ROOT."admin/gerenciar/secao/ficha/c/".$projeto['id'];?>" class="btn btn-warning">Alterar<br>Ficha<br>Técnica</a></td>
                <td class="centraliza span1"><a href="<?php echo PATH_ROOT."admin/editar/secao/projetos/id/".$projeto['id']?>"><em class="icon-pencil"></em></a></td>
                <td class="centraliza span1"><a href="javascript:void(0)" onclick="apaga('<?php echo PATH_ROOT."admin/deletar/secao/projetos/id/".$projeto['id']?>')"><em class="icon-trash"></em></a></td>
            </tr>
            <?php 
            $counter++;
            endforeach;
            endif;
            ?>
        </table>
</div>
    
<?php @include 'include/footer_painel.php';?>
<script>
   
    
    function apaga(url){
               
        var r=confirm("Tem certeza que deseja apagar este item?");
        if (r==true)
        {
            document.location.href= url;
        }
            }   
     
    $(document).ready(function(){
         
         $(".bt_img").click(function(e){
             e.preventDefault();
             var valor = $(this).parents("tr");
             var img1 = valor.find('.img_destaque').val();
             var img2 = valor.find('.img_thumb').val();
             var img3 = valor.find('.img_banner').val();
             var img4 = valor.find('.img_bg').val();
             
             if(img1=="" && img2=="" && img3=="" && img4==""){
                 alert("selecione um imagem para alterar");
             }else{
                 valor.find(".form-horizontal").submit();
             }
         });
     });
</script>  
  </body>
</html>

    



