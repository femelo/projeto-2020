<?php
if ($view_acao_form == "Cadastrar"):
    $page5 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Áreas de Lazer - <?php echo $view_acao_form; ?></strong>
</div>
<?php
if (isset($view_content)):
    foreach ($view_content as $vContent):
        $id = $vContent['lazer_id'];
        $titulo = $vContent['lazer_titulo'];
        $img = $vContent['lazer_img'];
    endforeach;
else:
    $id = "";
    $titulo = "";
    $img = "";
endif;
($id == "") ? $getid = "" : $getid = "id/" . $id;
?>
<form action="<?php echo PATH_ROOT."admin/".strtolower($view_acao_form)."/secao/lazer/do/action/".$getid ?>" method="post" class="form-horizontal" name="lazer_form" enctype="multipart/form-data">
    <input type="hidden" name="codtipo" id="codtipo" value="<?php echo $id; ?>">
    <div class="control-group">
        <label class="control-label" for="tituloarea">Título da área de lazer<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="tituloarea" name="tituloarea" value="<?php echo $titulo; ?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="imgarea">Imagem da área de lazer<span class="required">*</span>:</label>
        <div class="controls">
            <input type="file" id="imgarea" name="imgarea" <?php if ($view_acao_form == "Cadastrar"){echo 'required="required"';}?>>
        </div>
        
    </div> 
    <?php if($img!=""):?>
    <img src="<?php echo PATH_ROOT.'web-files/img/ico_lazer/'.$img;?>" alt="icone" style="width: 35px;margin-bottom: 20px;"/>
        <span class="label">Imagem Atual</span>
        <?php endif;?>
    <div class="well">
        <button type="submit" class="btn" id="bt_lazer">Salvar</button>
    </div>
</form>
<?php @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
       
    });
</script>
</body>
</html>

