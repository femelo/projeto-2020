<?php
   
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Imagens do Projeto "<?php echo $view_projeto[0]["titulo"]?>"<br><?php echo $view_acao_form;?> tipo "<?php echo $view_tipo[0]["tipoimg_nome"]?>"</strong>
</div>
<?php
if (isset($view_tipo) && sizeof($view_tipo)!=0):  
 ?>
<form action="<?php echo PATH_ROOT?>admin/cadastrar/secao/imagens/do/action/" method="post" class="form-horizontal" enctype="multipart/form-data">
    <input type="hidden" value="<?php echo $view_projeto[0]['id']?>" name="sel_projeto"/>
    <input type="hidden" value="<?php echo $view_tipo[0]['tipoimg_id']?>" name="sel_tipoimg"/>
    <input type="hidden" value="<?php echo $view_modulo?>" name="sel_modulo"/>
    <?php if($view_tipo[0]['tipoimg_id'] !=5):?>
    <div class="control-group">
        <label class="control-label" for="legenda">Legenda da Imagem:</label>
        <div class="controls">
            <input type="text" id="legenda" name="legenda">
        </div>
    </div>
    <?php 
    else:?>
    <div class="control-group">
        <label class="control-label" for="data">Data da Imagem(Somente para tipo "Implantação"):</label>
        <div class="controls">
            <input type="text" id="data" name="data">
        </div>
    </div>
    <?php endif;?>
    <div class="control-group">
        <label class="control-label" for="img">Selecione uma Imagem Adicional<span class="required">*</span>:</label>
            <input type="file" class="filestyle" data-input="false" required="required" name="img_adicional">       
    </div>
    <div class="well">
        <button type="submit" class="btn bt_imgadicional" >Salvar</button>
    </div>
</form>
<?php endif;
if(sizeof($view_tipo)==0):?>
<div class="alert alert-info">Não há projetos Disponíveis</div>
<?php endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
        $("#data").mask("99/9999");
    });
</script>
</body>
</html>

