<?php 
 @include 'include/header_painel.php';?>
<div class="well">
    <strong>Itens da Ficha Técnica</strong>
</div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th class="centraliza">#</th>
                    <th>Itens</th>
                    <th>Valores</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <?php if(!$view_ficha):?>
            <tr>
                <td colspan="5" class="centraliza">Não há dados cadastrados</td>
            </tr>
            <?php
            else:
               $counter=1;
            foreach($view_ficha as $ficha):?>         
            <tr class="line_<?php echo $ficha['id_projeto']?> linhas_ficha">
                <td class="centraliza span1"><?php echo $counter;?></td>
                <td><?php echo $ficha['item_nome'];?></td>
                <td><?php echo $ficha['item_valor']?></td>
                <td class="centraliza span1"><a href="<?php echo PATH_ROOT."admin/editar/secao/ficha/id/".$ficha['item_id']?>"><em class="icon-pencil"></em></a></td>
                <td class="centraliza span1"><a href="javascript:void(0)" onclick="apaga('<?php echo PATH_ROOT."admin/deletar/secao/ficha/id/".$ficha['item_id']?>')"><em class="icon-trash"></em></a></td>
                
            </tr>          
          <?php $counter++;
           
          endforeach;?>
           <?php endif;
            ?>
        </table>
</div>
<?php
@include 'include/footer_painel.php';?>
<script>
    function apaga(url){
               
        var r=confirm("Tem certeza que deseja apagar este item?");
        if (r==true)
        {
            document.location.href= url;
        }
            }
    $(document).ready(function(){ 
    });
</script>
  </body>
</html>

    



