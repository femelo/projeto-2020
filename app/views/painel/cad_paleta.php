<?php
if ($view_acao_form == "Cadastrar"):
    $page12 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Paleta de cores - <?php echo $view_acao_form; ?></strong>
</div>
<?php
if (isset($view_content)):
    foreach ($view_content as $vContent):
        $id = $vContent['paleta_id'];
        $titulo = $vContent['paleta_cor'];
        $img = $vContent['paleta_img'];
        $cor_titulo = $vContent['paleta_tituloprj'];
        $cor_sub = $vContent['paleta_subtitulo'];
        $cor_active = $vContent['paleta_active'];
        $cor_header = $vContent['paleta_header'];
        $cor_icon = $vContent['paleta_iconbg'];
        $cor_financiamento = $vContent['paleta_financiamento'];
        $cor_bgprj = $vContent['paleta_bgprj'];
    endforeach;
else:
    $id = "";
    $titulo = "";
    $img = "";
    $cor_titulo = "";
    $cor_sub = "";
    $cor_active = "";
    $cor_header = "";
    $cor_icon = "";
    $cor_financiamento = "";
    $cor_bgprj = "";
endif;
($id == "") ? $getid = "" : $getid = "id/" . $id;
?>
<form action="<?php echo PATH_ROOT."admin/".strtolower($view_acao_form)."/secao/paleta/do/action/".$getid ?>" method="post" class="form-horizontal" name="paleta_form" enctype="multipart/form-data">
    <input type="hidden" name="codpaleta" id="codpaleta" value="<?php echo $id; ?>">
    <div class="control-group">
        <label class="control-label" for="nomecor">Cor Primária da paleta<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="nomecor" name="nomecor" value="<?php echo $titulo; ?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="cortitulo">Cor do título do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="cortitulo" name="cortitulo" value="<?php echo $cor_titulo; ?>" required="required" maxlength="7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="corsub">Cor do Subtítulo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="corsub" name="corsub" value="<?php echo $cor_sub; ?>" required="required" maxlength="7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="coractive">Cor de box ativo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="coractive" name="coractive" value="<?php echo $cor_active; ?>" required="required" maxlength="7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="corheader">Cor de cabeçalhos<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="corheader" name="corheader" value="<?php echo $cor_header; ?>" required="required" maxlength="7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="coricone">Cor de fundo dos ícones<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="coricone" name="coricone" value="<?php echo $cor_icon; ?>" required="required" maxlength="7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="corfinanciamento">Cor de fundo box financiamento<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="corfinanciamento" name="corfinanciamento" value="<?php echo $cor_financiamento; ?>" required="required" maxlength="7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="corbg">Cor de fundo da Página do Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="corbg" name="corbg" value="<?php echo $cor_bgprj; ?>" required="required" maxlength="7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="imgpaleta">Imagem da paleta de cores<span class="required">*</span>:</label>
        <div class="controls">
            <input type="file" id="imgpaleta" name="imgpaleta" <?php echo ($view_acao_form=="Cadastrar")?"required=\"required\"":"";?>>
        </div>
        
    </div> 
    <?php if($img!=""):?>
    <img src="<?php echo PATH_ROOT.'web-files/img/paleta/'.$img;?>" alt="paleta" style="width: 100px;margin-bottom: 20px;"/>
        <span class="label">Imagem Atual</span>
        <?php endif;?>
    <div class="well">
        <button type="submit" class="btn" id="bt_paleta">Salvar</button>
    </div>
</form>
<?php @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
       
    });
</script>
</body>
</html>

