<?php
    if ($view_acao_form == "Cadastrar"):
    $page14 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Módulos de Projetos - <?php echo $view_acao_form;?></strong>
</div>
<?php
if (isset($view_projetos) && sizeof($view_projetos)!=0):  
 ?>
<form action="" method="post" class="form-horizontal">
<div class="control-group">
        <label class="control-label" for="titulo">Selecione o Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <select id="sel_projeto">
                <?php foreach ($view_projetos as $vContent):?>
                <option value="<?php echo $vContent["id"] ?>"><?php echo $vContent['titulo']?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="titulo">Nome do Módulo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="nomemodulo" name="nomemodulo" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inicio">Início do Módulo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="inicio" name="inicio" value="<?php echo $inicio;?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="entrega">Entrega do Módulo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="entrega" name="entrega" value="<?php echo $entrega;?>" required="required">
        </div>
    </div>
    <div class="well">
        <button type="submit" class="btn bt_itemmodulo" >Salvar</button>
    </div>
</form>
<?php 
endif;
if(sizeof($view_content)==0):?>
<div class="alert alert-info">Não há projetos Disponíveis</div>
<?php endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
        $("#inicio,#entrega").mask("99/99/9999");
        $(".bt_itemmodulo").click(function(e){
            e.preventDefault();
            var codigo = $("#sel_projeto option:selected").val();
            var nome = $("#nomemodulo").val();
            var inicio = $("#inicio").val();
            var entrega = $("#entrega").val();
           
            var urlDirect = "<?php echo PATH_ROOT."admin/cadastrar/secao/modulo/do/action/"?>";

            if(nome ==""){
                alert("Digite o nome do módulo");
            } 
            else if(inicio == ""){
                alert("Digite o ínicio do módulo");
            }
            else if(entrega == ""){
                alert("Digite a entrega do módulo");
            }
            else{
                $.ajax({                        					
        			type:"post",
        			url:urlDirect,
        			data:{codigo:codigo,nome:nome,inicio:inicio,entrega:entrega},					
                    success:function(data){	  
                        if(data==0){						
                            alert('Erro ao cadastrar!');
    				    }
                        else{
                            alert('sucesso!');
                            document.location.href="<?php echo PATH_ROOT?>admin/cadastrar/secao/modulo";
                        }                                                
    			    }										
                });
            }
        });
        $('.click_accordion').next('form').hide();
        $('.click_accordion').click(function(e){
            if($(this).next('form').css("display")=='block'){
                e.preventDefault();
            }else{
                $(".form-horizontal:visible").slideUp("slow");
                $(this).next(".form-horizontal").slideDown("slow");
                return false;
            }
        });
    });
</script>
</body>
</html>