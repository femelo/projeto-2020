<?php
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Mapa de Localização - <?php echo $view_acao_form;?></strong>
</div>
<?php if (isset($view_content)): ?>
    <?php foreach ($view_content as $vContent):
        $latitude = $vContent['mapa_latitude'];
        $longitude = $vContent['mapa_longitude'];
        $icone = $vContent['mapa_thumb'];
        $id = $vContent['mapa_id'];
        $projeto = $vContent['projeto_id'];
    endforeach;?>
<div id="area_maps"></div>
<form action="<?php echo PATH_ROOT.'admin/editar/secao/mapa/do/action/id/'.$id;?>" class="form-horizontal" enctype="multipart/form-data" method="post">
    <input type="hidden" value="<?php echo $projeto;?>" name="codigo"/>
    <div class="control-group">
        <label class="control-label" for="latitude">Latitude do Mapa:</label>
        <div class="controls">
            <input type="text" id="latitude" name="latitude" value="<?php echo $latitude;?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="longitude">Longitude do Mapa:</label>
        <div class="controls">
            <input type="text" id="longitude" name="longitude" value="<?php echo $longitude;?>">
        </div>
    </div>
    <img src="<?php echo ($icone=='')?PATH_ROOT.'web-files/img/ico_mapa/icone_default.png':PATH_ROOT.'web-files/img/ico_mapa/'.$icone?>" width="50"/>
    <div class="control-group">
        <label class="control-label" for="longitude">Alterar Ícone:        
            <input type="file" id="thumb" name="thumb" class="filestyle" data-input="false">
        </label>
    </div>
    <br/><br/>
    <div class="well">
        <button type="submit" class="btn bt_mapa" >Salvar</button>
    </div>
</form>
<?php endif; 
@include 'include/footer_painel.php'; ?>
<script>
    function initialize() {	
    var latlng = new google.maps.LatLng(<?php echo $latitude.', '.$longitude;?>);
 	
    var options = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	
    map = new google.maps.Map(document.getElementById("area_maps"), options);
	
	var marker = new google.maps.Marker({
    position: new google.maps.LatLng(<?php echo $latitude.', '.$longitude;?>),
    <?php if($icone!=""):?>
        icon: '<?php echo PATH_ROOT."web-files/img/ico_mapa/".$icone;?>',
    <?php endif;?>
    map:map
   
});
}
    $(document).ready(function(){
      google.maps.event.addDomListener(window, 'load', initialize);  
    });
</script>
</body>
</html>
