<?php
if ($view_acao_form == "Cadastrar"):
    $page4 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Tipos de Projetos - <?php echo $view_acao_form; ?></strong>
</div>
<?php
$campos = array();
if (isset($view_content)):
    foreach ($view_content as $vContent):
        $id = $vContent['tipoprj_id'];
        $nome = $vContent['tipoprj_nome'];
    endforeach;
else:
    $id = "";
    $nome = "";
endif;
($id == "") ? $getid = "" : $getid = "id/" . $id;
?>
<form action="" method="post" class="form-horizontal" name="tipo_form">
    <input type="hidden" name="codtipo" id="codtipo" value="<?php echo $id; ?>">
    <div class="control-group">
        <label class="control-label" for="nometipo">Nome do Tipo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="nometipo" name="nometipo" value="<?php echo $nome; ?>" required="required">
        </div>
    </div>       
    <div class="well">
        <button type="submit" class="btn" id="bt_tipo">Salvar</button>
    </div>
</form>
<?php @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
        $("#bt_tipo").click(function(e){
            e.preventDefault();
            var codtipo = $("#codtipo").val();
            var nometipo = $("#nometipo").val();
            var urlDirect = '<?php echo PATH_ROOT . "admin/" . strtolower($view_acao_form) . "/secao/tipo/do/action/" . $getid ?>';
            
            if(nometipo==""){
                alert("Digite o tipo de Projeto");
            }
            else{
                $.ajax({                        					
			type:"post",
			url:urlDirect,
			data:{codtipo:codtipo,nometipo:nometipo},					
			success:function(data)
			    {	  
                                
                            if(data==0)
				{						
                                    alert('Erro ao cadastrar!');
				}
                               else{
                                 alert('sucesso!');
                                      document.location.href="<?php echo PATH_ROOT?>admin/gerenciar/secao/tipo";
                                }                                                
			    }										
		    });
            }
            
        });
    });
</script>
</body>
</html>

