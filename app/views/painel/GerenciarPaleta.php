<?php 
$page13 = PAINEL_MENU;
 @include 'include/header_painel.php';
    ?>
<div class="well">
    <strong>Lista de Itens de Lazer</strong>
</div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th class="centraliza">#</th>
                    <th>Paletas de cor</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <?php if(!$view_sql):?>
            <tr>
                <td colspan="4" class="centraliza">Não há dados cadastrados</td>
            </tr>
            <?php
            else:
                $counter=1;
            foreach($view_sql as $projeto):?>
         
            <tr>
                <td class="centraliza span1"><?php echo $counter;?></td>
                <td><?php echo $projeto['paleta_cor'];?></td>
                <td class="centraliza span1"><a href="<?php echo PATH_ROOT."admin/editar/secao/paleta/id/".$projeto['paleta_id']?>"><em class="icon-pencil"></em></a></td>
                <td class="centraliza span1"><a href="javascript:void(0)" onclick="apaga('<?php echo PATH_ROOT."admin/deletar/secao/paleta/id/".$projeto['paleta_id']?>')"><em class="icon-trash"></em></a></td>
            </tr>
            <?php 
            $counter++;
            endforeach;
            endif;
            ?>
        </table>
</div>
<?php @include 'include/footer_painel.php';?>
<script>
    function apaga(url){
               
        var r=confirm("Tem certeza que deseja apagar este item?");
        if (r==true)
        {
            document.location.href= url;
        }
            }
    $(document).ready(function(){
        
    });
</script>    
  </body>
</html>

    





