<?php
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Módulos das fases dos projetos - <?php echo $view_acao_form;?></strong>
</div>
<?php
if (isset($view_content)):
    foreach ($view_content as $vContent):
         $inicio = date("d/m/Y",strtotime($vContent['inicio']));
        $entrega = date("d/m/Y",strtotime($vContent['entrega']));
 ?>
<form action="" method="post" class="form-horizontal">
    <input type="hidden" name="codigo" id="codigo" value="<?php echo $vContent["id_modulo"] ?>">          
    <div class="control-group">
        <label class="control-label" for="titulo">Nome do Módulo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="nomeitem" name="nomeitem" value="<?php echo $vContent["nome_modulo"]?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inicio">Início do Módulo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="inicio" name="inicio" value="<?php echo $inicio?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="entrega">Entrega do Módulo<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="entrega" name="entrega" value="<?php echo $entrega?>" required="required">
        </div>
    </div>
    <div class="well">
        <button type="submit" class="btn" id="bt_itemficha">Salvar</button>
    </div>
</form>
<?php endforeach;
endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
        $("#inicio,#entrega").mask("99/99/9999");
        $("#bt_itemficha").click(function(e){
            e.preventDefault();
            
            var codigo = $("#codigo").val();
            var nome_modulo = $("#nomeitem").val();
            var inicio = $("#inicio").val();
            var entrega = $("#entrega").val();
            
            var urlDirect = "<?php echo PATH_ROOT."admin/editar/secao/modulo/do/action/id/"?>"+codigo;
             $.ajax({                        					
    			type:"post",
    			url:urlDirect,
    			data:{codigo:codigo,nome_modulo:nome_modulo,inicio:inicio,entrega:entrega},					
    			success:function(data){	  
                    if(data==0){						
                        alert('Erro ao cadastrar!');
    				}
                    else{
                        alert('sucesso!');
                        document.location.href="<?php echo PATH_ROOT?>admin/gerenciar/secao/modulo";
                    }                                                
    			}										
		    });
        });
       
    });
</script>
</body>
</html>

