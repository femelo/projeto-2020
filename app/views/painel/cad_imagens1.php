<?php
    if ($view_acao_form == "Cadastrar"):
    $page8 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Imagens do Projeto - <?php echo $view_acao_form;?></strong>
</div>
<?php
if (isset($view_tipoimg) && sizeof($view_tipoimg)!=0):  
 ?>
<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="control-group">
        <label class="control-label" for="titulo">Selecione o Tipo de Imagem<span class="required">*</span>:</label>
        <div class="controls">
            <select name="sel_tipoimg" id="sel_tipoimg">
                <?php foreach ($view_tipoimg as $vTipoImg):?>
                <option value="<?php echo $vTipoImg["tipoimg_id"] ?>"><?php echo $vTipoImg['tipoimg_nome']?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="well">
        <button type="submit" class="btn bt_imgadicional" id="bt_img_passo1">Prosseguir</button>
    </div>
</form>
<?php endif;
if(sizeof($view_tipoimg)==0):?>
<div class="alert alert-info">Não há projetos Disponíveis</div>
<?php endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
        $("#bt_img_passo1").click(function(e){
            e.preventDefault();
            var tipo = $("#sel_tipoimg option:selected").val();
            document.location.href = "<?php echo PATH_ROOT?>admin/cadastrar/secao/imagens/t/"+tipo;
        });
        
    });
</script>
</body>
</html>

