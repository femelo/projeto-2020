<?php

@include 'include/header_painel.php';?>
<div class="well">
    <strong>Resultado</strong>
</div>
<?php  
if($view_mensagem!=0):
    $classe ="alert-success";
    $mensagem = "Sucesso!\nClique para voltar";
    else:
    $classe="alert-error";
    $mensagem = "Ocorreu um erro!\nClique e tente novamente!";
endif;
?>
    <div class="alert <?php echo $classe;?>">
        <p><?php echo $mensagem;?></p>
        <a href="<?php echo PATH_ROOT.'admin/'.$view_page?>">Voltar</a>        
    </div>

<?php @include 'include/footer_painel.php'; ?>  
</body>
</html>

