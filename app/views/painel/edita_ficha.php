<?php
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Itens da Ficha Técnica - <?php echo $view_acao_form;?></strong>
</div>
<?php
if (isset($view_content)):
    foreach ($view_content as $vContent):
 ?>
<form action="" method="post" class="form-horizontal">
    <input type="hidden" name="codigo" id="codigo" value="<?php echo $vContent["item_id"] ?>">          
    <div class="control-group">
        <label class="control-label" for="titulo">Nome do item<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="nomeitem" name="nomeitem" value="<?php echo $vContent["item_nome"]?>" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="titulo">Valor do item<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="valoritem" name="valoritem" value="<?php echo $vContent["item_valor"]?>" required="required">
        </div>
    </div>
    <div class="well">
        <button type="submit" class="btn" id="bt_itemficha">Salvar</button>
    </div>
</form>
<?php endforeach;
endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
       
        $("#bt_itemficha").click(function(e){
            e.preventDefault();
            
            var codigo = $("#codigo").val();
            var nome = $("#nomeitem").val();
            var valor = $("#valoritem").val();
            
            var urlDirect = "<?php echo PATH_ROOT."admin/editar/secao/ficha/do/action/id/"?>"+codigo;
             $.ajax({                        					
			type:"post",
			url:urlDirect,
			data:{codigo:codigo,nome:nome,valor:valor},					
			success:function(data)
			    {	  
                               
                           if(data==0)
				{						
                                    alert('Erro ao cadastrar!');
				}
                               else{
                                 alert('sucesso!');
                                      document.location.href="<?php echo PATH_ROOT?>admin/gerenciar/secao/ficha";
                                }                                                
			    }										
		    });
        });
       
    });
</script>
</body>
</html>

