<?php 
 @include 'include/header_painel.php';
$page15 = PAINEL_MENU;
 ?>
<div class="well">
    <strong>Módulos das Fases do Projeto</strong>
</div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th class="centraliza">#</th>
                    <th>Projetos</th>
                    <th>Módulos</th>
                    <th></th>
                </tr>
            </thead>
            <?php if(!$view_sql):?>
            <tr>
                <td colspan="5" class="centraliza">Não há dados cadastrados</td>
            </tr>
            <?php
            else:
               $counter=1;
            foreach($view_sql as $modulos):
                ?>         
            <tr class="line_<?php echo $modulos['id_modulo']?> linhas_ficha">
                <td class="centraliza span1"><?php echo $counter;?></td>
                <td><?php echo $modulos['titulo'];?></td>
                <td><?php echo $modulos['nome_modulo']?></td>
                <td class="centraliza span1"><a href="<?php echo PATH_ROOT."admin/editar/secao/modulo/id/".$modulos['id_modulo']?>"><em class="icon-pencil"></em></a></td>
            </tr>          
          <?php $counter++;
           
          endforeach;?>
           <?php endif;
            ?>
        </table>
</div>
<?php
@include 'include/footer_painel.php';?>
  </body>
</html>

    



