<?php 
function checa($page){
    ($page)?$valor=$page:$valor="";
    return $valor;
}?>

<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <title>Administrador de Conteúdo - Big</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo PATH_ROOT;?>web-files/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo  PATH_ROOT;?>web-files/css/painel.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="<?php echo  PATH_ROOT;?>web-files/css/bootstrap-responsive.min.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>
      <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="javascript:void(0)">Big Construtora</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
                <a href="<?php echo PATH_ROOT?>admin/logout/" class="navbar-link">SAIR</a>
            </p>
            <ul class="nav">
              <li class="active"><a href="javascript:void(0)">Painel Administrativo</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Projeto - Inserção</li>
              <li class="nav-header <?php echo checa($page1);?>" ><a href="<?php echo PATH_ROOT?>admin/cadastrar/secao/projetos">inserir Projeto</a></li>
              <li class="nav-header">Projeto - Listagem</li>
              <li class="nav-header <?php echo checa($page2);?>"><a href="<?php echo PATH_ROOT?>admin/gerenciar/secao/projetos">Listar Projetos</a></li>              
              <li class="nav-header">Sistema - Inserção</li>              
              <li class="nav-header <?php echo checa($page4);?>"><a href="<?php echo PATH_ROOT?>admin/cadastrar/secao/tipo">inserir tipo de Projeto</a></li>
              <li class="nav-header <?php echo checa($page5);?>"><a href="<?php echo PATH_ROOT?>admin/cadastrar/secao/lazer">inserir itens de lazer</a></li>
              <li class="nav-header <?php echo checa($page12);?>"><a href="<?php echo PATH_ROOT?>admin/cadastrar/secao/paleta">inserir paleta de cores</a></li>
              <li class="nav-header">Sistema - Listagem</li>
              <li class="nav-header <?php echo checa($page6);?>"><a href="<?php echo PATH_ROOT?>admin/gerenciar/secao/tipo">Listar tipo</a></li>
              <li class="nav-header <?php echo checa($page7);?>"><a href="<?php echo PATH_ROOT?>admin/gerenciar/secao/lazer">Listar itens de lazer</a></li>
              <li class="nav-header <?php echo checa($page13);?>"><a href="<?php echo PATH_ROOT?>admin/gerenciar/secao/paleta">Listar paletas de cor</a></li>
              <li class="nav-header">Adicionais do Projeto</li>
              <li class="nav-header <?php echo checa($page15);?>"><a href="<?php echo PATH_ROOT?>admin/gerenciar/secao/modulo">Listar Módulos</a></li>
              <li class="nav-header <?php echo checa($page8);?>"><a href="<?php echo PATH_ROOT?>admin/cadastrar/secao/imagens">inserir Imagens</a></li>
              <li class="nav-header <?php echo checa($page9);?>"><a href="<?php echo PATH_ROOT?>admin/cadastrar/secao/ficha">inserir Itens da ficha técnica</a></li>
              <li class="nav-header <?php echo checa($page14);?>"><a href="<?php echo PATH_ROOT?>admin/cadastrar/secao/modulo">inserir Módulos de Projeto</a></li>
              <li class="nav-header <?php echo checa($page10);?>"><a href="<?php echo PATH_ROOT?>admin/editar/secao/status">Editar Status da obra</a></li>
              
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9"> 