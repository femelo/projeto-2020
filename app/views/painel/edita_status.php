<?php
    $page10 = PAINEL_MENU;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Status de Projetos - Editar</strong>
</div>
<?php
$campos = array();
if (isset($view_content) && sizeof($view_content)!=0):
    foreach ($view_content as $vContent):
        $modulo =  $vContent['nome_modulo'];
        $titulo_completo ='';
        ($modulo==""?$titulo_completo=$vContent['titulo']:$titulo_completo=$vContent['titulo'].' - '.$modulo);
 ?>
<a href="javascript:void(0)" class="btn btn-info click_accordion">Ver Projeto <?php echo $titulo_completo;?></a>

<form action="" method="post" class="form-horizontal">
    <input type="hidden" name="codigo" class="codigo" value="<?php echo $vContent["status_id"] ?>">          
            <p>Movimento de Terra</p><input type="range" id="nometipo" class="status1" min="0" max="100" step="1" value="<?php echo $vContent['status_mov_terra']; ?>">              
            <br><br><p>Fundações</p><input type="range" class="status2" name="nometipo" min="0" max="100" step="1" value="<?php echo $vContent['status_fundacoes']; ?>">        
            <br><br><label>Estrutura e Paredes</label><input type="range" class="status3" name="nometipo" min="0" max="100" step="1" value="<?php echo $vContent['status_estrut_parede']; ?>">
            <br><br><label>Acabamentos e Pinturas</label><input type="range" class="status4" name="nometipo" min="0" max="100" step="1" value="<?php echo $vContent['status_acab_pint']; ?>">
            <br><br><label>Instalações</label><input type="range" class="status5" name="nometipo" min="0" max="100" step="1" value="<?php echo $vContent['status_instalacoes']; ?>">
            <br><br><label>Áreas Externas</label><input type="range" class="status6" name="nometipo" min="0" max="100" step="1" value="<?php echo $vContent['status_area_ext']; ?>">
        
        <br><br>
    <div class="well">
        <button type="submit" class="btn bt_status">Salvar</button>
    </div>
</form>
<?php endforeach;
endif;
if(sizeof($view_content==0)):?>
<div class="alert alert-info">Não há status Disponíveis</div>
<?php endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
        $(":range").rangeinput();
        $(".bt_status").click(function(e){
            e.preventDefault();
            var valor = $(this).parents("form");
            var codigo = valor.children(".codigo").val();
            var st1 = valor.children(".status1").val();
            var st2 = valor.children(".status2").val();
            var st3 = valor.children(".status3").val();
            var st4 = valor.children(".status4").val();
            var st5 = valor.children(".status5").val();
            var st6 = valor.children(".status6").val();
            var urlDirect = "<?php echo PATH_ROOT."admin/editar/secao/status/do/action/"?>";
             $.ajax({                        					
			type:"post",
			url:urlDirect,
			data:{codigo:codigo,st1:st1,st2:st2,st3:st3,st4:st4,st5:st5,st6:st6},					
			success:function(data)
			    {	  
                                
                            if(data==0)
				{						
                                    alert('Erro ao cadastrar!');
				}
                               else{
                                 alert('sucesso!');
                                      document.location.href="<?php echo PATH_ROOT?>admin/editar/secao/status";
                                }                                                
			    }										
		    });
        });
        $('.click_accordion').next('form').hide();
        $('.click_accordion').click(function(e){
            if($(this).next('form').css("display")=='block'){
                e.preventDefault();
            }else{
                $(".form-horizontal:visible").slideUp("slow");
                $(this).next(".form-horizontal").slideDown("slow");
                return false;
            }
        });
    });
</script>
</body>
</html>