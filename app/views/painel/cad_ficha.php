<?php
    if ($view_acao_form == "Cadastrar"):
    $page9 = PAINEL_MENU;
endif;
@include 'include/header_painel.php';?>
<div class="well">
    <strong>Itens da Ficha Técnica - <?php echo $view_acao_form;?></strong>
</div>
<?php
if (isset($view_content) && sizeof($view_content)!=0):  
 ?>
<form action="" method="post" class="form-horizontal">
<div class="control-group">
        <label class="control-label" for="titulo">Selecione o Projeto<span class="required">*</span>:</label>
        <div class="controls">
            <select id="sel_projeto">
                <?php foreach ($view_content as $vContent):?>
                <option value="<?php echo $vContent["id"] ?>"><?php echo $vContent['titulo']?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="titulo">Nome do item<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="nomeitem" name="nomeitem" required="required">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="titulo">Valor do item<span class="required">*</span>:</label>
        <div class="controls">
            <input type="text" id="valoritem" name="valoritem" required="required">
        </div>
    </div>
    <div class="well">
        <button type="submit" class="btn bt_itemficha" >Salvar</button>
    </div>
</form>
<?php 
endif;
if(sizeof($view_content)==0):?>
<div class="alert alert-info">Não há projetos Disponíveis</div>
<?php endif;
 @include 'include/footer_painel.php'; ?>  
<script>
    $(document).ready(function(){
       
        $(".bt_itemficha").click(function(e){
            e.preventDefault();
            var valor = $(this).parents("form");
            var codigo = $("#sel_projeto option:selected").val();
            var nome = $("#nomeitem").val();
            var valor = $("#valoritem").val();
           
            var urlDirect = "<?php echo PATH_ROOT."admin/cadastrar/secao/ficha/do/action/"?>";
             $.ajax({                        					
			type:"post",
			url:urlDirect,
			data:{codigo:codigo,nome:nome,valor:valor},					
			success:function(data)
			    {	  
                                
                            if(data==0)
				{						
                                    alert('Erro ao cadastrar!');
				}
                               else{
                                 alert('sucesso!');
                                      document.location.href="<?php echo PATH_ROOT?>admin/cadastrar/secao/ficha";
                                }                                                
			    }										
		    });
        });
        $('.click_accordion').next('form').hide();
        $('.click_accordion').click(function(e){
            if($(this).next('form').css("display")=='block'){
                e.preventDefault();
            }else{
                $(".form-horizontal:visible").slideUp("slow");
                $(this).next(".form-horizontal").slideDown("slow");
                return false;
            }
        });
    });
</script>
</body>
</html>