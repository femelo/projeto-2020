<?php 
$page3 = PAINEL_MENU;
 @include 'include/header_painel.php';?>
<div class="well">
    <strong>Imagens Adicionais do Projeto</strong>
</div>
<?php
function mudaData($data){
                    $data = explode("-",$data);
                    return $data[1]."/".$data[0];
                }?>
<div class="control-group">
        <label class="control-label" for="titulo">Selecione galeria ou Obras por data da Imagem<span class="required">*</span>:</label>
        <div class="controls">
            <select id="sel_projeto">
                <?php foreach ($view_data as $vContent):
                    (mudaData($vContent["img_data"])=="00/0000")?$data="galeria":$data=mudaData($vContent["img_data"])
                    ?>
                <option value="<?php echo mudaData($vContent["img_data"]);?>"><?php echo $data;?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Imagem</th>
                    <th></th>
                    <th></th>
                    <th>Alterar data</th>
                    <th></th>
                    <th>Tipo</th>
                    <th></th>
                </tr>
            </thead>
            <?php if(!$view_imagens):?>
            <tr>
                <td colspan="5" class="centraliza">Não há dados cadastrados</td>
            </tr>
            <?php
            else:
               $counter=1;
            $img;           
            foreach($view_imagens as $imagens):
                switch($imagens['img_tipo']){
                case 1:$img="perspectivas";
                    break;
                case 2:$img="plantas";
                    break;
                case 3:$img="decorado";
                    break;
                case 4:$img="implantação";
                    break;
                case 5:$img="obras";
                    break;
                }
                
                ?>         
            <tr class="line_<?php echo $imagens['img_tipo']?> linhas_ficha">
                <td><img src="<?php echo PATH_ROOT."web-files/img/".$imagens['img_caminho'];?>" width="200"/></td>
                <td>
                     <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo PATH_ROOT."admin/editar/secao/imagens/id/".$imagens['img_id']."/p/".$imagens["projeto_id"]?>">
                         <input type="hidden" value="<?php echo $imagens['img_tipo'];?>" name="tipo"/>
                         <label>Alterar imagem:<input type="file" class="filestyle" data-input="false" name="img"></label>
                </td>
                <td><label>Alterar Legenda:</label><br><input type="text" value="<?php echo $imagens['img_legenda']?>" name="legenda"/></td>
                <td><?php if($imagens['img_tipo']==5):?>
                    <label>Alterar Data: <input type="text" name="data" value="<?php echo mudaData($imagens['img_data']);?>" class="data span5"/></label>
                    <?php else:
                        echo "Sem dados";
                    endif;?>
                </td>
                <td><input type="submit" class="btn btn-info" value="Alterar Dados"/></form></td>
                
                <td><?php echo $img;?></td>
                <td class="centraliza span1"><a href="javascript:void(0)" onclick="apaga('<?php echo PATH_ROOT."admin/deletar/secao/imagens/id/".$imagens['img_id']."/p/".$imagens["projeto_id"]?>')"><em class="icon-trash"></em></a></td>
                
            </tr>          
          <?php $counter++;
           
          endforeach;?>
           <?php endif;
            ?>
        </table>
</div>
<?php 
@include 'include/footer_painel.php';?>
<script>
    function apaga(url){
               
        var r=confirm("Tem certeza que deseja apagar este item?");
        if (r==true)
        {
            document.location.href= url;
        }
            }
    $(document).ready(function(){
        
        $(".data").mask('99/9999');        
        $(".linhas_ficha").css("display","none");
        $(".line_1,.line_2,.line_3,.line_4").css("display","table-row");
       // $(".no-dados").css("display","none");
        
        var tamanho = $(".linhas_ficha").length;
        
        $("#sel_projeto").change(function(){
           
            $(".linhas_ficha").css("display","none");
            var clicado = $("#sel_projeto option:selected").val();
            if(clicado=="00/0000"){
                $(".line_1,.line_2,.line_3,.line_4").css("display","table-row");
            }else{
                for(var i=0;i < $(".line_5").length;i++){
                    if($(".line_5 .data").eq(i).val()==clicado){             
                        $(".line_5").eq(i).css("display","table-row");
                    }
                }
            }
            var linhas = 0;
            var i=0;
            while(i < tamanho){
            if($(".linhas_ficha").eq(i).css("display")=="table-row"){
                 linhas++;
                 console.log(linhas);
                }
                i++;
            }
            if(linhas==0){
                $(".no-dados").css("display","table-row");
            }
            else{
                $(".no-dados").css("display","none");
            }
        });
    });
</script>
  </body>
</html>

    



