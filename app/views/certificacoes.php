<?php
$title = " | Certificações";
$body = "certificacoes";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                <a href="<?php echo PATH_ROOT?>sobre/">Sobre a Big</a> >
                Certificações
            </p>
            <h1 id="titulo_interna_prj">Certificações</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/sobre/certificacoes/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/sobre/certificacoes/" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div> 
        <div id="bloco_chamada_destaque">
            <h1 id="txt_chamada_destaque">Big Construtora<br>Construindo o seu grande sonho.</h1>
        </div>
        <div id="container_sobre">
        <div class="sidebar_esq">
            <h3 class="header_sidebar_esq">Sobre a Big</h3>
            <div id="filtro_estagio">
                <a href="/sobre#missao" class="ancora_sobre">Missão</a>
                <a href="/sobre#visao" class="ancora_sobre">Visão</a>
                <a href="/sobre#valores" class="ancora_sobre">Valores</a>
            </div>
            <a class="ancora_sobre " href="javascript:void(0)"><strong>Certificações</strong></a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>sobre/qualidade">Política da Qualidade Big</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>noticias/">Notícias</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>localizacao/">Localização</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>contato/">Fale Conosco</a>
           
        </div>
        <div id="content_sobre">  
            <div id="bloco_certificadoA">
                <img src="<?php echo PATH_ROOT?>web-files/img/logo_pbqp.jpg"/>
                <p class="desc_certificacao"><strong>PBQP do Habitat – Nível A</strong><br><br>
                    O <a href="http://pbqp-h.cidades.gov.br" target="_blank">PBQP-H</a>, Programa Brasileiro da Qualidade e Produtividade do Habitat, é um instrumento do Governo Federal
                    criado para organizar o setor da construção civil em torno de duas questões principais: <strong>a melhoria da qualidade
                        do habitat</strong> e a <strong>modernização produtiva</strong>.
                    A busca por esses objetivos envolve um conjunto de ações, entre as quais se destacam: avaliação da conformidade
                    de empresas de serviços e obras, melhoria da qualidade de materiais, formação e requalificação de mão-de-obra,
                    normalização técnica, capacitação de laboratórios, avaliação de tecnologias inovadoras, informação ao consumidor
                    e promoção da comunicação entre os setores envolvidos.
                </p>
                <div class="clearfix"></div>
            </div>
            <div id="bloco_certificadoB">
                <img src="<?php echo PATH_ROOT?>web-files/img/logo_iso.jpg"/>
                 <p class="desc_certificacao">                
                     <strong>NBR ISO 9001:2015</strong><br><br>
                A Big Construtora e Incorporadora possui Sistema de Gestão da Qualidade em conformidade
                com a norma NBR ISO 9001, certificada pelo ITAC – Instituto Tecnológico de Avaliação e Certificação da Conformidade Ltda.
            </p> 
            <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
