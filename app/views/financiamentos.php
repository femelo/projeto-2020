<?php
$title = " | Financiamentos";
$body = "financia";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                Financiamentos
            </p>
            <h1 id="titulo_interna_prj">Financiamentos</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/financiamentos/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/financiamentos/" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div> 
        <div id="bloco_chamada_destaque">
            <h1 id="txt_chamada_destaque">Financiamento fácil<br>Minha Casa Minha Vida</h1>
        </div>
        <div id="container_sobre">
        <div class="sidebar_esq">
            <h3 class="header_sidebar_esq">Links Úteis</h3>
            <a href="http://www.caixa.gov.br/novo_habitacao/" target="_blank" class="ancora_sobre">Caixa Habitação</a>
            <a href="http://www.caixa.gov.br/voce/habitacao/minha-casa-minha-vida/Paginas/default.aspx" target="_blank" class="ancora_sobre">Minha Casa Minha Vida</a>
            <a href="https://siopiweb.caixa.gov.br/siopiweb/mantemPropostaInternet.do?method=iniciarCasoUso" target="_blank" class="ancora_sobre">Acompanhe seu Financiamento</a>
            <a href="http://www.caixa.gov.br/voce/habitacao/minha-casa-minha-vida/urbana/Paginas/default.aspx" target="_blank" class="ancora_sobre">Documentos</a>
        </div>
        <div id="content_sobre">
            <p class="desc_sobre">O programa do Governo Federal, Minha Casa Minha Vida, é uma solução para facilitar
                a compra de seu apartamento da Big Construtora e realizar o sonho da casa própria.<br><br>
                <strong>Quem tem direito?</strong><br><br>
                Para financiar sua moradia pelo Minha Casa Minha Vida é preciso:<br>
                - Ter renda familiar de, no máximo, 10 salários mínimos.<br>
                - Não possuir nenhum imóvel.<br>
                - Não ter outro financiamento para a compra de imóvel.<br>
                - Não ter restrições de crédito.
            </p>
            <img src="<?php echo PATH_ROOT?>web-files/img/banner_caixa.jpg"/>
            <p class="desc_sobre"><strong>Como funciona?</strong><br><br>
                O Minha Casa Minha Vida quer que o máximo de pessoas tenham a casa própria, por isso o 
                programa te ajuda a parcelar o pagamento em muitas vezes (financiamento) e até paga uma 
                parte do valor para você (subsídio). O incentivo que o programa vai te dar é calculado a partir
                do valor do imóvel e de sua renda familiar.
            </p>
            <p class="desc_sobre">
                <strong>Quais são os benefícios?</strong><br><br>
                - Até 360 meses para pagar (30 anos).<br>
                - Liberação do seu fundo de garantia para realizar a compra.<br>
                - Fundo Garantidor que refinancia parte das parcelas em caso de desemprego.<br>
                - Taxas e juros menores que em outros financiamentos imobiliários, para viabilizar a compra.<br>
                - Subsídio de até R$ 25.000,00 (o Governo Federal paga uma parte do valor total do imóvel).
            </p>
            <p class="desc_sobre">
                <strong>Valor máximo do imóvel que você pode comprar pelo Programa:</strong><br><br>
                - São Paulo, Rio de Janeiro e Brasília: até R$ 190 mil.<br>
                    - Cidades a partir de 1 milhão de habitantes: até R$ 170 mil.<br>
                    - Cidades a partir de 250 mil habitantes e entorno do DF: até R$ 145 mil.<br>
                    - Cidades a partir de 50 mil habitantes: até R$ 115 mil.<br>
                    - Outras cidades: até R$ 90 mil.
            </p>
            <div id="box_simulacao">
                <a href="<?php echo URL_FINANCIA;?>" target="_blank" id="bt_financia">
                    <strong>SIMULAR FINANCIAMENTO</strong>
                    <p>MINHA CASA MINHA VIDA</p>
                </a>
                <p>Simule seu financiamento e comece a planejar seu imóvel da Big Construtora.</p>
            </div>
            <div id="box_infocaixa">
                 <a href="http://www.caixa.gov.br/voce/habitacao/minha-casa-minha-vida/urbana/Paginas/default.aspx" target="_blank" id="bt_infocaixa">
                    <strong>MAIS INFORMAÇÕES</strong>
                    <p>MINHA CASA MINHA VIDA</p>
                </a>
                <p>Tire suas dúvidas e saiba todos os detalhes do Minha Casa Minha Vida.</p>
            </div>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
<script> 
function carrega_interna(caminho){
        document.location.href="<?php echo PATH_ROOT?>empreendimentos/imovel/"+caminho;
    }
   $(document).ready(function(){               
        /*======== FUNÇÕES SLIDER =========*/ 
       $(".slides").eq(0).clone().appendTo("#banner_slider_b");
       var qtd = $(".slides").length;
       var contador=1;      
       $(window).resize(tamanho);
       tamanho();
       
       var pare = false;
       $(".slides").removeClass("next");
       $(".slides").removeClass("prev");
       $(".slides").eq(contador).addClass("next");
       $(".slides").eq(qtd-2).addClass("prev");
     
       $("#prev_slide").click(function(e){
          if(pare){
            e.preventDefault();
          } else{
            pare=true;
            var posicao = $("#banner_slider_b").css('left').replace("px", "");
            var tamanho_atual = $("#banner_slider").width();
            if(posicao==0 && contador==1){
                contador=qtd-1;
                var calculo = (qtd-1)*tamanho_atual;
                $("#banner_slider_b").css('left',-calculo);
                var posicao_atual=$("#banner_slider_b").css('left').replace("px", "");
                var calc_final = parseInt(posicao_atual)+parseInt(tamanho_atual);
                TweenLite.to("#banner_slider_b", 1, {'left': calc_final, ease: Expo.easeInOut,onComplete:desab});
                $(".slides").removeClass("next");
                $(".slides").removeClass("prev");
                $(".slides").eq(contador).addClass("next");
                $(".slides").eq(contador-2).addClass("prev");
            }else{
                contador--;
                var posicao_atual=$("#banner_slider_b").css('left').replace("px", "");
                var calc_final = parseInt(posicao_atual)+parseInt(tamanho_atual);
                TweenLite.to("#banner_slider_b", 1, {'left': calc_final, ease: Expo.easeInOut,onComplete:desab});
                $(".slides").removeClass("next");
                $(".slides").removeClass("prev");
                $(".slides").eq(contador).addClass("next");
                if(contador!=1){
                    $(".slides").eq(contador-2).addClass("prev");
                }else{
                    $(".slides").eq(qtd-2).addClass("prev");
                }                
              }
            }
        });
      
       $("#next_slide").click(function(e){
            if (pare) {
               e.preventDefault();
            } else {
               pare=true;
               contador++;         
               var posicao = $("#banner_slider_b").css('left').replace("px", "");        
               var tamanho_atual = $("#banner_slider").width();        
               TweenLite.to("#banner_slider_b", 1, {'left': (posicao-tamanho_atual), ease: Expo.easeInOut,onComplete:zera});
               $(".slides").removeClass("next");
               $(".slides").removeClass("prev");
               $(".slides").eq(contador).addClass("next");
               $(".slides").eq(contador-2).addClass("prev");
            }
        });
      
       $("#prev_slide").mouseover(function(){
            var img = $("#banner_slider_b").find(".prev").css("background-image");
            $("#bt_prev").css("display","none");
            $(".nav_prev_h").css({display:"block",backgroundImage:img});
        }); 
       $("#prev_slide").mouseout(function(){ 
            $(".nav_prev_h").css({display:"none"});       
            $("#bt_prev").css("display","block");           
        });
    
       $("#next_slide").hover(
            function(){
               $(this).children("#bt_next").css("display","none");
               var img = $("#banner_slider_b").find(".next").css("background-image");           
               $(".nav_next_h").css({display:"block",backgroundImage:img});
        }, 
            function(){ 
              $(this).children(".nav_next_h").css("display","none");
              $(this).children("#bt_next").css("display","block");
        });
      var tween = TweenLite.to("#barra_progresso", 15, {'width':"100%",ease:Linear.easeNone,onComplete:disparaProgress});
       function disparaProgress(){
        $("#barra_progresso").css("width","0%");
       $("#next_slide").trigger("click");
        tween.restart();
        }
       function desab(){
          pare=false; 
        }
       function zera(){
          pare=false;
          if(contador==qtd){
            contador=1;
            $("#banner_slider_b").css('left',"0");
            $(".slides").removeClass("next");
            $(".slides").removeClass("prev");
            $(".slides").eq(contador).addClass("next");
            $(".slides").eq(qtd-2).addClass("prev");
          } 
        }
       function tamanho(){
           var larguramedia =$("#content_header").css("left");
           $(".box_legenda_destaque").css("left",larguramedia);
           var tamanho = $("#banner_slider").width();
           for(var i=0; i<qtd; i++){
              $(".slides").eq(i).css("width",tamanho);
           }
           $("#banner_slider_b").css("width",tamanho* qtd);
           if(contador!=1){
               var calc_resize = (contador-1)*tamanho;
               $("#banner_slider_b").css("left",-calc_resize);
           }
        }   
       
       /*======== FIM FUNÇÕES SLIDER =========*/ 
   });
</script>
