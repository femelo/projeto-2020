<?php
$title = " | Política de Privacidade";
$body = "politica";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                Política de Privacidade
            </p>
            <h1 id="titulo_interna_prj">Política de Privacidade</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/sobre/politica" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/sobre/politica" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div> 
        <div id="bloco_chamada_destaque">
            <h1 id="txt_chamada_destaque">Big Construtora<br>Construindo o seu grande sonho.</h1>
        </div>
        <div id="container_sobre">        
            <p class="desc_sobre">
                Esta Política de Privacidade foi elaborada visando zelar pela confidencialidade, privacidade e proteção dos dados pessoais
                dos visitantes deste site e dos usuários de nossos serviços, protegendo essas informações de perdas, uso impróprio,
                acesso não autorizado, alteração e destruição.<br>
                <strong>1. Coleta e Uso das Informações</strong><br>
                O acesso a este Site poderá ser condicionado ao cadastramento do visitante, quando então serão solicitadas informações
                capazes de indentificá-lo e possibilitar o contato com nossos usuários. Ao decidir fornecer seus dados, o usuário declara
                conhecer e aceitar os termos da presente Política.<br>O Site também recebe e armazena automaticamente, por meio de cookies,
                informações sobre as atividades do navegador, incluindo endereço IP e a página acessada. Estes registros de atividades (logs)
                serão utilizados apenas para fins estatísticos e de métricas dos serviços disponibilizados.
                O usuário tem ciência e concorda que as informações coletadas serão utilizadas para os seguintes propósitos:
                (a) informar a respeito de novos conteúdos, notícias e eventos; (b) atualização das informações de contato;
                (c) otimizar a usabilidade e a experiência interativa durante a navegação do usuário no Site;
                (d) elaborar estatísticas gerais, sem que haja identificação dos usuários; (e) responder às dúvidas e solicitações
                do próprio usuário; e (f) obedecer determinações legais ou judiciais.Também nos reservamos o direito de utilizar
                tais registros, de forma individualizada, em casos de investigação de ilícitos civis ou criminais,
                especialmente quando houver supeita de fraude, ataques de hackers, alterações no sistema ou acesso indevido
                à informações protegidas e dados cadastrais de outros usuários.<br>
                <strong>2. Guarda e Responsabilidade pelas Informações Coletadas</strong><br>
                Qualquer informação fornecida pelos usuários será obtida e armazenada de acordo com os mais rígidos padrões de
                segurança e confiabilidade. Envidaremos nossos melhores esforços na manutenção da integridade das informações que
                nos forem fornecidas, razão pela qual não somos responsáveis pelos danos decorrentes do seu acesso e/ou utilização
                por terceiros estranhos.<br>O acesso às informações coletadas é restrito aos colaboradores e pessoas autorizadas.
                Aqueles que utilizarem indevidamente essas informações, em violação desta Política de Privacidade, estarão sujeitos
                à sanções administrativas e disciplinares, sem exclusão das medidas legais cabíveis. A não ser por determinação legal
                ou judicial, as informações dos usuários cadastrados neste Site jamais serão transferidas a terceiros ou usadas para
                finalidades diferentes daquelas para as quais foram coletadas.Todos os dados pessoais coletados serão incorporados ao
                banco de dados deste Site, sendo seu responsável a BIG CONSTRUTORA E INCORPORADORA.<br>                
                <strong>3. Responsabilidade dos Usuários</strong>
                É de responsabilidade do usuário a guarda de sua senha e de seu login de acesso. Não é recomendada a utilização de senhas óbvias,
                como datas especiais, nomes ou seqüências numéricas. Caso o usuário tome conhecimento ou apenas suspeite que sua senha foi descoberta,
                ele deverá alterá-la em sua página de cadastro.Temos o compromisso de proteger a privacidade on-line de crianças. Eventuais cadastros
                de menores ocorrerão apenas para fins de participação em áreas restritas do site com conteúdos educacionais. Os dados dos menores só
                serão utilizados para resposta a consultas, dúvidas acadêmicas, agendamento de palestras em escolas ou por motivo de estatísticas gerais.
                Recomendamos ainda que o menor esteja sempre sob a orientação e supervisão de seus responsáveis, que poderão, a qualquer tempo, solicitar
                o cancelamento do cadastro. Esta Política está sujeita a constantes melhorias e aprimoramentos. Desse modo, recomendamos sua periódica consulta.
            </p>       
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
