<?php
if(empty($view_proj)){
    header('Location: http://www.bigconstrutora.com.br/mobile');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- acesso DATATAG -->
        <script src="https://www.dataunion.com.br/80dc0f41-cea0-49f2-ba51-92a2da2504fe" type="text/javascript" async></script>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Big Construtora</title>
        <link rel="stylesheet" href="<?php echo PATH_ROOT ?>web-files/css/bootstrap.min.css" type="text/css" />

        <link rel="stylesheet" href="<?php echo PATH_ROOT ?>web-files/css/bootstrap-responsive.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo PATH_ROOT ?>web-files/css/mobile.css"/>
        <link rel="stylesheet" href="<?php echo PATH_ROOT ?>web-files/css/owl.carousel.css"/>
        <link rel="stylesheet" href="<?php echo PATH_ROOT ?>web-files/css/owl.theme.css"/>

        <!-- Código do Google para tag de remarketing -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 958495232;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/958495232/?guid=ON&amp;script=0"/>
</div>
</noscript>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53750666-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>

<script>
	var userAgent = navigator.userAgent.toLowerCase();
	var devices = new Array('nokia','iphone','blackberry','sony','lg',
	'htc_tattoo','samsung','symbian','SymbianOS','elaine','palm',
		 'series60','windows ce','android','obigo','netfront',
		 'openwave','mobilexplorer','operamini');
	var url_redirect = '<?php echo PATH_ROOT ?>';

		function init(){	
			function mobiDetect(userAgent, devices){
				for(var i = 0; i < devices.length; i++) {
					if (userAgent.search(devices[i]) > 0) {
						return true;
					}
				}
				return false;
			}
			if (mobiDetect(userAgent, devices)) {}  
			else{
				window.location.href = url_redirect;
			}      
		}
		window.onload = init();
</script>
    </head>
    <body class="mobile">
        <header class="header_mobile">
            <img src="<?php echo PATH_ROOT ?>web-files/img/logo_big.jpg"/>
            <div id="abre_mn_drop"><img src="<?php echo PATH_ROOT ?>web-files/img/icon_menu.png"/></div>
        </header>
        <nav id="menu_mobile">
            <a href="<?php echo PATH_ROOT ?>mobile/">Home</a>
            <a href="<?php echo PATH_ROOT ?>mobile/empreendimentos">Empreendimentos</a>
            <a href="<?php echo PATH_ROOT ?>mobile/sobre">Sobre a Big</a>
            <a href="<?php echo PATH_ROOT ?>mobile/visite">Visite-nos</a>
            <a href="<?php echo PATH_ROOT ?>noticias">Notícias</a>
            <a href="<?php echo PATH_ROOT ?>mobile/contato">Contato</a>
        </nav>      
        <?php
        foreach ($view_proj as $prj_mb):
             $titulo =  explode("<t>", $prj_mb["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
            $sem_acento =  trim(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $titulo_longo ) ));
            $titleUrl = strtolower(preg_replace('/\s/', '-', $sem_acento));
            $chamada = explode("<t>", $prj_mb['apresentacao']);
            $titulo_present = $chamada[0];
            $txt_present = $chamada[1];
            $lazer = $prj_mb['lazer'];
            $descricao = $prj_mb['descricao'];
            $showroom = $prj_mb['showroom'];
            ?>
            <h1 class="titulo_mob"><?php echo $titulo_longo;?></h1>
            <div class="empreendimentos_mob">
                <img src="<?php echo PATH_ROOT . "web-files/img/imgprojeto/" . $prj_mb['img_destaque'] ?>" class="img_destaque"/>
                <div class="legenda_mob"><p><?php echo $titulo_curto;?> > <span><?php echo $prj_mb['subtitulo'] ?></span></p></div>
                <?php if($showroom == "s"):?>
                    <a href="http://maps.google.com/maps?q=-23.53757,-46.362429" id="mobile-showroom">
                        <div id="blc-showroom-mb">
                            <h2>VISITE O SHOWROOM</h2>
                            <h3>Conheça Apto Decorado</h3>
                            <p>Avenida Brasil, 765, Centro, Ferraz de Vasconcelos, SP</p>
                        </div>
                    </a>
                <?php endif;
                foreach ($view_mapa as $mapa):
                    $lat = $mapa['mapa_latitude'];
                    $lng = $mapa['mapa_longitude'];
                endforeach;
?>
                <a href="http://maps.google.com/maps?q=<?php echo $lat.",".$lng;?>" class="btn"><em class="icon-map-marker"></em> <?php echo $prj_mb['endereco'] ?></a>
                <div id="box_descricao">
                    <h1 id="chamada_desc"><?php echo $titulo_present; ?></h1>
                    <p id="txt_desc"><?php echo $txt_present; ?></p>
                </div>
                <div id="slide">
    <?php foreach ($view_galeria as $thumbs_galeria): ?>
                        <div class="item">
                            <img src="<?php echo PATH_ROOT ?>web-files/img/<?php echo $thumbs_galeria["img_caminho"] ?>" alt="" />
                        </div>
    <?php endforeach; ?>
                </div>
            </div>
            <?php endforeach; ?>
            <h1 class="titulo_mob">Itens de lazer</h1>
        <ul class="container_lista_lazer">
            <?php
            if($lazer==""){
                echo "<li>Em Breve</li>";
            }else{
            $itens = explode(",", $lazer);
            $contador_item = 0;
            foreach ($view_lazer as $item_lazer):

                for ($i = 0; $i < sizeof($itens); $i++):
                    if ($contador_item < 1) {
                        $close_linha = "";
                    } else {
                        $close_linha = '</ul><ul class="container_lista_lazer">';
                    }
                    if ($itens[$i] == $item_lazer['lazer_id']):
                        $contador_item++;
                        ?>
                        <li>
                            <img src="<?php echo PATH_ROOT ?>web-files/img/ico_lazer/<?php echo $item_lazer['lazer_img'] ?>"/>
                            <span><?php echo $item_lazer['lazer_titulo'] ?></span>
                        </li>
                        <?php
                        echo $close_linha;
                        if ($contador_item == 2) {
                            $contador_item = 0;
                        }
                    endif;
                endfor;
            endforeach;
            }
            ?>
        </ul>
            <div id="content_ficha">
                <h1 class="titulo_mob">Ficha Técnica</h1>
                <ul id="lista_ficha">
                    <?php foreach($view_ficha as $lista_ficha):?>
                    <li><strong><?php echo $lista_ficha['item_nome']?>:</strong> <?php echo $lista_ficha['item_valor']?></li>
                    <?php endforeach;?>                    
                </ul>
                <div id="desc_ficha"><?php echo $descricao;?></div>
            </div>
            <div id="status">
                <h1 class="titulo_mob">Acompanhamento da Obra</h1>
                <?php foreach($view_status as $status):?>
                    <h2 class="titulo_modulo"><?php echo $status['nome_modulo']?></h2>
                    <div class="blc_status">
                        <ul class="porcentagem_obra">
                            <li><?php echo $status['status_mov_terra']?>%</li>
                            <li><?php echo $status['status_fundacoes']?>%</li>
                            <li><?php echo $status['status_estrut_parede']?>%</li>
                            <li><?php echo $status['status_acab_pint']?>%</li>
                            <li><?php echo $status['status_instalacoes']?>%</li>
                            <li><?php echo $status['status_area_ext']?>%</li>
                        </ul>
                        <ul>
                            <li class="titulo_status">MOVIMENTO DE TERRA</li>
                            <li class="barra_status"><span style="width:<?php echo $status['status_mov_terra']?>% "></span></li>
                            <li class="titulo_status">FUNDAÇÕES</li>
                            <li class="barra_status"><span style="width:<?php echo $status['status_fundacoes']?>% "></span></li>
                            <li class="titulo_status">ESTRUTURA E PAREDES</li>
                            <li class="barra_status"><span style="width:<?php echo $status['status_estrut_parede']?>% "></span></li>
                            <li class="titulo_status">ACABAMENTOS E PINTURAS</li>
                            <li class="barra_status"><span style="width:<?php echo $status['status_acab_pint']?>% "></span></li>
                            <li class="titulo_status">INSTALAÇÕES</li>
                            <li class="barra_status"><span style="width:<?php echo $status['status_instalacoes']?>% "></span></li>
                            <li class="titulo_status">ÁREA EXTERNAS</li>
                            <li class="barra_status"><span style="width:<?php echo $status['status_area_ext']?>% "></span></li>
                        </ul>
                    </div>
                <?php endforeach;?>
            </div>
            <div id="box_obras">
            <div id="galeria_obras">
                <?php foreach ($view_obras as $thumbs_obras): ?>
                        <div class="item">
                            <img src="<?php echo PATH_ROOT ?>web-files/img/<?php echo $thumbs_obras["img_caminho"] ?>" alt="" />
                            <p style="text-align: center;padding-top: 20px;"><?php echo $thumbs_obras["nome_modulo"]?></p>
                        </div>
    <?php endforeach; ?>                
            </div>
            </div>
        <div id="rodape_mobile">
            <div id="bts_sociais">
          <a href="<?php echo URL_FACE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" width="40"/></a>
          <a href="<?php echo URL_TWITTER;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" width="40"/></a>
          <a href="<?php echo URL_YOUTUBE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" width="40"/></a>
          <a href="<?php echo URL_INSTA;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" width="40"/></a>
          <a href="<?php echo URL_PINTEREST;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" width="40"/></a>
          </div>         
            <p class="desc_sobre_inverse">
                <img src="<?php echo PATH_ROOT ?>web-files/img/logo_footer.png" width="40"/><br>
                Copyright © 2014<br>
                Big Construtora e Incorporadora<br>
                os os direitos reservados.<br>
                Rua Antonio Camardo, 577<br>
                Tatuapé - São Paulo - SP<br>
                CEP 03309-060
            </p>
        </div>
        <footer id="footer_contato_mb">
          <div id="conteudo_contato_footer">
          <div id="blc_whatsapp">
            	<div id="header_whatsapp"><p>Atendimento por WhatsApp</p><div id="close_popup">X</div></div>
                <p class="desc_whatsapp">Para conversar com um corretor, adicione o número: (11) 94244-3587 nos contatos, clique abaixo:</p>
                <a href="<?php echo PATH_ROOT."web-files/files/big.vcf";?>" class="add_contato">Salvar contato</a>
                <p class="desc_whatsapp">Após salvar o contato, clique abaixo para abrir o WhatsApp.</p>
                <a href="whatsapp://send?text=Olá Big Construtora" class="bt_whats_interna">Atendimento WhatssApp</a>
            </div>
              <a href="<?php echo PATH_ROOT?>mobile/ligamos" id="bt_ligamos_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligamos_mb.png"/></a>
            <a href="tel:1120900301" id="bt_ligue_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligue_mb.png"/></a>
            <a href="javascript:void(0)" id="bt_whats_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_whatsapp_mb.png"/></a>
            <a href="<?php echo PATH_ROOT?>mobile/contato" id="bt_email_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_email_mb.png"/></a>
          </div>
      </footer>
    </body>
    <script src="<?php echo PATH_ROOT ?>web-files/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo PATH_ROOT ?>web-files/js/bootstrap.min.js"></script>
    <script src="<?php echo PATH_ROOT ?>web-files/js/owl.carousel.min.js"></script>
    <script>
        function largura_elemento() {
            var largura_destaque = $(".img_destaque").width();
            $(".legenda_mob").css("width", largura_destaque);
        }
        $(document).ready(function() {
			$("#bt_whats_mb").click(function(){
			   $("#blc_whatsapp").show();
			});
			$("#close_popup").click(function(){
				$("#blc_whatsapp").hide();
			});
            $("#bt_ligamos_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_nosligamos_interna');
            });
            $("#bt_ligue_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_ligue_interna');
            });
            $("#bt_email_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_envie_duvida_interna');
            });
			$("#bt_whats_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_whatsapp_interna');
            });
            $(window).load(function() {
                $(window).resize(largura_elemento);
                largura_elemento();
            });

            var flag = false;
            $("#abre_mn_drop img").click(function() {
                flag = !flag;
                if (flag) {

                    $("#menu_mobile").animate({marginTop: 0}, 500);
                } else {
                    $("#menu_mobile").animate({marginTop: -165}, 500);
                }
            });
            $("#slide,#galeria_obras").owlCarousel({
                autoPlay: 8000, //Set AutoPlay to 3 seconds
                items: 4,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [979, 3],
                navigation: true,
                pagination: false
            });
        });
    </script>
</html>