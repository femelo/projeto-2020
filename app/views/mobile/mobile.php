<!DOCTYPE html>
<html>
   <head>
      <!-- acesso DATATAG -->
      <script src="https://www.dataunion.com.br/80dc0f41-cea0-49f2-ba51-92a2da2504fe" type="text/javascript" async></script>
      <meta charset="UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Big Construtora</title>
            <link rel="stylesheet" href="<?php echo PATH_ROOT?>web-files/css/bootstrap.min.css" type="text/css" />

      <link rel="stylesheet" href="<?php echo PATH_ROOT?>web-files/css/bootstrap-responsive.min.css" type="text/css" />
      <link rel="stylesheet" href="<?php echo PATH_ROOT?>web-files/css/mobile.css"/>
      <!-- Código do Google para tag de remarketing -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 958495232;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/958495232/?guid=ON&amp;script=0"/>
</div>
</noscript>
      <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53750666-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
      <script>
        	 var userAgent = navigator.userAgent.toLowerCase();
        	var devices = new Array('nokia','iphone','blackberry','sony','lg',
        	'htc_tattoo','samsung','symbian','SymbianOS','elaine','palm',
       		 'series60','windows ce','android','obigo','netfront',
       		 'openwave','mobilexplorer','operamini');
				var url_redirect = '<?php echo PATH_ROOT ?>';
						 ////
				function init()
				{	
				
					function mobiDetect(userAgent, devices) 
					{
						for(var i = 0; i < devices.length; i++) {
							if (userAgent.search(devices[i]) > 0) {
								return true;
							}
						}
						return false;
					}
					
					if (mobiDetect(userAgent, devices)) {
						
					}  
					else{
						window.location.href = url_redirect;
						}      
				}
				window.onload = init();
        </script>
  </head>
  <body class="mobile">
      <header class="header_mobile">
          <img src="<?php echo PATH_ROOT?>web-files/img/logo_big.jpg"/>
          <p>Construindo seu grande sonho</p>
          <div id="abre_mn_drop"><img src="<?php echo PATH_ROOT?>web-files/img/icon_menu.png"/></div>
      </header>
      <nav id="menu_mobile">
          <a href="<?php echo PATH_ROOT?>mobile/">Home</a>
          <a href="<?php echo PATH_ROOT?>mobile/empreendimentos">Empreendimentos</a>
          <a href="<?php echo PATH_ROOT?>mobile/sobre">Sobre a Big</a>
          <a href="<?php echo PATH_ROOT?>mobile/visite">Visite-nos</a>
          <a href="<?php echo PATH_ROOT ?>noticias">Notícias</a>
          <a href="<?php echo PATH_ROOT?>mobile/contato">Contato</a>
      </nav>
      <?php if($view_titulo=="home"):?>
      <img src="<?php echo PATH_ROOT?>web-files/img/banner_default_mb.jpg" id="destaque_home_mb"/>
      <h1 class="titulo_mob">Destaques</h1>
      <div id="bloco_destaques">
      <?php foreach($view_projetos as $prj_mb):
           $titulo =  explode("<t>", $prj_mb["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
     $sem_acento =  trim(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $titulo_longo ) ));
            $titleUrl = strtolower(preg_replace('/\s/', '-', $sem_acento));
          ?>      
      <a class="empreendimentos_mob_home" href="<?php echo PATH_ROOT."mobile/imovel/c/".$prj_mb['id']."/n/".$titleUrl?>">
          <img src="<?php echo PATH_ROOT."web-files/img/imgprojeto/".$prj_mb['thumb']?>" class="img_destaque"/>
          <div class="legenda_mob">
              <p><?php echo $titulo_curto?><br>
                  <span><?php echo $prj_mb['subtitulo']?></span><br>
                  <span><?php echo $prj_mb['dormitorio']?> Dorms</span><br>
                  <?php switch($prj_mb['estagio']){
                            case 1:$status = "Pré-lançamento";
                                break;
                            case 2:$status = "Lançamento";
                                break;
                            case 3:$status = "Em construção";
                                break;
                            case 4:$status = "Pronto para morar";
                                break;
                    }?>
                  <span><?php echo $status;?></span>
              </p></div>         
      </a>
      <?php endforeach;?>      
      </div>
      <h1 class="titulo_mob">Serviços</h1>
      <div id="bloco_servicos">
          <div id="content_srvicos_mb">
            <a href="<?php echo AREA_CLIENTE;?>" target="_blank" id="bt_cliente_mb">
                <img src="<?php echo PATH_ROOT?>web-files/img/bt_cliente_mb.jpg"/>
            </a>
            <a href="http://www.caixa.gov.br/novo_habitacao/minha-casa-minha-vida/index.asp" target="_blank" id="bt_caixa_mb">
                <img src="<?php echo PATH_ROOT?>web-files/img/bt_caixa_mb.jpg"/>
            </a>
          </div>
      </div>
      <div id="rodape_mobile">
          <div id="bts_sociais">
          <a href="<?php echo URL_FACE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" width="40"/></a>
          <a href="<?php echo URL_TWITTER;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" width="40"/></a>
          <a href="<?php echo URL_YOUTUBE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" width="40"/></a>
          <a href="<?php echo URL_INSTA;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" width="40"/></a>
          <a href="<?php echo URL_PINTEREST;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" width="40"/></a>
          </div>          
          <p class="desc_sobre_inverse">
              <img src="<?php echo PATH_ROOT?>web-files/img/logo_footer.png" width="40"/><br>
                                 Copyright © 2014<br>
                                 Big Construtora e Incorporadora<br>
                                 os os direitos reservados.<br>
                                 Rua Antonio Camardo, 577<br>
                                 Tatuapé - São Paulo - SP<br>
                                 CEP 03309-060
          </p>
      </div>
      <footer id="footer_contato_mb">
          <div id="conteudo_contato_footer">
          <div id="blc_whatsapp">
            	<div id="header_whatsapp"><p>Atendimento por WhatsApp</p><div id="close_popup">X</div></div>
                <p class="desc_whatsapp">Para conversar com um corretor, adicione o número: (11) 94244-3587 nos contatos, clique abaixo:</p>
                <a href="<?php echo PATH_ROOT."web-files/files/big.vcf";?>" class="add_contato">Salvar contato</a>
                <p class="desc_whatsapp">Após salvar o contato, clique abaixo para abrir o WhatsApp.</p>
                <a href="whatsapp://send?text=Olá Big Construtora" class="bt_whats_interna">Atendimento WhatssApp</a>
            </div>
              <a href="<?php echo PATH_ROOT?>mobile/ligamos" id="bt_ligamos_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligamos_mb.png"/></a>
            	<a href="tel:1120900301" id="bt_ligue_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligue_mb.png"/></a>
                <a href="javascript:void(0)" id="bt_whats_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_whatsapp_mb.png"/></a>
            	<a href="<?php echo PATH_ROOT?>mobile/contato" id="bt_email_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_email_mb.png"/></a>
          </div>
      </footer>
        <?php elseif($view_titulo=="empreendimentos"):?>
      <h1 class="titulo_mob">Empreendimentos</h1>
      <?php foreach($view_projetos as $prj_mb):
          $titulo =  explode("<t>", $prj_mb["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
    $sem_acento =  trim(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $titulo_longo ) ));
            $titleUrl = strtolower(preg_replace('/\s/', '-', $sem_acento));
switch($prj_mb['estagio']){
                            case 1:$status = "Pré-lançamento";
                                break;
                            case 2:$status = "Lançamento";
                                break;
                            case 3:$status = "Em construção";
                                break;
                            case 4:$status = "Pronto para morar";
                                break;
                    }?>
                  
 
      <a class="empreendimentos_mob" href="<?php echo PATH_ROOT."mobile/imovel/c/".$prj_mb['id']."/n/".$titleUrl?>">
          <img src="<?php echo PATH_ROOT."web-files/img/imgprojeto/".$prj_mb['img_destaque']?>" class="img_destaque"/>
          <div class="legenda_mob"><p><?php echo $titulo_curto?> > <span><?php echo $prj_mb['subtitulo']?> > </span><span><?php echo $status;?></span></p></div>
      </a>
      <?php endforeach;?>
      <div id="rodape_mobile">
          <div id="bts_sociais">
          <a href="<?php echo URL_FACE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" width="40"/></a>
          <a href="<?php echo URL_TWITTER;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" width="40"/></a>
          <a href="<?php echo URL_YOUTUBE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" width="40"/></a>
          <a href="<?php echo URL_INSTA;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" width="40"/></a>
          <a href="<?php echo URL_PINTEREST;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" width="40"/></a>
          </div>          
          <p class="desc_sobre_inverse">
              <img src="<?php echo PATH_ROOT?>web-files/img/logo_footer.png" width="40"/><br>
                                 Copyright © 2014<br>
                                 Big Construtora e Incorporadora<br>
                                 os os direitos reservados.<br>
                                 Rua Antonio Camardo, 577<br>
                                 Tatuapé - São Paulo - SP<br>
                                 CEP 03309-060
          </p>
      </div>
      <footer id="footer_contato_mb">
          <div id="conteudo_contato_footer">
          	<div id="blc_whatsapp">
            	<div id="header_whatsapp"><p>Atendimento por WhatsApp</p><div id="close_popup">X</div></div>
                <p class="desc_whatsapp">Para conversar com um corretor, adicione o número: (11) 94244-3587 nos contatos, clique abaixo:</p>
                <a href="<?php echo PATH_ROOT."web-files/files/big.vcf";?>" class="add_contato">Salvar contato</a>
                <p class="desc_whatsapp">Após salvar o contato, clique abaixo para abrir o WhatsApp.</p>
                <a href="whatsapp://send?text=Olá Big Construtora" class="bt_whats_interna">Atendimento WhatssApp</a>
            </div>
              <a href="<?php echo PATH_ROOT?>mobile/ligamos" id="bt_ligamos_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligamos_mb.png"/></a>
            <a href="tel:1120900301" id="bt_ligue_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligue_mb.png"/></a>
            <a href="javascript:void(0)" id="bt_whats_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_whatsapp_mb.png"/></a>
            <a href="<?php echo PATH_ROOT?>mobile/contato" id="bt_email_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_email_mb.png"/></a>
          </div>
      </footer>
      <?php elseif($view_titulo=="sobre"):?>
      <h1 class="titulo_mob">Sobre a Big</h1>
      <div class="empreendimentos_mob">
          <p class="desc_sobre">A <strong>BIG Construtora e Incorporadora S/A</strong>, surgiu do sonho de oferecer ao público 
                                empreendimentos diferenciados com qualidade e preço justo. Foi com o 
                                ingresso no Programa do Governo Federal <strong>MCMV</strong> (Minha Casa, Minha Vida), 
                                parceria com investidores, bancos e agentes de financiamento que esse sonho 
                                tornou-se realidade.
            </p>
            <img src="<?php echo PATH_ROOT?>web-files/img/destaque_sobre.jpg"/>
            <p class="desc_sobre">Desde sua fundação em 2008 a Big Construtora e Incorporadora desenvolveu,
                                entre unidades concluídas, em construção e em lançamento, <strong>962 unidades 
                                    habitacionais</strong> na cidade de São Paulo, nos bairros do Tremembé (Zona Norte) e 
                                Tatuapé (Zona Leste) e na cidade vizinha de Ferraz de Vasconcelos, nos bairros 
                                do Cambiri e Vila São Paulo.<br><br>
                                Contamos com uma equipe de profissionais comprometidos e altamente 
                                capacitados tecnicamente nas áreas: administrativa, incorporação, vendas, 
                                jurídica, correspondente bancário, engenharia e construção, que acompanham e dão 
                                suporte desde a aquisição do terreno, financiamento, até o período pós-entrega final do imóvel ao cliente.<br><br>
                                A <strong>BIG CONSTRUTORA</strong> faz uso de um sitema integrado de gestão que visa a interligação 
                                de todos os processos da empresa, com o objetivo de gerar agilidade, confiabilidade e transparência nas informações, sistema esse 
                                reconhecido com a obtenção das certificações: Programa de Qualidade e 
                                Produtividade no Habitat, <strong>PBQP-H nível A e ISO 9001:2015</strong>.
            </p>                      
      </div>
      <div id="rodape_mobile">
          <div id="bts_sociais">
          <a href="<?php echo URL_FACE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" width="40"/></a>
          <a href="<?php echo URL_TWITTER;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" width="40"/></a>
          <a href="<?php echo URL_YOUTUBE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" width="40"/></a>
          <a href="<?php echo URL_INSTA;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" width="40"/></a>
          <a href="<?php echo URL_PINTEREST;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" width="40"/></a>
          </div>          
          <p class="desc_sobre_inverse">
              <img src="<?php echo PATH_ROOT?>web-files/img/logo_footer.png" width="40"/><br>
                                 Copyright © 2014<br>
                                 Big Construtora e Incorporadora<br>
                                 os os direitos reservados.<br>
                                 Rua Antonio Camardo, 577<br>
                                 Tatuapé - São Paulo - SP<br>
                                 CEP 03309-060
          </p>
      </div>
      <footer id="footer_contato_mb">
          <div id="conteudo_contato_footer">
          <div id="blc_whatsapp">
            	<div id="header_whatsapp"><p>Atendimento por WhatsApp</p><div id="close_popup">X</div></div>
                <p class="desc_whatsapp">Para conversar com um corretor, adicione o número: (11) 94244-3587 nos contatos, clique abaixo:</p>
                <a href="<?php echo PATH_ROOT."web-files/files/big.vcf";?>" class="add_contato">Salvar contato</a>
                <p class="desc_whatsapp">Após salvar o contato, clique abaixo para abrir o WhatsApp.</p>
                <a href="whatsapp://send?text=Olá Big Construtora" class="bt_whats_interna">Atendimento WhatssApp</a>
            </div>
              <a href="<?php echo PATH_ROOT?>mobile/ligamos" id="bt_ligamos_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligamos_mb.png"/></a>
            <a href="tel:1120900301" id="bt_ligue_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligue_mb.png"/></a>
            <a href="javascript:void(0)" id="bt_whats_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_whatsapp_mb.png"/></a>
            <a href="<?php echo PATH_ROOT?>mobile/contato" id="bt_email_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_email_mb.png"/></a>
          </div>
      </footer>
      <?php 
        elseif($view_titulo=="visite"):
      ?>
      <h1 class="titulo_mob">Visite-nos</h1>
      <div class="empreendimentos_mob_visite">
          <p class="desc_sobre"><strong>Horário de Funcionamento:</strong><br>
            Segunda a Sexta, das 8 às 17h30<br><br>
            <strong>Sede:</strong> Rua Antônio camardo, 577, Tatuapé, SP<br>
            <strong>Telefone (sede):</strong> (11) 2090-0301 <br><br>
            <!--<strong>Loja:</strong> Avenida Brasil, 767, Centro, Ferraz de Vasconcelos, SP<br>-->
            <!--<strong>Telefone (loja):</strong> (11) 4200-2287-->
            </p>
            <a href="http://maps.google.com/maps?q=Rua Antônio camardo, 577, Tatuapé" class="btn"><em class="icon-plus-sign"></em> Ver Mapa da Sede</a>
            <!--<a href="http://maps.google.com/maps?q=Avenida Brasil, 767, Centro, Ferraz de Vasconcelos" class="btn"><em class="icon-plus-sign"></em> Ver Mapa da Loja</a>-->
      </div>
      <div id="rodape_mobile">
          <div id="bts_sociais">
          <a href="<?php echo URL_FACE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" width="40"/></a>
          <a href="<?php echo URL_TWITTER;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" width="40"/></a>
          <a href="<?php echo URL_YOUTUBE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" width="40"/></a>
          <a href="<?php echo URL_INSTA;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" width="40"/></a>
          <a href="<?php echo URL_PINTEREST;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" width="40"/></a>
          </div>          
          <p class="desc_sobre_inverse">
              <img src="<?php echo PATH_ROOT?>web-files/img/logo_footer.png" width="40"/><br>
                                 Copyright © 2014<br>
                                 Big Construtora e Incorporadora<br>
                                 os os direitos reservados.<br>
                                 Rua Antonio Camardo, 577<br>
                                 Tatuapé - São Paulo - SP<br>
                                 CEP 03309-060
          </p>
      </div>
      <footer id="footer_contato_mb">
          <div id="conteudo_contato_footer">
          <div id="blc_whatsapp">
            	<div id="header_whatsapp"><p>Atendimento por WhatsApp</p><div id="close_popup">X</div></div>
                <p class="desc_whatsapp">Para conversar com um corretor, adicione o número: (11) 94244-3587 nos contatos, clique abaixo:</p>
                <a href="<?php echo PATH_ROOT."web-files/files/big.vcf";?>" class="add_contato">Salvar contato</a>
                <p class="desc_whatsapp">Após salvar o contato, clique abaixo para abrir o WhatsApp.</p>
                <a href="whatsapp://send?text=Olá Big Construtora" class="bt_whats_interna">Atendimento WhatssApp</a>
            </div>
              <a href="<?php echo PATH_ROOT?>mobile/ligamos" id="bt_ligamos_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligamos_mb.png"/></a>
            <a href="tel:1120900301" id="bt_ligue_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligue_mb.png"/></a>
            <a href="javascript:void(0)" id="bt_whats_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_whatsapp_mb.png"/></a>
            <a href="<?php echo PATH_ROOT?>mobile/contato" id="bt_email_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_email_mb.png"/></a>
          </div>
      </footer>
       <?php 
        elseif($view_titulo=="contato"):
      ?>
      <h1 class="titulo_mob">Contato</h1>
      <div class="empreendimentos_mob">
          <form name="form_mobile" method="post" action="">
              <select id="mob_assunto">
                  <option selected="selected" disabled="disabled" value="0">Selecione o Assunto:</option>
                    <option value="Relacionamento Com o Cliente">Relacionamento Com o Cliente</option>
                    <option value="Informações Sobre Empreendimento">Informações Sobre Empreendimento</option>
                    <option value="Fornecedor">Fornecedor</option>
                    <option value="Outros">Outros</option>
              </select>
              <div class="control-group">
                  <label for="mb_nome">Nome:</label>
                  <div class="controls">
                      <input type="text" id="mb_nome"/>
                  </div>
              </div>
              <div class="control-group">
                  <label for="mb_email">Email:</label>
                  <div class="controls">
                      <input type="text" id="mb_email"/>
                  </div>
              </div>
              <div class="control-group">
                  <label for="mb_email">Telefone:</label>
                  <div class="controls">
                      <input type="text" id="mb_telefone"/>
                  </div>
              </div>
              <select id="mb_projeto">
                  <option disabled="disabled" selected="selected" value="0">Empreendimento</option>
                  <?php foreach($view_contato as $projeto):
                      $titulo =  explode("<t>", $projeto["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
                      ?>
                        <option value="<?php echo $titulo_longo;?>"><?php echo $titulo_longo;?></option>
                  <?php endforeach;?>
              </select>
              <div class="control-group" id="box_torre">
                  <label for="mb_email">Torre:</label>
                  <div class="controls">
                      <input type="text" id="mb_torre"/>
                  </div>
              </div>
              <div class="control-group" id="box_unidade">
                  <label for="mb_email">Unidade:</label>
                  <div class="controls">
                      <input type="text" id="mb_unidade"/>
                  </div>
              </div>
              <div class="control-group">
                  <label for="mb_email">Mensagem:</label>
                  <div class="controls">
                      <textarea type="text" id="mb_mensagem" rows="10"></textarea>
                  </div>
              </div>
              <input type="submit" value="Enviar" class="btn" id="bt_mb_contato"/>
          </form>
      </div>
      <div id="rodape_mobile">
          <div id="bts_sociais">
          <a href="<?php echo URL_FACE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" width="40"/></a>
          <a href="<?php echo URL_TWITTER;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" width="40"/></a>
          <a href="<?php echo URL_YOUTUBE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" width="40"/></a>
          <a href="<?php echo URL_INSTA;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" width="40"/></a>
          <a href="<?php echo URL_PINTEREST;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" width="40"/></a>
          </div>          
          <p class="desc_sobre_inverse">
              <img src="<?php echo PATH_ROOT?>web-files/img/logo_footer.png" width="40"/><br>
                                 Copyright © 2014<br>
                                 Big Construtora e Incorporadora<br>
                                 os os direitos reservados.<br>
                                 Rua Antonio Camardo, 577<br>
                                 Tatuapé - São Paulo - SP<br>
                                 CEP 03309-060
          </p>
      </div>
      <footer id="footer_contato_mb">
          <div id="conteudo_contato_footer">
          <div id="blc_whatsapp">
            	<div id="header_whatsapp"><p>Atendimento por WhatsApp</p><div id="close_popup">X</div></div>
                <p class="desc_whatsapp">Para conversar com um corretor, adicione o número: (11) 94244-3587 nos contatos, clique abaixo:</p>
                <a href="<?php echo PATH_ROOT."web-files/files/big.vcf";?>" class="add_contato">Salvar contato</a>
                <p class="desc_whatsapp">Após salvar o contato, clique abaixo para abrir o WhatsApp.</p>
                <a href="whatsapp://send?text=Olá Big Construtora" class="bt_whats_interna">Atendimento WhatssApp</a>
            </div>
              <a href="<?php echo PATH_ROOT?>mobile/ligamos" id="bt_ligamos_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligamos_mb.png"/></a>
            <a href="tel:1120900301" id="bt_ligue_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligue_mb.png"/></a>
            <a href="javascript:void(0)" id="bt_whats_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_whatsapp_mb.png"/></a>
            <a href="<?php echo PATH_ROOT?>mobile/contato" id="bt_email_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_email_mb.png"/></a>
          </div>
      </footer>
      <?php elseif($view_titulo=="ligamos"):?>
      <h1 class="titulo_mob">Nós Ligamos</h1>
      <div class="empreendimentos_mob">
          <p class="desc_sobre">Informe seu número e aguarde o nosso retorno.</p>
          <form name="form_mobile_ligamos" method="post" action="">
              <div class="control-group" id="box_unidade">
                  <label for="mb_email">Nome:</label>
                  <div class="controls">
                      <input type="text" id="nome_ligamos_mb"/>
                  </div>
              </div>
              <div class="control-group" id="box_unidade">
                  <label for="mb_email">Telefone:</label>
                  <div class="controls">
                      <input type="text" id="tel_ligamos_mb" placeholder="99 999999999"/>
                  </div>
              </div>
              <input type="submit" value="Enviar" class="btn" id="bt_mb_ligamos"/>             
          </form>
          <p class="desc_sobre">Horário de atendimento:<br>
              Segunda a Sexta, das 8 às 17h30</p>
      </div>
      <div id="rodape_mobile">
          <div id="bts_sociais">
          <a href="<?php echo URL_FACE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" width="40"/></a>
          <a href="<?php echo URL_TWITTER;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" width="40"/></a>
          <a href="<?php echo URL_YOUTUBE;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" width="40"/></a>
          <a href="<?php echo URL_INSTA;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" width="40"/></a>
          <a href="<?php echo URL_PINTEREST;?>" target="_blank"><img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" width="40"/></a>
          </div>          
          <p class="desc_sobre_inverse">
              <img src="<?php echo PATH_ROOT?>web-files/img/logo_footer.png" width="40"/><br>
                                 Copyright © 2014<br>
                                 Big Construtora e Incorporadora<br>
                                 os os direitos reservados.<br>
                                 Rua Antonio Camardo, 577<br>
                                 Tatuapé - São Paulo - SP<br>
                                 CEP 03309-060
          </p>
      </div>
      <footer id="footer_contato_mb">
          <div id="conteudo_contato_footer">
          <div id="blc_whatsapp">
            	<div id="header_whatsapp"><p>Atendimento por WhatsApp</p><div id="close_popup">X</div></div>
                <p class="desc_whatsapp">Para conversar com um corretor, adicione o número: (11) 94244-3587 nos contatos, clique abaixo:</p>
                <a href="<?php echo PATH_ROOT."web-files/files/big.vcf";?>" class="add_contato">Salvar contato</a>
                <p class="desc_whatsapp">Após salvar o contato, clique abaixo para abrir o WhatsApp.</p>
                <a href="whatsapp://send?text=Olá Big Construtora" class="bt_whats_interna">Atendimento WhatssApp</a>
            </div>
              <a href="<?php echo PATH_ROOT?>mobile/ligamos" id="bt_ligamos_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligamos_mb.png"/></a>
            <a href="tel:1120900301" id="bt_ligue_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_ligue_mb.png"/></a>
            <a href="javascript:void(0)" id="bt_whats_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_whatsapp_mb.png"/></a>
            <a href="<?php echo PATH_ROOT?>mobile/contato" id="bt_email_mb"><img src="<?php echo PATH_ROOT?>web-files/img/bt_email_mb.png"/></a>
          </div>
      </footer>
      <?php endif;?>
   </body>
   <script src="<?php echo PATH_ROOT?>web-files/js/jquery-1.10.2.min.js"></script>
   <script src="<?php echo PATH_ROOT?>web-files/js/bootstrap.min.js"></script>
   <script>
       function largura_elemento(){
           var largura_destaque = $(".img_destaque").width();
           $(".legenda_mob").css("width",largura_destaque);
       }
       $(document).ready(function(){
		   $("#bt_whats_mb").click(function(){
			   $("#blc_whatsapp").show();
			});
			$("#close_popup").click(function(){
				$("#blc_whatsapp").hide();
			});
           $("#bt_ligamos_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_nosligamos_home');
            });
            
           $("#bt_ligue_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_ligue_home');
            });
            $("#bt_email_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_envie_duvida_home');
            });
			$("#bt_whats_mb").click(function(){
                ga('send', 'event', 'button', 'click', 'mb_whatsapp_home');
            });
           $(window).load(function(){
               $(window).resize(largura_elemento);
                largura_elemento();
           });
                
           var flag = false;
           $("#abre_mn_drop img").click(function(){
               flag=!flag;
               if(flag){
                   
               $("#menu_mobile").animate({marginTop:0},500);
           }else{
               $("#menu_mobile").animate({marginTop:-165},500);
           }
           });
           $("#mob_assunto").change(function(){
               if($(this).val()=="Relacionamento Com o Cliente"){
                   $("#mb_projeto").show();
                   $("#box_torre").show();
                   $("#box_unidade").show();
               }
               else if($(this).val()=="Informações Sobre Empreendimento"){
                   $("#mb_projeto").show();
                   $("#box_torre").hide();
                   $("#box_unidade").hide();
               }
               else if($(this).val()=="Fornecedor"){
                   $("#mb_projeto").hide();
                   $("#box_torre").hide();
                   $("#box_unidade").hide();
               }
                else if($(this).val()=="Outros"){
                   $("#mb_projeto").hide();
                   $("#box_torre").hide();
                   $("#box_unidade").hide();
               }
           });
           $("#bt_mb_ligamos").click(function(e){
               e.preventDefault();
               var nome = $("#nome_ligamos_mb").val().trim();
               var tel = $("#tel_ligamos_mb").val().trim();
               var urlDirect = "<?php echo PATH_ROOT."mobile/ligamos/do/action/"?>";
               if(nome==""){
                   alert("Digite seu nome");
               }
               else if(tel==""){
                   alert("Digite seu telefone");
               }
               else{
                    $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{nome:nome,tel:tel},
                    success:function(data){
                        if(data == "ok"){
                            ga('send', 'event', 'formulario', 'envio', 'mb_nos_ligamos');
                            alert("Agradecemos o seu contato\nAguarde retorno!");
                            document.form_mobile_ligamos.reset();
                        }
                        else{
                            alert("Erro ao enviar");
                        }
                    }
                    });
               }
           });
           $("#bt_mb_contato").click(function(e){
               e.preventDefault();
               var assunto_contato = $("#mob_assunto option:selected").val();
               var nome = $("#mb_nome").val().trim();
               var email = $("#mb_email").val().trim();        
               var tel = $("#mb_telefone").val().trim();
               var empreendimento = $("#mb_projeto option:selected").val();
              var torre = $("#mb_torre").val().trim();
            var unidade = $("#mb_unidade").val().trim();
            var mensagem = $("#mb_mensagem").val().trim();
            var urlDirect = "<?php echo PATH_ROOT."mobile/envio/"?>";
            if(assunto_contato==0){
                alert("Escolha um assunto");
            }
            else if(nome==""){
                alert("Digite seu nome");
            }
            else if(email=="" || email.indexOf('@')== -1 || email.indexOf('.')== -1){
                alert("Digite um email válido");
            }
            else if(assunto_contato=="Relacionamento Com o Cliente"){                
                if(empreendimento==0){
                        alert("Escolha um empreendimento");
                    }
                else if(torre==""){
                    alert("Digite a torre");
                }
                else if(unidade==""){
                    alert("Digite a unidade");
                }
                else if(mensagem==""){
                    alert("Digite a mensagem");
                }
                else{
                    $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{assunto_contato:assunto_contato,tel:tel,nome:nome,email:email,empreendimento:empreendimento,torre:torre,unidade:unidade,mensagem:mensagem},
                    success:function(data){
                        if(data == "ok"){
                            ga('send', 'event', 'formulario', 'envio', 'mb_relacionamento_cliente');
                            alert("Enviado com sucesso");
                            document.form_mobile.reset();
                        }
                        else{
                            alert("Erro ao enviar");
                        }
                    }
                    });
                }
                }
            else if(assunto_contato=="Informações Sobre Empreendimento"){
                if(empreendimento==0){
                    alert("Escolha um empreendimento");
                }
                else if(mensagem==""){
                    alert("Digite a mensagem");
                }
                else{
                    $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{assunto_contato:assunto_contato,tel:tel,nome:nome,email:email,empreendimento:empreendimento,mensagem:mensagem},
                    success:function(data){
                        if(data=="ok"){
                            ga('send', 'event', 'formulario', 'envio', 'mb_info_empreendimento');
                            alert("Enviado com sucesso");
                            document.form_mobile.reset();
                        }
                        else{
                            alert("Erro ao enviar");
                        }
                    }
                    });
                }
            }
            else if(mensagem==""){
                alert("Digite a mensagem");
            }
            else{
                $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{assunto_contato:assunto_contato,tel:tel,nome:nome,email:email,mensagem:mensagem},
                    success:function(data){
                       if(data== "ok"){
                           ga('send', 'event', 'formulario', 'envio', 'mb_form_outros');
                            alert("Enviado com sucesso");
                            document.form_mobile.reset();
                        }
                        else{
                            alert("Erro ao enviar");
                        }
                    }
                    });
            }
            
        });
       });
   </script>
</html>

