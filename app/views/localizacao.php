<?php
$title = " | Visite-nos";
$body = "visite";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj" class="sobre">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                Visite-nos
            </p>
            <h1 id="titulo_interna_prj">Visite-nos</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="localhost/big_local/localizacao/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="localhost/big_local/localizacao/" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div> 
        <div id="bloco_chamada_destaque">
            <h1 id="txt_chamada_destaque">Venha nos visitar<br>Temos satisfação em atendê-lo.</h1>
        </div>
        <div id="container_sobre">
        <div class="sidebar_esq">
            <!--<h3 class="header_sidebar_esq" id="bt_mapa_loja"><em class="icon-plus-sign"></em> Ver Mapa da Loja <em class="icon-map-marker" id="marker_loja"></em></h3>-->
            <!--<a href="javascript:void(0)" class="ancora_sobre" id="mapa_loja">Como Chegar</a>-->
            <!--<div id="rota_loja">-->
            <!--    <div class="control-group">-->
            <!--        <p class="ancora_sobre">Origem:</p>-->
            <!--        <div class="ancora_sobre controls">-->
            <!--            <input type="text" class="span3" id="campo_rota_loja"/>-->
            <!--            <button class="btn" id="bt_rota_loja">Traçar Rota</button>-->
            <!--        </div>-->
                    
            <!--    </div>-->
            <!--</div>-->
            <h3 class="header_sidebar_esq" id="bt_mapa_sede"><em class="icon-plus-sign"></em> Ver Mapa da Sede <em id="marker_sede"></em></h3>
            <a href="javascript:void(0)" class="ancora_sobre" id="mapa_sede">Como Chegar</a>
            <div id="rota_sede">
                <div class="control-group">
                    <p class="ancora_sobre">Origem:</p>
                    <div class="ancora_sobre controls">
                        <input type="text" class="span3" id="campo_rota_sede"/>
                        <button class="btn" id="bt_rota_sede">Traçar Rota</button>
                    </div>
                    
                </div>
            </div>
        </div>
        <div id="content_sobre">
            <div id="mapa_visite">
            
            </div>
            <p class="desc_sobre" id="sede_hora"><strong>Horário de Funcionamento:</strong><br>
            <strong>Sede:</strong> Segunda a Sexta, das 8 às 17h30<br>
            <strong>Telefone:</strong> (11) 2090-0301<br>
            <strong>Endereço:</strong> Rua Antônio Camardo, 577, Tatuapé, SP
            </p>
            <!--<p class="desc_sobre" id="loja_hora"><strong>Horário de Funcionamento:</strong><br>-->
            <!--<strong>Loja:</strong> Segunda a Domingo, das 8h30 às 20h<br>-->
            <!--<strong>Telefone:</strong> (11) 4200-2287<br>-->
            <!--<strong>Endereço:</strong> Avenida Brasil, 765, Centro, Ferraz de Vasconcelos, SP-->
            <!--</p>-->
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
<script>  
var posicao_sede = '"-23.548765", "-46.562369"';
var posicao_loja = '"-23.537434", "-46.362449"';
var map;
var markers=[];
var directionsService = new google.maps.DirectionsService();
var marker = new google.maps.Marker({
                icon:'<?php echo PATH_ROOT?>web-files/img/pin_big_pequeno.png',
                position: new google.maps.LatLng("-23.537434", "-46.362449")
        });
        var marker2 = new google.maps.Marker({
            icon:'<?php echo PATH_ROOT?>web-files/img/pin_big_pequeno.png',
                position: new google.maps.LatLng("-23.548785", "-46.562326")
        });
function initialize() {	
    var options = {
        zoom: 16,
        center: marker.position,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("mapa_visite"), options);

    marker.setMap(map);
   
}
google.maps.event.addDomListener(window, 'load', initialize);
   $(document).ready(function(){               
       $("#bt_mapa_loja").click(function(){
           $("#marker_loja").addClass("icon-map-marker");
           $("#marker_sede").removeClass("icon-map-marker");
           $("#loja_hora").show();
           $("#sede_hora").hide();
           marker2.setMap(null);
           var marker = new google.maps.Marker({
               icon:'<?php echo PATH_ROOT?>web-files/img/pin_big_pequeno.png',
                position: new google.maps.LatLng("-23.537434", "-46.362449")
            });
            var options = {
                zoom: 16,
                center: marker.position,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("mapa_visite"), options);

            marker.setMap(map);
       });
       $("#bt_mapa_sede").click(function(){
       $("#marker_loja").removeClass("icon-map-marker");
           $("#marker_sede").addClass("icon-map-marker");
         $("#loja_hora").hide();
           $("#sede_hora").show();
           marker.setMap(null);
           var marker2 = new google.maps.Marker({
               icon:'<?php echo PATH_ROOT?>web-files/img/pin_big_pequeno.png',
                position: new google.maps.LatLng("-23.548785", "-46.562326")
            });
            var options = {
                zoom: 16,
                center: marker2.position,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("mapa_visite"), options);

            marker2.setMap(map);
       }); 
       
       $("#mapa_loja").click(function(){          
               $("#rota_loja").toggle();         
       });
       $("#mapa_sede").click(function(){
               $("#rota_sede").toggle(); 
       });
       $("#bt_rota_loja").click(function(){         
           var campoL = $("#campo_rota_loja").val();
           if(campoL==""){
               alert("Digite a origem para Traçar Rota!");
           }else{
               marker.setMap(null);
               marker2.setMap(null);
               var directionsDisplay = new google.maps.DirectionsRenderer();
            
            var request = {
                origin: campoL,
                destination: marker.position,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
           directionsService.route(request,function(response,status){
               if(status == google.maps.DirectionsStatus.OK){
                   directionsDisplay.setDirections(response);
                   directionsDisplay.setMap(map);
               }
           });
           }
       });
       $("#bt_rota_sede").click(function(){        
           var campoS = $("#campo_rota_sede").val();
           if(campoS==""){
               alert("Digite a origem para Traçar Rota!");
           }else{
                marker.setMap(null);
               marker2.setMap(null);
               var directionsDisplay = new google.maps.DirectionsRenderer();
            
            var request = {
                origin: campoS,
                destination: marker2.position,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
           directionsService.route(request,function(response,status){
               if(status == google.maps.DirectionsStatus.OK){
                   directionsDisplay.setDirections(response);
                   directionsDisplay.setMap(map);
               }
           });
           }
       });
   });
</script>


