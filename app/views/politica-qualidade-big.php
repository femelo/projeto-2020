<?php
$title = " | Política de Qualidade Big";
$body = "politica-qualidade";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                <a href="<?php echo PATH_ROOT?>sobre/">Sobre a Big</a> >
                Política da Qualidade Big
            </p>
            <h1 id="titulo_interna_prj">Política da Qualidade Big</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/sobre/certificacoes/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/sobre/certificacoes/" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div> 
        <div id="bloco_chamada_destaque">
            <h1 id="txt_chamada_destaque">Big Construtora<br>Construindo o seu grande sonho.</h1>
        </div>
        <div id="container_sobre">
        <div class="sidebar_esq">
            <h3 class="header_sidebar_esq">Sobre a Big</h3>
            <div id="filtro_estagio">
                <a href="/sobre#missao" class="ancora_sobre">Missão</a>
                <a href="/sobre#visao" class="ancora_sobre">Visão</a>
                <a href="/sobre#valores" class="ancora_sobre">Valores</a>
            </div>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>sobre/certificacoes">Certificações</a>
            <a class="ancora_sobre" href="javascript:void(0)"><strong>Política da Qualidade Big</strong></a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>noticias/">Notícias</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>localizacao/">Localização</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>contato/">Fale Conosco</a>
           
        </div>
        <div id="content_sobre">  
            <h3 style="margin-top:0;padding-left: 15px;">Política da Qualidade Big</h3>
            <p class="desc_sobre">O compromisso com nossos clientes deve refletir-se no respeito aos seus direitos e na busca de soluções que atendam aos seus interesses, através da construção de empreendimentos que garantam sua qualidade de vida e com respeito a comunidade, ao meio ambiente e aos requisitos regulamentares da construção civil, sempre em harmonia com os objetivos de desenvolvimento e rentabilidade da empresa. Para tanto, temos como fundamentos:</p>
            <p class="desc_sub" style="text-align: center;margin-bottom: 15px;"><strong>CLIENTE SATISFEITO e MELHORIA CONTÍNUA</strong></p>
            <img src="<?php echo PATH_ROOT?>web-files/img/img-politica-qualidade.jpg" alt="Política de Qualidade Big"/> 
            <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>        
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
