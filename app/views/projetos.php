<?php
$title = "| Empreendimentos";
$body = "empreendimentos";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj" class="empreendimentos">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                Empreendimentos
            </p>
            <h1 id="titulo_interna_prj">Empreendimentos</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/empreendimentos/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/empreendimentos/" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div>
        <div id="bloco_qtd_imoveis">
            <h1 id="txt_qtd_imoveis">Foram encontrados<br><span><?php echo (sizeof($view_projeto)==1)? sizeof($view_projeto)." Imóvel":sizeof($view_projeto)." Imóveis"?></span></h1>
        </div>
        <div id="container_empreendimentos">
        <div class="sidebar_esq">
            <h3 class="header_sidebar_esq">Pesquisa Detalhada</h3>
            <h3 class="titulo_filtros">Estágios da Obra</h3>
            <div class="control-group" id="filtro_estagio">
                <label class="radio">
                    <input type="radio" id="" value="1" name="r_filtroestagio"> Pré-lançamento
                </label>
                <label class="radio">
                    <input type="radio" id="" value="2" name="r_filtroestagio"> Lançamento
                </label>
                <label class="radio">
                    <input type="radio" id="" value="3" name="r_filtroestagio"> Em construção
                </label>
                <label class="radio">
                    <input type="radio" id="" value="4" name="r_filtroestagio"> Pronto para Morar
                </label>
            </div>
            <h3 class="titulo_filtros">Dormitórios</h3>
            <div class="control-group" id="filtro_dorms">
                <label class="radio">
                    <input type="radio" id="" value="1" name="r_filtrodorms"> 1 dormitório
                </label>
                <label class="radio">
                    <input type="radio" id="" value="2" name="r_filtrodorms"> 2 dormitórios
                </label>
                <label class="radio">
                    <input type="radio" id="" value="3" name="r_filtrodorms"> 3 dormitórios
                </label>
                <label class="radio">
                    <input type="radio" id="" value="4" name="r_filtrodorms"> 4 dormitórios
                </label>
            </div>
            <!--<h3 class="titulo_filtros">Tipo</h3>-->
            <p class="alert alert-error span2" id="remove_filtro">
                <strong>X  Remover Filtro</strong></p>
        </div>
        <div id="content_empreendimentos">
            
            <?php
            if(sizeof($view_projeto)==0):
                echo "<p class='alert alert-info span4'>Nenhum resultado encontrado</p>";
            else:
            foreach($view_projeto as $prj):
                switch($prj['estagio']){
                case 1:$status = "Pré-lançamento";
                    break;
                case 2:$status = "Lançamento";
                    break;
                case 3:$status = "Em construção";
                    break;
                case 4:$status = "Pronto para morar";
                    break;
            }
             $titulo =  explode("<t>", $prj["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
            $sem_acento =  trim(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $titulo_longo ) ));
            $titleUrl = strtolower(preg_replace('/\s/', '-', $sem_acento));
            
            ?>
            <div class="destaque_empreendimentos">
                <img src="<?php echo PATH_ROOT?>web-files/img/imgprojeto/<?php echo $prj['img_destaque'];?>"/>
                <div id="legenda_destaque">
                    <p id="estagio_prj"><?php echo $status;?></p>
                    <div class="titulo-box-destaque">
                    	<h1><?php echo $titulo_curto?></h1>
                        <p style="color:<?php echo $prj['paleta_subtitulo']?>"><?php echo $prj['subtitulo'];?></p>
                    </div>
                    <p id="itens_destaque"><?php echo $prj['tipoprj_nome']?><br><?php echo ($prj['dormitorio']==1)?$prj['dormitorio']." Dorm":$prj['dormitorio']." Dorms";?><br><?php echo $prj['area']?>m²</p>
                    <a href="<?php echo PATH_ROOT."empreendimentos/imovel/c/".$prj['id']."/n/".$titleUrl;?>" class="btn btn-prj">CLIQUE E CONHEÇA</a>
                    
                </div>
            </div>
            <?php endforeach;
            endif;?>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
<script> 
function carrega_interna(caminho){
        document.location.href="<?php echo PATH_ROOT?>empreendimentos/imovel/"+caminho;
    }
   $(document).ready(function(){   
      $("#remove_filtro").click(function(){
          document.location.reload();
      });
        $("input[name='r_filtrodorms']").click(function(){
           var valdorms = $(this).val();
           var valestagio = $("input[name='r_filtroestagio']:checked").val();
           var urlDirect = "<?php echo PATH_ROOT?>empreendimentos/filtro/";
           if(valestagio != undefined){               
                $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{valdorms:valdorms,valestagio:valestagio},
                    success:function(data){
                        var dados = JSON.parse(data);
                        var valor = "";
                        if(dados==""){
                            criaResultado(); 
                         }else{var estagio_nome;
                            for(var i=0;i < dados.length;i++){
                                
                             if(dados[i].estagio==1){estagio_nome="Pré-lançamento";}
                                else if(dados[i].estagio==2){estagio_nome="Lançamento";}
                                else if(dados[i].estagio==3){estagio_nome="Em contrução";}
                                else if(dados[i].estagio==4){estagio_nome="Pronto para morar";}
                                var titulo = dados[i].titulo.split("<t>");
                                var titulo_longo;
                                var titulo_curto;
                                if(titulo[1]==undefined){
                                    titulo_curto = titulo[0];
                                    titulo_longo = titulo[0];
                                }else{
                                    titulo_curto = titulo[1];
                                    titulo_longo = titulo[0]+" "+titulo[1];
                                }
                                var url1 = titulo_longo.replace(/[' ']/g,'-');
                                var urlfinal = url1.replace(/[áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ]/g,'aaaaeeiooouuncAAAAEEIOOOUUNC-').toLowerCase();
                                valor += '<div class="destaque_empreendimentos"><img src="<?php echo PATH_ROOT?>web-files/img/imgprojeto/'+dados[i].img_destaque+'"/>';
                                valor += '<div id="legenda_destaque"><p id="estagio_prj">'+estagio_nome+'</p><div class="titulo-box-destaque"><h1>'+titulo_curto+'</h1>';
                                valor += '<p style="color:'+dados[i].paleta_subtitulo+'">'+dados[i].subtitulo+'</p></div>';
                                valor += '<p id="itens_destaque">'+dados[i].tipoprj_nome+'<br>'+dados[i].dormitorio+' Dorms<br>'+dados[i].area+'m²</p>'; 
                                valor += '<a href="<?php echo PATH_ROOT?>empreendimentos/imovel/c/'+dados[i].id+'/n/'+urlfinal+'" class="btn btn-prj" rel="'+titulo_longo+'">CLIQUE E CONHEÇA</a></div></div>'; 
                            }
                            $("#content_empreendimentos").html(valor);
                             var imovel = "";
                            if(dados.length > 1){imovel=" Imóveis";}else{imovel=" Imóvel";}
                            $("#txt_qtd_imoveis span").html(dados.length+imovel);
                        }
                    }
                });   
                
            }else{
               $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{valdorms:valdorms},
                    success:function(data){
                        var dados = JSON.parse(data);
                        var valor = "";
                        if(dados==""){
                             criaResultado(); 
                         }else{var estagio_nome;
                            for(var i=0;i < dados.length;i++){
                                
                              if(dados[i].estagio==1){estagio_nome="Pré-lançamento";}
                                else if(dados[i].estagio==2){estagio_nome="Lançamento";}
                                else if(dados[i].estagio==3){estagio_nome="Em contrução";}
                                else if(dados[i].estagio==4){estagio_nome="Pronto para morar";}
                                
                                var titulo = dados[i].titulo.split("<t>");
                                var titulo_longo;
                                var titulo_curto;
                                if(titulo[1]==undefined){
                                    titulo_curto = titulo[0];
                                    titulo_longo = titulo[0];
                                }else{
                                    titulo_curto = titulo[1];
                                    titulo_longo = titulo[0]+" "+titulo[1];
                                }
                                var url1 = titulo_longo.replace(/[' ']/g,'-');
                                var urlfinal = url1.replace(/[áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ]/g,'aaaaeeiooouuncAAAAEEIOOOUUNC-').toLowerCase();
                                valor += '<div class="destaque_empreendimentos"><img src="<?php echo PATH_ROOT?>web-files/img/imgprojeto/'+dados[i].img_destaque+'"/>';
                                valor += '<div id="legenda_destaque"><p id="estagio_prj">'+estagio_nome+'</p><div class="titulo-box-destaque"><h1>'+titulo_curto+'</h1>';
                                valor += '<p style="color:'+dados[i].paleta_subtitulo+'">'+dados[i].subtitulo+'</p></div>';
                                valor += '<p id="itens_destaque">'+dados[i].tipoprj_nome+'<br>'+dados[i].dormitorio+' Dorms<br>'+dados[i].area+'m²</p>'; 
                                valor += '<a href="<?php echo PATH_ROOT?>empreendimentos/imovel/c/'+dados[i].id+'/n/'+urlfinal+'" class="btn btn-prj">CLIQUE E CONHEÇA</a></div></div>'; 
                            }
                            $("#content_empreendimentos").html(valor);
                             var imovel = "";
                            if(dados.length > 1){imovel=" Imóveis";}else{imovel=" Imóvel";}
                            $("#txt_qtd_imoveis span").html(dados.length+imovel);
                        }
                    }
                });   
           }
       });
       
       $("input[name='r_filtroestagio']").click(function(){
           var valestagio = $(this).val();
           var valdorms = $("input[name='r_filtrodorms']:checked").val();
           var urlDirect = "<?php echo PATH_ROOT?>empreendimentos/filtro/";
           if(valestagio != undefined){               
                $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{valdorms:valdorms,valestagio:valestagio},
                    success:function(data){
                        var dados = JSON.parse(data);
                        var valor = "";
                        if(dados==""){
                             criaResultado();  
                         }else{var estagio_nome="";
                            for(var i=0;i < dados.length;i++){
                                
                                if(dados[i].estagio==1){estagio_nome="Pré-lançamento";}
                                else if(dados[i].estagio==2){estagio_nome="Lançamento";}
                                else if(dados[i].estagio==3){estagio_nome="Em contrução";}
                                else if(dados[i].estagio==4){estagio_nome="Pronto para morar";}
                                var titulo = dados[i].titulo.split("<t>");
                                var titulo_longo;
                                var titulo_curto;
                                if(titulo[1]==undefined){
                                    titulo_curto = titulo[0];
                                    titulo_longo = titulo[0];
                                }else{
                                    titulo_curto = titulo[1];
                                    titulo_longo = titulo[0]+" "+titulo[1];
                                }
                                var url1 = titulo_longo.replace(/[' ']/g,'-');
                                
                                var urlfinal = url1.replace(/[áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ]/g,'aaaaeeiooouuncAAAAEEIOOOUUNC-').toLowerCase();
                                valor += '<div class="destaque_empreendimentos"><img src="<?php echo PATH_ROOT?>web-files/img/imgprojeto/'+dados[i].img_destaque+'"/>';
                                valor += '<div id="legenda_destaque"><p id="estagio_prj">'+estagio_nome+'</p><div class="titulo-box-destaque"><h1>'+titulo_curto+'</h1>';
                                valor += '<p style="color:'+dados[i].paleta_subtitulo+'">'+dados[i].subtitulo+'</p></div>';
                                valor += '<p id="itens_destaque">'+dados[i].tipoprj_nome+'<br>'+dados[i].dormitorio+' Dorms<br>'+dados[i].area+'m²</p>'; 
                                valor += '<a href="<?php echo PATH_ROOT?>empreendimentos/imovel/c/'+dados[i].id+'/n/'+urlfinal+'" class="btn btn-prj">CLIQUE E CONHEÇA</a></div></div>'; 
                            }
                            $("#content_empreendimentos").html(valor);
                             var imovel = "";
                            if(dados.length > 1){imovel=" Imóveis";}else{imovel=" Imóvel";}
                            $("#txt_qtd_imoveis span").html(dados.length+imovel);
                        }
                    }
                });   
                
            }else{
               $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{valestagio:valestagio},
                    success:function(data){
                        var dados = JSON.parse(data);
                        var valor = "";
                        if(dados==""){
                             criaResultado(); 
                         }else{var estagio_nome="";
                            for(var i=0;i < dados.length;i++){
                                
                                if(dados[i].estagio==1){estagio_nome="Pré-lançamento";}
                                else if(dados[i].estagio==2){estagio_nome="Lançamento";}
                                else if(dados[i].estagio==3){estagio_nome="Em contrução";}
                                else if(dados[i].estagio==4){estagio_nome="Pronto para morar";}
                                    
                 
                                var titulo = dados[i].titulo.split("<t>");
                                var titulo_longo;
                                var titulo_curto;
                                if(titulo[1]==undefined){
                                    titulo_curto = titulo[0];
                                    titulo_longo = titulo[0];
                                }else{
                                    titulo_curto = titulo[1];
                                    titulo_longo = titulo[0]+" "+titulo[1];
                                }
                                var url1 = titulo_longo.replace(/[' ']/g,'-');
                                var urlfinal = url1.replace(/[áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ]/g,'aaaaeeiooouuncAAAAEEIOOOUUNC-').toLowerCase();
                                valor += '<div class="destaque_empreendimentos"><img src="<?php echo PATH_ROOT?>web-files/img/imgprojeto/'+dados[i].img_destaque+'"/>';
                                valor += '<div id="legenda_destaque"><p id="estagio_prj">'+estagio_nome+'</p><div class="titulo-box-destaque"><h1>'+titulo_curto+'</h1>';
                                valor += '<p style="color:'+dados[i].paleta_subtitulo+'">'+dados[i].subtitulo+'</p></div>';
                                valor += '<p id="itens_destaque">'+dados[i].tipoprj_nome+'<br>'+dados[i].dormitorio+' Dorms<br>'+dados[i].area+'m²</p>'; 
                                valor += '<a href="<?php echo PATH_ROOT?>empreendimentos/imovel/c/'+dados[i].id+'/n/'+urlfinal+'" class="btn btn-prj">CLIQUE E CONHEÇA</a></div></div>'; 
                            }
                            $("#content_empreendimentos").html(valor);
                            var imovel = "";
                            if(dados.length > 1){imovel=" Imóveis";}else{imovel=" Imóvel";}
                            $("#txt_qtd_imoveis span").html(dados.length+imovel);
                        }
                    }
                });   
           }
       });
       function criaResultado(){
     $("#content_empreendimentos").html("<p class='alert alert-info span4'>Nenhum resultado encontrado</p>");
     $("#txt_qtd_imoveis span").html("0 Imóveis");
}
 /*======== FUNÇÕES SLIDER =========*/ 
       $(".slides").eq(0).clone().appendTo("#banner_slider_b");
       var qtd = $(".slides").length;
       var contador=1;      
       $(window).resize(tamanho);
       tamanho();
       
       var pare = false;
       $(".slides").removeClass("next");
       $(".slides").removeClass("prev");
       $(".slides").eq(contador).addClass("next");
       $(".slides").eq(qtd-2).addClass("prev");
     
       $("#prev_slide").click(function(e){
          if(pare){
            e.preventDefault();
          } else{
            pare=true;
            var posicao = $("#banner_slider_b").css('left').replace("px", "");
            var tamanho_atual = $("#banner_slider").width();
            if(posicao==0 && contador==1){
                contador=qtd-1;
                var calculo = (qtd-1)*tamanho_atual;
                $("#banner_slider_b").css('left',-calculo);
                var posicao_atual=$("#banner_slider_b").css('left').replace("px", "");
                var calc_final = parseInt(posicao_atual)+parseInt(tamanho_atual);
                TweenLite.to("#banner_slider_b", 1, {'left': calc_final, ease: Expo.easeInOut,onComplete:desab});
                $(".slides").removeClass("next");
                $(".slides").removeClass("prev");
                $(".slides").eq(contador).addClass("next");
                $(".slides").eq(contador-2).addClass("prev");
            }else{
                contador--;
                var posicao_atual=$("#banner_slider_b").css('left').replace("px", "");
                var calc_final = parseInt(posicao_atual)+parseInt(tamanho_atual);
                TweenLite.to("#banner_slider_b", 1, {'left': calc_final, ease: Expo.easeInOut,onComplete:desab});
                $(".slides").removeClass("next");
                $(".slides").removeClass("prev");
                $(".slides").eq(contador).addClass("next");
                if(contador!=1){
                    $(".slides").eq(contador-2).addClass("prev");
                }else{
                    $(".slides").eq(qtd-2).addClass("prev");
                }                
              }
            }
        });
      
       $("#next_slide").click(function(e){
            if (pare) {
               e.preventDefault();
            } else {
               pare=true;
               contador++;         
               var posicao = $("#banner_slider_b").css('left').replace("px", "");        
               var tamanho_atual = $("#banner_slider").width();        
               TweenLite.to("#banner_slider_b", 1, {'left': (posicao-tamanho_atual), ease: Expo.easeInOut,onComplete:zera});
               $(".slides").removeClass("next");
               $(".slides").removeClass("prev");
               $(".slides").eq(contador).addClass("next");
               $(".slides").eq(contador-2).addClass("prev");
            }
        });
      
       $("#prev_slide").mouseover(function(){
            var img = $("#banner_slider_b").find(".prev").css("background-image");
            $("#bt_prev").css("display","none");
            $(".nav_prev_h").css({display:"block",backgroundImage:img});
        }); 
       $("#prev_slide").mouseout(function(){ 
            $(".nav_prev_h").css({display:"none"});       
            $("#bt_prev").css("display","block");           
        });
    
       $("#next_slide").hover(
            function(){
               $(this).children("#bt_next").css("display","none");
               var img = $("#banner_slider_b").find(".next").css("background-image");           
               $(".nav_next_h").css({display:"block",backgroundImage:img});
        }, 
            function(){ 
              $(this).children(".nav_next_h").css("display","none");
              $(this).children("#bt_next").css("display","block");
        });
      var tween = TweenLite.to("#barra_progresso", 15, {'width':"100%",ease:Linear.easeNone,onComplete:disparaProgress});
       function disparaProgress(){
        $("#barra_progresso").css("width","0%");
        $("#next_slide").trigger("click");
        tween.restart();
        }
       function desab(){
          pare=false; 
        }
       function zera(){
          pare=false;
          if(contador==qtd){
            contador=1;
            $("#banner_slider_b").css('left',"0");
            $(".slides").removeClass("next");
            $(".slides").removeClass("prev");
            $(".slides").eq(contador).addClass("next");
            $(".slides").eq(qtd-2).addClass("prev");
          } 
        }
       function tamanho(){
           var larguramedia =$("#content_header").css("left");
           $(".box_legenda_destaque").css("left",larguramedia);
           var tamanho = $("#banner_slider").width();
           for(var i=0; i<qtd; i++){
              $(".slides").eq(i).css("width",tamanho);
           }
           $("#banner_slider_b").css("width",tamanho* qtd);
           if(contador!=1){
               var calc_resize = (contador-1)*tamanho;
               $("#banner_slider_b").css("left",-calc_resize);
           }
        }   
       
       /*======== FIM FUNÇÕES SLIDER =========*/ 
   });
</script>


