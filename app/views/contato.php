<?php
$title = " | Contato";
$body = "contato";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                Contato
            </p>
            <h1 id="titulo_interna_prj">Contato</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/contato/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/contato/" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div> 
        <div id="bloco_chamada_destaque">
            <h1 id="txt_chamada_destaque2">Atendimento por e-mail</h1>
        </div>
        <div id="container_sobre">
        <div class="sidebar_esq">
            <h3 class="header_sidebar_esq">Área do Cliente</h3>
            <div id="area_cliente_contato">
                <a href="<?php echo AREA_CLIENTE;?>" target="_blank">
                    <img alt="Área do Cliente" title="Área do Cliente" src="<?php echo PATH_ROOT?>web-files/img/bt_area_cliente.gif" width="310"/>
                </a>
                <p>Faça seu login e tenha acesso<br>a serviços exclusivos.</p>
                <h3>Horário de atendimento:</h3>
                <p>Segunda a Sexta, das 8 às 17h30</p>
            </div>
           
        </div>
        <div id="content_sobre">
            <form action="" method="post" name="form_contato">
                <select id="contato_sel">
                    <option selected="selected" disabled="disabled" value="0">Selecione o Assunto:</option>
                    <option value="Relacionamento Com o Cliente">Relacionamento Com o Cliente</option>
                    <option value="Informações Sobre Empreendimento">Informações Sobre Empreendimento</option>
                    <option value="Fornecedor">Fornecedor</option>
                    <option value="Outros">Outros</option>
                </select>
                <div id="bloco1_contato">
                    <label for="nome_contato">Nome:</label>
                    <input type="text" name="nome_contato" id="nome_contato" class="input_full_contato" required="required"/>
                    <label for="email_contato">E-mail:</label>
                    <input type="text" name="email_contato" id="email_contato" class="input_full_contato" required="required"/>               
                </div>
                <div id="bloco2_contato">
                    <div id="bloco2a">
                        <label>Telefone Móvel:</label>
                        <input type="text" class="ddd" maxlength="2" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode)))
                                                    return true;
                                                else
                                                    return false;" id="ddd_cel"/>
                                            <input type="text" class="fone" id="cel"/>
                    </div>
                        <div id="bloco2b">
                    <label>Telefone fixo:</label>
                    <input type="text" class="ddd" maxlength="2" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode)))
                                                    return true;
                                                else
                                                    return false;" id="ddd_tel"/>
                                            <input type="text" class="fone" id="tel"/>
                    </div>
                </div>
                <div id="bloco3_contato">
                    <select id="empreendimento_sel" required="required">
                        <option disabled="disabled" selected="selected" value="0">Empreendimento</option>
                        <?php foreach($view_contato as $projeto):
                            $titulo =  explode("<t>", $projeto["titulo"]);
                            $titulo_longo = $titulo[0]." ".$titulo[1];
                            if($titulo[1]!=""){
                            $titulo_curto = $titulo[1];
                            }else{
                                $titulo_curto = $titulo[0];
            }?>
                        <option value="<?php echo $titulo_longo;?>"><?php echo $titulo_longo;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div id="bloco4_contato">
                    <div id="bloco4a">
                        <label>Torre:</label>
                        <input type="text" class="dados_prj" id="torre" required="required"/>
                    </div>
                    <div id="bloco4b">
                        <label>Unidade:</label>
                        <input type="text" class="dados_prj" id="unidade" required="required"/>
                    </div>
                </div>
                <div id="bloco5_contato">
                    <label>Mensagem:</label>
                    <textarea id="mensagem_contato" required="required"></textarea>
                </div>
                <input type="submit" id="bt_enviar_contato" value="Enviar"/>
            </form>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
<script>         
   $(document).ready(function(){               
        $("#contato_sel,#empreendimento_sel").selectbox();
        var assunto = $(".sbOptions").eq(0);
        var empreendimento = $(".sbOptions").eq(1);
        
        var cliente = assunto.find("a").eq(0);
        var info = assunto.find("a").eq(1);
        var fornecedor = assunto.find("a").eq(2);
        var outros = assunto.find("a").eq(3);
        
        cliente.click(function(){
            $("#bloco3_contato,#bloco4_contato").show();
        });
        
        info.click(function(){
            $("#bloco4_contato").hide();
            $("#bloco3_contato").show();
        });
        
        fornecedor.click(function(){
            $("#bloco3_contato,#bloco4_contato").hide();
        });
        
        outros.click(function(){
            $("#bloco3_contato,#bloco4_contato").hide();
        });
        
        $("#bt_enviar_contato").click(function(e){
            e.preventDefault();
            var assunto_contato = $("#contato_sel option:selected").val();
            var nome = $("#nome_contato").val().trim();
            var email = $("#email_contato").val().trim();
            var cel = $("#cel").val().trim();
            var ddd_cel = $("#ddd_cel").val().trim();
            var celular = "("+ddd_cel+")"+" "+cel;
            var tel = $("#tel").val().trim();
            var ddd_tel = $("#ddd_tel").val().trim();
            var telefone = "("+ddd_tel+")"+" "+tel;
            var empreendimento = $("#empreendimento_sel option:selected").val();
            var torre = $("#torre").val().trim();
            var unidade = $("#unidade").val().trim();
            var mensagem = $("#mensagem_contato").val().trim();
            var urlDirect = "<?php echo PATH_ROOT."contato/envio/"; ?>";
            if(assunto_contato==0){
                alert("Escolha um assunto");
            }
            else if(nome==""){
                alert("Digite seu nome");
            }
            else if(email=="" || email.indexOf('@')== -1 || email.indexOf('.')== -1){
                alert("Digite um email válido");
            }
            else if(assunto_contato=="Relacionamento Com o Cliente"){                
                if(empreendimento==0){
                        alert("Escolha um empreendimento");
                    }
                else if(torre==""){
                    alert("Digite a torre");
                }
                else if(unidade==""){
                    alert("Digite a unidade");
                }
                else if(mensagem==""){
                    alert("Digite a mensagem");
                }
                else{
                    $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{assunto_contato:assunto_contato,celular:celular,telefone:telefone,nome:nome,email:email,empreendimento:empreendimento,torre:torre,unidade:unidade,mensagem:mensagem},
                    success:function(data){
                        if(data == "ok"){
                            ga('send', 'event', 'formulario', 'envio', 'ok_rel_cliente');
                            alert("Enviado com sucesso");
                            document.form_contato.reset();
                        }
                        else{
                            alert("Erro ao enviar");
                        }
                    }
                    });
                }
                }
            else if(assunto_contato=="Informações Sobre Empreendimento"){
                if(empreendimento==0){
                    alert("Escolha um empreendimento");
                }
                else if(mensagem==""){
                    alert("Digite a mensagem");
                }
                else{
                    $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{assunto_contato:assunto_contato,celular:celular,telefone:telefone,nome:nome,email:email,empreendimento:empreendimento,mensagem:mensagem},
                    success:function(data){
                        if(data=="ok"){
                            ga('send', 'event', 'formulario', 'envio', 'ok_info_empreendimento');
                            alert("Enviado com sucesso");
                            document.form_contato.reset();
                        }
                        else{
                            alert("Erro ao enviar");
                        }
                    }
                    });
                }
            }
            else if(mensagem==""){
                alert("Digite a mensagem");
            }
            else{
                $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{assunto_contato:assunto_contato,celular:celular,telefone:telefone,nome:nome,email:email,mensagem:mensagem},
                    success:function(data){
                      
                       if(data== "ok"){
                           ga('send', 'event', 'formulario', 'envio', 'ok_contato_outros');
                            alert("Enviado com sucesso");
                            document.form_contato.reset();
                        }
                        else{
                            alert("Erro ao enviar");
                        }
                    }
                    });
            }
            
        });
   });
</script>