<?php
if($body=="home"){
    $url_og= "http://www.bigconstrutora.com.br".PATH_ROOT;
    $description_og = "A BIG Construtora e Incorporadora S/A, surgiu do sonho de oferecer ao público 
                                empreendimentos diferenciados com qualidade e preço justo. Foi com o 
                                ingresso no Programa do Governo Federal MCMV (Minha Casa, Minha Vida), 
                                parceria com investidores, bancos e agentes de financiamento que esse sonho 
                                tornou-se realidade.Desde sua fundação em 2008 a Big Construtora e Incorporadora desenvolveu,
                                entre unidades concluídas, em construção e em lançamento, 962 unidades 
                                habitacionais na cidade de São Paulo, nos bairros do Tremembé (Zona Norte) e 
                                Tatuapé (Zona Leste) e na cidade vizinha de Ferraz de Vasconcelos, nos bairros 
                                do Cambiri e Vila São Paulo.Contamos com uma equipe de profissionais comprometidos e altamente 
                                capacitados tecnicamente nas áreas: administrativa, incorporação, vendas, 
                                jurídica, correspondente bancário, engenharia e construção, que acompanham e dão 
                                suporte desde a aquisição do terreno, financiamento, até o período pós-entrega final do imóvel ao cliente.
                                A BIG CONSTRUTORA faz uso de um sitema integrado de gestão que visa a interligação 
                                de todos os processos da empresa, com o objetivo de gerar agilidade, confiabilidade e transparência nas informações, sistema esse 
                                reconhecido com a obtenção das certificações: Programa de Qualidade e 
                                Produtividade no Habitat, PBQP-H nível A e ISO 9001:2008";
    $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
}
else if($body=="sobre"){
    $url_og= "http://www.bigconstrutora.com.br".PATH_ROOT."sobre/";
    $description_og = "A BIG Construtora e Incorporadora S/A, surgiu do sonho de oferecer ao público 
                                empreendimentos diferenciados com qualidade e preço justo. Foi com o 
                                ingresso no Programa do Governo Federal MCMV (Minha Casa, Minha Vida), 
                                parceria com investidores, bancos e agentes de financiamento que esse sonho 
                                tornou-se realidade.Desde sua fundação em 2008 a Big Construtora e Incorporadora desenvolveu,
                                entre unidades concluídas, em construção e em lançamento, 962 unidades 
                                habitacionais na cidade de São Paulo, nos bairros do Tremembé (Zona Norte) e 
                                Tatuapé (Zona Leste) e na cidade vizinha de Ferraz de Vasconcelos, nos bairros 
                                do Cambiri e Vila São Paulo.Contamos com uma equipe de profissionais comprometidos e altamente 
                                capacitados tecnicamente nas áreas: administrativa, incorporação, vendas, 
                                jurídica, correspondente bancário, engenharia e construção, que acompanham e dão 
                                suporte desde a aquisição do terreno, financiamento, até o período pós-entrega final do imóvel ao cliente.
                                A BIG CONSTRUTORA faz uso de um sitema integrado de gestão que visa a interligação 
                                de todos os processos da empresa, com o objetivo de gerar agilidade, confiabilidade e transparência nas informações, sistema esse 
                                reconhecido com a obtenção das certificações: Programa de Qualidade e 
                                Produtividade no Habitat, PBQP-H nível A e ISO 9001:2008";
    $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
}
else if($body=="certificacoes"){
    $url_og= "http://www.bigconstrutora.com.br".PATH_ROOT."sobre/certificacoes/";
    $description_og = "PBQP do Habitat – Nível A
                    O PBQP-H, Programa Brasileiro da Qualidade e Produtividade do Habitat, é um instrumento do Governo Federal
                    criado para organizar o setor da construção civil em torno de duas questões principais: a melhoria da qualidade
                        do habitat e a modernização produtiva.A busca por esses objetivos envolve um conjunto de ações, entre as quais se destacam: avaliação da conformidade
                    de empresas de serviços e obras, melhoria da qualidade de materiais, formação e requalificação de mão-de-obra,
                    normalização técnica, capacitação de laboratórios, avaliação de tecnologias inovadoras, informação ao consumidor
                    e promoção da comunicação entre os setores envolvidos.
                NBR ISO 9001:2008. A Big Construtora e Incorporadora possui Sistema de Gestão da Qualidade em conformidade
                com a norma NBR ISO 9001, certificada pelo ITAC – Instituto Tecnológico de Avaliação e Certificação da Conformidade Ltda.";
    $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
}
else if($body=="contato"){
     $url_og= "http://www.bigconstrutora.com.br".PATH_ROOT."contato/";
    $description_og = "Horário de atendimento:Segunda a Sexta, das 9 às 21hs,
                    Sábados, das 10 às 21hs,
                    Domingos e feriados, das 11 às 20hs. Rua Antonio Camardo, 577
Tatuapé - São Paulo - SP
CEP 03309-060
Fone / Fax (11) 2090-0301";
    $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
}
else if($body=="visite"){
    $url_og= "http://www.bigconstrutora.com.br".PATH_ROOT."localizacao/";
    $description_og = "Horário de atendimento:Segunda a Sexta, das 9 às 21hs,
                    Sábados, das 10 às 21hs,
                    Sede: Rua Antônio camardo, 577, Tatuapé, SP,
            Loja: Avenida Brasil, 767, Centro, Ferraz de Vasconcelos, SP,
Fone / Fax (11) 2090-0301";
    $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
}
else if($body=="noticias"){
	$server = $_SERVER['SERVER_NAME'];

	$endereco = $_SERVER ['REQUEST_URI'];
    
	$url_og = "http://".$server.$endereco;

    $pos = strpos($endereco, "artigo");
    if ($pos === false) {
        $description_og ="A BIG Construtora e Incorporadora S/A, surgiu do sonho de oferecer ao público 
                                empreendimentos diferenciados com qualidade e preço justo. Foi com o 
                                ingresso no Programa do Governo Federal MCMV (Minha Casa, Minha Vida), 
                                parceria com investidores, bancos e agentes de financiamento que esse sonho 
                                tornou-se realidade.Desde sua fundação em 2008 a Big Construtora e Incorporadora desenvolveu,
                                entre unidades concluídas, em construção e em lançamento, 962 unidades 
                                habitacionais na cidade de São Paulo, nos bairros do Tremembé (Zona Norte) e 
                                Tatuapé (Zona Leste) e na cidade vizinha de Ferraz de Vasconcelos, nos bairros 
                                do Cambiri e Vila São Paulo.Contamos com uma equipe de profissionais comprometidos e altamente 
                                capacitados tecnicamente nas áreas: administrativa, incorporação, vendas, 
                                jurídica, correspondente bancário, engenharia e construção, que acompanham e dão 
                                suporte desde a aquisição do terreno, financiamento, até o período pós-entrega final do imóvel ao cliente.
                                A BIG CONSTRUTORA faz uso de um sitema integrado de gestão que visa a interligação 
                                de todos os processos da empresa, com o objetivo de gerar agilidade, confiabilidade e transparência nas informações, sistema esse 
                                reconhecido com a obtenção das certificações: Programa de Qualidade e 
                                Produtividade no Habitat, PBQP-H nível A e ISO 9001:2008";
        $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
    }
    else{
        $part_endereco = explode("/",$endereco);
        $url_frame = "http://".$server."/blog/".$part_endereco[4]."/?f=1";
           $DOMDocument = new DOMDocument( '1.0', 'utf-8' );
           $DOMDocument->preserveWhiteSpace = false;
           @$DOMDocument->loadHTML( file_get_contents( $url_frame ) );
           $DOMXPath = new DOMXPath( $DOMDocument );
           $description_og = $DOMXPath->query( './/meta[@property="og:description"]' )->item( 0 )->getAttribute( 'content' );
           $img_og = $DOMXPath->query( './/meta[@property="og:image"]' )->item( 0 )->getAttribute( 'content' );
    } 
}
else if($body=="politica"){
    $url_og= "http://www.bigconstrutora.com.br".PATH_ROOT."sobre/politica/";
    $description_og = "A BIG Construtora e Incorporadora S/A, surgiu do sonho de oferecer ao público 
                                empreendimentos diferenciados com qualidade e preço justo. Foi com o 
                                ingresso no Programa do Governo Federal MCMV (Minha Casa, Minha Vida), 
                                parceria com investidores, bancos e agentes de financiamento que esse sonho 
                                tornou-se realidade.Desde sua fundação em 2008 a Big Construtora e Incorporadora desenvolveu,
                                entre unidades concluídas, em construção e em lançamento, 962 unidades 
                                habitacionais na cidade de São Paulo, nos bairros do Tremembé (Zona Norte) e 
                                Tatuapé (Zona Leste) e na cidade vizinha de Ferraz de Vasconcelos, nos bairros 
                                do Cambiri e Vila São Paulo.Contamos com uma equipe de profissionais comprometidos e altamente 
                                capacitados tecnicamente nas áreas: administrativa, incorporação, vendas, 
                                jurídica, correspondente bancário, engenharia e construção, que acompanham e dão 
                                suporte desde a aquisição do terreno, financiamento, até o período pós-entrega final do imóvel ao cliente.
                                A BIG CONSTRUTORA faz uso de um sitema integrado de gestão que visa a interligação 
                                de todos os processos da empresa, com o objetivo de gerar agilidade, confiabilidade e transparência nas informações, sistema esse 
                                reconhecido com a obtenção das certificações: Programa de Qualidade e 
                                Produtividade no Habitat, PBQP-H nível A e ISO 9001:2008";
    $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
}
else if($body=="empreendimentos"){
    $url_og="http://www.bigconstrutora.com.br".PATH_ROOT."empreendimentos/";
    $description_og = "A BIG Construtora e Incorporadora S/A, surgiu do sonho de oferecer ao público 
                                empreendimentos diferenciados com qualidade e preço justo. Foi com o 
                                ingresso no Programa do Governo Federal MCMV (Minha Casa, Minha Vida), 
                                parceria com investidores, bancos e agentes de financiamento que esse sonho 
                                tornou-se realidade.Desde sua fundação em 2008 a Big Construtora e Incorporadora desenvolveu,
                                entre unidades concluídas, em construção e em lançamento, 962 unidades 
                                habitacionais na cidade de São Paulo, nos bairros do Tremembé (Zona Norte) e 
                                Tatuapé (Zona Leste) e na cidade vizinha de Ferraz de Vasconcelos, nos bairros 
                                do Cambiri e Vila São Paulo.Contamos com uma equipe de profissionais comprometidos e altamente 
                                capacitados tecnicamente nas áreas: administrativa, incorporação, vendas, 
                                jurídica, correspondente bancário, engenharia e construção, que acompanham e dão 
                                suporte desde a aquisição do terreno, financiamento, até o período pós-entrega final do imóvel ao cliente.
                                A BIG CONSTRUTORA faz uso de um sitema integrado de gestão que visa a interligação 
                                de todos os processos da empresa, com o objetivo de gerar agilidade, confiabilidade e transparência nas informações, sistema esse 
                                reconhecido com a obtenção das certificações: Programa de Qualidade e 
                                Produtividade no Habitat, PBQP-H nível A e ISO 9001:2008";
    $img_og = "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/logo_big_face.jpg";
}
else if($body=="projetos"){
    $url_og= "http://www.bigconstrutora.com.br".PATH_ROOT."empreendimentos/imovel/c/".$id."/n/".$titleUrl;
    
    $description_og = preg_replace("<br>",chr(13),$titulo_present.strip_tags($txt_present));
    $img_og =  "http://www.bigconstrutora.com.br".PATH_ROOT."web-files/img/imgprojeto/".$destaque;
}
$title_og = "Big".$title;
?>
<meta name="description" content="<?php echo $description_og;?>"/>
<meta property="og:locale" content="pt_BR">
<meta property="og:url" content="<?php echo $url_og;?>">
<meta property="og:title" content="<?php echo $title_og;?>">
<meta property="og:site_name" content="Big Construtora">
<meta property="og:description" content="<?php echo $description_og;?>">
<meta property="og:image" content="<?php echo $img_og;?>">
<meta property="og:image:type" content="image/jpeg">
<meta property="og:type" content="website">
<link rel="shortcut icon" href="<?php echo PATH_ROOT?>web-files/img/icon/favicon.ico" type="image/x-icon" />
  
<link rel="apple-touch-icon" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo PATH_ROOT?>web-files/img/icon/apple-touch-icon-152x152.png" />