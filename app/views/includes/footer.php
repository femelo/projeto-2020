        <div id="footer">
            <div id="content_footer">
                <div id="bloco_contato_ft">
                    <img src="<?php echo PATH_ROOT?>web-files/img/logo_footer.png" id="logo_footer" alt=""/>
                    <div id="container_contato_ft">
                        <p id="legenda_contato_ft">ENTRE EM CONTATO AGORA</p>
                        <div id="redes_sociais_ft">
                            <ul id="lista_redes">
                                <li>
                                    <a href="<?php echo URL_FACE;?>" target="_blank">
                                        <img src="<?php echo PATH_ROOT?>web-files/img/bt_facebook.png" with="40" height="40" alt=""/>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL_TWITTER;?>" target="_blank">
                                        <img src="<?php echo PATH_ROOT?>web-files/img/bt_twitter.jpg" with="40" height="40" alt=""/>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL_YOUTUBE;?>" target="_blank">
                                        <img src="<?php echo PATH_ROOT?>web-files/img/bt_youtube.jpg" with="40" height="40" alt=""/>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL_INSTA;?>" target="_blank">
                                        <img src="<?php echo PATH_ROOT?>web-files/img/bt_instagram.jpg" with="40" height="40" alt=""/>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL_PINTEREST;?>" target="_blank">
                                        <img src="<?php echo PATH_ROOT?>web-files/img/pinterest-logo.png" with="40" height="40" alt=""/>
                                    </a>
                                </li>
                            </ul>
                            <p id="legenda_redes_ft">CANAIS SOCIAIS</p>   
                        </div>
                        <div id="menu_header" class="border_footer">
                            <div class="item_menu_header" id="item_chat_footer"><div><span>CHAT</span></div></div>
                            <div class="item_menu_header" id="item_mail_footer"><div><span>E-MAIL</span></div></div>
                            <div class="item_menu_header" id="item_telefone_footer"><div><span>TELEFONE</span></div></div>
                            <div class="item_menu_header" id="item_ligamos_footer"><div><span>NÓS LIGAMOS</span></div></div>
                        </div>
                    </div>
                </div>
                <div id="divisor"></div>
                <div id="bloco_menu_ft">
                    <div class="info_footer"><p>Copyright © 2014<br>Big Construtora e Incorporadora<br>os os direitos reservados.<p>
                        <p>Rua Antonio Camardo, 577<br/>Tatuapé - São Paulo - SP<br/>CEP 03309-060<br/>Fone: (11) 2090-0301</p>
                        <a href="<?php echo PATH_ROOT;?>sobre/politica/">Política de Privacidade</a>
                    </div>
                    <img src="<?php echo PATH_ROOT?>web-files/img/vertical_pontilhado.jpg" id="vert_divisor"/>
                    <div id="menu_footer">
                        <p>
                            <a href="<?php echo PATH_ROOT;?>sobre/">Sobre a Big</a> | 
                            <a href="<?php echo PATH_ROOT;?>empreendimentos/">Empreendimentos</a> | 
                            <a href="<?php echo PATH_ROOT;?>financiamentos/">Financiamentos</a> | 
                            <a href="<?php echo PATH_ROOT;?>noticias/">Notícias</a> | 
                            <a href="<?php echo PATH_ROOT;?>localizacao/">Visite-nos</a> | 
                            <a href="<?php echo AREA_CLIENTE;?>" target="_blank">Área do Cliente</a> | 
                            <a href="<?php echo PATH_ROOT;?>contato/">Contato</a>
                        </p>
                        <ul class="lista_submenu_footer">
                            <li><a href="<?php echo PATH_ROOT;?>sobre/certificacoes/">Certificações</a></li>
                            <li><a href="<?php echo PATH_ROOT;?>localizacao/">Localização</a></li>
                            <li><a href="<?php echo PATH_ROOT;?>contato">Fale Conosco</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>       
    </body>
   
    <script src="<?php echo PATH_ROOT ?>web-files/js/jquery-1.10.2.min.js"></script>   
    <script src="<?php echo PATH_ROOT ?>web-files/js/bootstrap.min.js"></script>         
    <script src="<?php echo PATH_ROOT ?>web-files/js/CSSPlugin.min.js"></script>
    <script src="<?php echo PATH_ROOT ?>web-files/js/EasePack.min.js"></script>
    <script src="<?php echo PATH_ROOT ?>web-files/js/TweenLite.min.js"></script>
    <script src="<?php echo PATH_ROOT ?>web-files/js/jquery.selectbox-0.2.min.js"></script>
    <script src="<?php echo PATH_ROOT ?>web-files/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo PATH_ROOT ?>web-files/libmox/mbox.min.js"></script>
    <script src="https://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&v=3.exp&libraries=places"></script>

<script>
    function titlePopup(texto){
        $("#header_popup > p").html(texto);
    }
    $(document).ready(function(){
        //$("#bt_area_cliente").click(function(e){
          //  e.preventDefault();
        //    var posicao = $(window).scrollTop();
        //    $("#bg-alpha").show();
        //    $("#box_popup").css("top",(posicao+120)+"px").show();
        //    $("#popup_nosligamos,#popup_telefone,#popup_financiamento").hide();
        //    $("#popup_areacliente").show();
        //    titlePopup("Área do Cliente");
       // });
       // alert($("#footer").offset().top);
           $("#item_chat_footer,#item_chat").click(function(){
               ga('send', 'event', 'button', 'click', 'bts_chat');
            $zopim(function(){
                    $zopim.livechat.button.hide();
            });   
           });
       
       var tela = $(window).height()+ $("#footer").height();
       
        $(window).scroll(function(){
            if($(window).scrollTop()> ($(document).height()-tela)){
                TweenLite.to("#header", 0.5, {'top': -118});
                
            }
            if($(window).scrollTop()< ($(document).height()-tela)){
                TweenLite.to("#header", 0.5, {'top': 0});
            }
        });
        $("#bg-alpha").css("height",$(document).height());
        $("#item_mail,#item_mail_footer").click(function(){
            ga('send', 'event', 'button', 'click', 'bts_email');
            document.location.href = "<?php echo PATH_ROOT?>contato";
        });
        $("#bt_search").click(function(){
            var valor_busca = $("#search_proj").val();
            document.location.href = "<?php echo PATH_ROOT;?>empreendimentos/search/val/"+valor_busca;
      
        });
        $("#item_telefone").click(function(){
            var posicao = $(window).scrollTop();
            $("#bg-alpha").show();
            $("#box_popup").css("top",(posicao+120)+"px").show();
            $("#popup_nosligamos,#popup_financiamento,#popup_areacliente,#popup_showroom").hide();
            $("#popup_telefone").show();
            titlePopup("Entrar em contato com a Big");
        });
        $("#item_ligamos").click(function(){
             var posicao = $(window).scrollTop();
            $("#bg-alpha").show();
            $("#box_popup").css("top",(posicao+120)+"px").show();
            $("#popup_telefone,#popup_financiamento,#popup_areacliente,#popup_showroom").hide();
            $("#popup_nosligamos").show();
            titlePopup("Entrar em contato com a Big");
        });
        $("#item_telefone_footer").click(function(){
            $("#bg-alpha").show();          
            var altura = $("#bg-alpha").height();
            $("#box_popup").css("top",altura-520+"px").show();
            $("#popup_nosligamos,#,#popup_financiamento,#popup_areacliente,#popup_showroom").hide();
            $("#popup_telefone").show();
            titlePopup("Entrar em contato com a Big");
        });
        $("#item_ligamos_footer").click(function(){
            ga('send', 'event', 'button', 'click', 'bt_nos_ligamos');
            $("#bg-alpha").show();
             var altura = $("#bg-alpha").height();
            $("#box_popup").css("top",altura-520+"px").show();
            $("#popup_telefone,#popup_financiamento,#popup_areacliente,#popup_showroom").hide();
            $("#popup_nosligamos").show();
            titlePopup("Entrar em contato com a Big");
        });
       
        $("#bg-alpha,#close_popup").click(function(){
            $("#bg-alpha").hide();
            $("#box_popup").hide();
        });
        $("#bt_nosligamos").click(function(){
            var campotel = $("#campo_tel_nosligamos").val().trim();
            var camponome = $("#campo_nome_nosligamos").val().trim();           
            var urlDirect = "<?php echo PATH_ROOT?>contato/nosligamos/";
            if(camponome==""){
                alert("digite seu nome");
            }
            else if(campotel==""){
                alert("digite seu telefone");
            }
            else{
                $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{camponome:camponome,campotel:campotel},
                    success:function(data){
                        if(data == "ok"){
                            ga('send', 'event', 'formulario', 'envio', 'ok_nos_ligamos');
                            alert("Enviado com sucesso");
                            $("#campo_nome_nosligamos").val("");
                            $("#campo_tel_nosligamos").val("");
                        }
                        else{
                            alert("Erron ao enviar");
                        }
                    }
                    });
            }
        });
    });
</script>
    
</html>
