<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    	<!-- acesso DATATAG -->
    	<script src="https://www.dataunion.com.br/80dc0f41-cea0-49f2-ba51-92a2da2504fe" type="text/javascript" async></script>
    	
    	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NLBTPPV');</script>
<!-- End Google Tag Manager -->

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?2IvVOZAhgSPgMOhS625vKJINs6gCoKUP";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
        <?php if($view_conversao =="2"):?>
        
        <?php endif;?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Big Construtora <?php echo $title;?></title> 
        <link rel="canonical" href="www.bigconstrutora.com.br"/>
        <script type="text/javascript">
//<![CDATA[
<!-- you already know what this does // and this next code uses javascript to automatically go to a www domain
var hostname = location.hostname // grab browser's current domain hostname
var tempstring = new String(hostname) // make sure this is a string so it is easier to parse 
var checkhostname = tempstring.toLowerCase() // and its also easier if we lower the case too please 
if (checkhostname.indexOf("www") !=0 ) // now check that www is the very first part of our browser location string 
{location.hostname = "www.bigconstrutora.com.br"} // else give the browser this location instead and we are done and out -->
//]]>
</script>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="p:domain_verify" content="1c31e9b15134fd21979639c416b50ec7"/>
<?php
@include 'meta.php';
?>

        <link href="<?php echo PATH_ROOT ?>web-files/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
<?php if($body=="contato"):?>
        <link href="<?php echo PATH_ROOT ?>web-files/css/geral.css" type="text/css" rel="stylesheet"/> 
        <link href="<?php echo PATH_ROOT ?>web-files/css/select2.css" type="text/css" rel="stylesheet"/>       
        <link href="<?php echo PATH_ROOT ?>web-files/css/jquery.mCustomScrollbar.css" type="text/css" rel="stylesheet"/> 
        
        <style>
            #header_titulo_prj{border-bottom: 13px solid #902122;}
        </style>
        <!-- Código do Google para tag de remarketing -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 958495232;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>

		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/958495232/?guid=ON&amp;script=0"/>
			</div>
		</noscript>
		<script>
		  	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  	ga('create', 'UA-53750666-1', 'bigconstrutora.com.br');
		  	ga('require', 'displayfeatures');
		  	ga('send', 'pageview');

		</script>
		<!-- Google Code for Bot&atilde;o E-mail Conversion Page -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 958495232;
			var google_conversion_language = "en";
			var google_conversion_format = "3";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "9z1dCMbqvVkQgPSFyQM";
			var google_remarketing_only = false;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/958495232/?label=9z1dCMbqvVkQgPSFyQM&amp;guid=ON&amp;script=0"/>
			</div>
		</noscript>         
    </head>
	<body class="<?php echo $body;?>">
	    <div id="assinatura-push">
	        <a href="http://push.ag" title="Desenvolvido por PUSH Publicidade" target="_blank" rel="index">
		    <img src="<?php echo PATH_ROOT?>web-files/img/push-assinatura.png" alt="PUSH Publicidade" title="PUSH Publicidade">
		</a>
	    </div>
<?php else:?>
        <link href="<?php echo PATH_ROOT ?>web-files/css/geral.css" type="text/css" rel="stylesheet"/> 
        <link href="<?php echo PATH_ROOT ?>web-files/css/select.css" type="text/css" rel="stylesheet"/>       
        <link href="<?php echo PATH_ROOT ?>web-files/css/jquery.mCustomScrollbar.css" type="text/css" rel="stylesheet"/> 
        <link href="<?php echo PATH_ROOT ?>web-files/libmox/style.css" type="text/css" rel="stylesheet"/> 
 		<?php @include CSS_PALETA;?>

 		<!-- Código do Google para tag de remarketing -->

		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 958495232;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/958495232/?guid=ON&amp;script=0"/>
			</div>
		</noscript>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-53750666-1', 'bigconstrutora.com.br');
		  ga('require', 'displayfeatures');
		  ga('send', 'pageview');

		</script>
		<!-- Google Code for Bot&atilde;o E-mail Conversion Page -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 958495232;
			var google_conversion_language = "en";
			var google_conversion_format = "3";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "9z1dCMbqvVkQgPSFyQM";
			var google_remarketing_only = false;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/958495232/?label=9z1dCMbqvVkQgPSFyQM&amp;guid=ON&amp;script=0"/>
			</div>
		</noscript>
		<script>
	        var userAgent = navigator.userAgent.toLowerCase();
	        var devices = new Array('nokia','iphone','blackberry','sony','lg',
	        	'htc_tattoo','samsung','symbian','SymbianOS','elaine','palm',
	       		'series60','windows ce','android','obigo','netfront',
	       		'openwave','mobilexplorer','operamini');
				var url_redirect = '<?php echo PATH_ROOT ?>mobile';
	            var url_atual = '<?php echo $body?>';
	            var url_geral = '<?php echo $view_proj[0]['id'];?>';
	            var url_mobile = '<?php echo PATH_ROOT ?>mobile/imovel/c/1/n/park-dos-pinheiros';
				function init(){	
					function mobiDetect(userAgent, devices){
						for(var i = 0; i < devices.length; i++) {
							if (userAgent.search(devices[i]) > 0) {
								return true;
							}
						}
						return false;
					}
					if (mobiDetect(userAgent, devices)) {
	                    if(url_atual=="noticias"){                        
	                    }
	                    else if (url_geral == "1"){            
	                        window.location.href = url_mobile;                    
	                    }
	                    else{         
							window.location.href = url_redirect;
                        }
					}     
				}
			window.onload = init();
        </script>
    	</head>
		<body class="<?php echo $body;?>">
		    <div id="assinatura-push">
		        <a href="http://push.ag" title="Desenvolvido por PUSH Publicidade" target="_blank" rel="index">
			    <img src="<?php echo PATH_ROOT?>web-files/img/push-assinatura.png" alt="PUSH Publicidade" title="PUSH Publicidade">
			</a>
		    </div>
<?php endif;?>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLBTPPV"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
    <div id="bg-alpha"></div>
        <div id="box_popup">
            <div id="header_popup">
                <p>Entrar em contato com a Big</p>
                <div id="close_popup">X</div>                    
            </div>
            <div id="popup_telefone">
                <!--<h1>(11) 4200-2287-->
                <!--    <p>ligue agora para vendas</p>-->
                <!--</h1>-->
                <!--<h1 class="bolinha-vermelha">(11) 2090-0301-->
                <!--    <p>ligue para outros assuntos</p>-->
                <!--</h1>-->
                <h1>(11) 2090-0301</h1>
                <!-- <p id="funcionamento">
                    Horário de atendimento:<br>
                    Segunda a Sexta, das 8 às 17h30
                </p> -->
                <h2>(11) 94244-3587
                    <p>WhatsApp</p>
                </h2>
                <p id="funcionamento2">
                    Horário de atendimento:<br>
                    Segunda a Sexta, das 8 às 17h30
                </p>
            </div>
            <div id="popup_nosligamos">
                <h1>Ligamos para você
                    <p>Informe seu nome e número<br>e aguarde o nosso retorno.</p>
                </h1>
                <input type="text" id="campo_nome_nosligamos" placeholder="Digite seu nome"/><br>
                <input type="text" id="campo_tel_nosligamos" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode)))
                                                    return true;
                                                else
                                                    return false;" placeholder="99 99999999"/>
                <button id="bt_nosligamos">LIGUE-ME</button>
                <p id="funcionamento">
                    Horário de atendimento:<br>
                    Segunda a Sexta, das 8 às 17h30
                </p>
            </div>
            <div id="popup_financiamento">
                <img src="<?php echo PATH_ROOT?>web-files/img/logo_caixa_mcmv.jpg"/>
                <p><strong>Minha Casa Minha Vida</strong><br><br>O programa do Governo Federal, Minha Casa Minha Vida, é uma solução para facilitar a compra
                    de seu apartamento da Big Construtora e realizar o sonho da casa própria.</p>
                <a href="<?php echo URL_FINANCIA;?>" target="_blank" id="bt_simula_popup">SIMULAR<br>FINANCIAMENTO</a>
                <a href="http://www.caixa.gov.br/voce/habitacao/minha-casa-minha-vida/urbana/Paginas/default.aspx" target="_blank" id="bt_info_popup">MAIS<br>INFORMAÇÕES</a>
            </div>
            <div id="popup_areacliente"> 
                <img src="<?php echo PATH_ROOT?>web-files/img/logo_big2_border.jpg"/>
                <p><strong>Aguarde</strong><br><br>O acesso estará disponível em breve.
                    </p>
            </div>
            <div id="popup_showroom">
                <div id="mapa_showroom"></div>
                <p>Avenida Brasil, 765, Centro, Ferraz de Vasconcelos, SP<br>Próximo à Estação CPTM Ferraz de Vasconcelos</p>
            </div>
            
        </div>
    
    
       