        <?php if($body=="home" || $body=="empreendimentos" || $body=="financia"):
    if(isset($view_projetos)): 
        if( sizeof($view_projetos)>1):
    ?>
<div id="banner_slider">
    <div id="barra_progresso"></div>
    <div id="banner_slider_b">
     <?php foreach ($view_projetos as $lista_prj):
        switch($lista_prj['estagio']){
        case 1:$estagio = "Pré-lançamento";
            break;
        case 2:$estagio = "Lançamento";
            break;
        case 3:$estagio = "Em construção";
            break;
        case 4:$estagio = "Pronto para morar";
            break;
        }
         $titulo =  explode("<t>", $lista_prj["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
        $variavel0 = strtolower(preg_replace("/([^a-zA-Z0-9-])/i", "-", strtr(utf8_decode(trim($titulo_longo)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"), "aaaaeeiooouuncAAAAEEIOOOUUNC-")));?>
    <div class="slides" style="background-image: url('<?php echo PATH_ROOT?>web-files/img/imgprojeto/<?php echo $lista_prj['banner']?>')" onclick="carrega_interna('<?php echo "c/".$lista_prj['id']."/n/".$variavel0?>')">
        <div class="box_legenda_destaque">
            <div class="borda_legenda" style="background-color: <?php echo $lista_prj['paleta_header']?>"></div>
                    <p class="estagio_prj_h"><?php echo $estagio?></p>
                    <h1 class="titulo_legenda_destaque_h"><?php echo $titulo_curto?></h1>
                    <p class="subtitulo_destaque_h" style="color: <?php echo $lista_prj['paleta_subtitulo']?>"><?php echo $lista_prj['subtitulo']?></p>
                    <p class="itens_destaque_h"><?php echo $lista_prj['tipoprj_nome']?><br><?php echo ($lista_prj['dormitorio']==1)?$lista_prj['dormitorio']." Dorm":$lista_prj['dormitorio']." Dorms"?><br><?php echo $lista_prj['area']?>m²</p>
        </div>
    </div>
     <?php endforeach;?>
    </div>
    <div id="prev_slide"><img src="<?php echo PATH_ROOT?>web-files/img/prev_slider.jpg" id="bt_prev"/><div class="nav_prev_h"><img src="<?php echo PATH_ROOT?>web-files/img/seta_hover_prev.jpg"/></div></div>
    <div id="next_slide"><img src="<?php echo PATH_ROOT?>web-files/img/next_slider.jpg" id="bt_next"/><div class="nav_next_h"><img src="<?php echo PATH_ROOT?>web-files/img/seta_hover_next.jpg"/></div></div>
</div>
<?php else:   
    ?> 
<div id="banner_slider">
    <?php foreach ($view_projetos as $lista_prj):
        switch($lista_prj['estagio']){
        case 1:$estagio = "Pré-lançamento";
            break;
        case 2:$estagio = "Lançamento";
            break;
        case 3:$estagio = "Em construção";
            break;
        case 4:$estagio = "Pronto para morar";
            break;
        }
        $titulo =  explode("<t>", $lista_prj["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
        $variavel0 = strtolower(preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($titulo_longo)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"), "aaaaeeiooouuncAAAAEEIOOOUUNC-")));?>
    <div class="slides" style="background-image: url('<?php echo PATH_ROOT?>web-files/img/imgprojeto/<?php echo $lista_prj['banner']?>')" onclick="carrega_interna('<?php echo "c/".$lista_prj['id']."/n/".$variavel0?>')">
        <div class="box_legenda_destaque">
            <div class="borda_legenda" style="background-color: <?php echo $lista_prj['paleta_header']?>"></div>
                    <p class="estagio_prj_h"><?php echo $estagio?></p>
                    <h1 class="titulo_legenda_destaque_h"><?php echo $titulo_curto?></h1>
                    <p class="subtitulo_destaque_h" style="color: <?php echo $lista_prj['paleta_subtitulo']?>"><?php echo $lista_prj['subtitulo']?></p>
                    <p class="itens_destaque_h"><?php echo $lista_prj['tipoprj_nome']?><br><?php echo ($lista_prj['dormitorio']==1)?$lista_prj['dormitorio']." Dorm":$lista_prj['dormitorio']." Dorms"?><br><?php echo $lista_prj['area']?>m²</p>
        </div>
    </div>
     <?php endforeach;?>
</div>
<?php endif; 
endif;
elseif($body=="projetos"):?>
<div id="banner_slider">
    <div class="slides" style="background-image: url('<?php echo PATH_ROOT?>web-files/img/imgprojeto/<?php echo $banner;?>')"></div>
</div>
<?php elseif($body=="noticias"):?>
<div id="banner_slider">
    <div class="slides" style="background-image: url('<?php echo PATH_ROOT?>web-files/img/banner_noticias.jpg')"></div>
</div>
<?php else:?>
<div id="banner_slider">
    <div class="slides" style="background-image: url('<?php echo PATH_ROOT?>web-files/img/banner_default.jpg')"></div>
</div>
<?php endif;?>
<div id="area_search">  
    <div id="content_search">
        <div class="input-append" id="box_search">
            <input id="search_proj" type="text">
            <button class="btn" type="button" id="bt_search">Pesquisar</button>
        </div>
    </div>
</div>

