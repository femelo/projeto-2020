<div id="header">
    <div id="content_header">
        <a href="<?php echo PATH_ROOT;?>" id="logo">
            <img src="<?php echo PATH_ROOT?>web-files/img/logo_big.jpg"/>
        </a>
        <h1 id="assinatura_logo">Construindo seu grande sonho.</h1>
        <div id="menu_header">
            <div class="item_menu_header" id="item_chat"><div><span>CHAT</span></div></div>
            <div class="item_menu_header" id="item_mail"><div><span>E-MAIL</span></div></div>
            <div class="item_menu_header" id="item_telefone"><div><span>TELEFONE</span></div></div>
            <div class="item_menu_header" id="item_ligamos"><div><span>NÓS LIGAMOS</span></div></div>
        </div>
    </div>
</div>
<div id="menu_principal">
    <div class="navbar">
              <ul class="nav_menu nav">
                <li <?php echo ($body=="home")? "class='ativo'":"";?>><a href="<?php echo PATH_ROOT;?>">Home</a></li>
                <li <?php echo ($body=="empreendimentos")? "class='ativo'":"";?>><a href="<?php echo PATH_ROOT;?>empreendimentos/">Empreendimentos</a></li> 
                <li <?php echo ($body=="sobre")? "class='ativo'":"";?>><a href="<?php echo PATH_ROOT;?>sobre/">Sobre a Big</a></li>
                <li <?php echo ($body=="financia")? "class='ativo'":"";?>><a href="<?php echo PATH_ROOT;?>financiamentos/">Financiamentos</a></li>               
                <li <?php echo ($body=="noticias")? "class='ativo'":"";?>><a href="<?php echo PATH_ROOT;?>noticias/">Notícias</a></li>
                <li <?php echo ($body=="visite")? "class='ativo'":"";?>><a href="<?php echo PATH_ROOT;?>localizacao/">Visite-nos</a></li>
                <li><a href="<?php echo AREA_CLIENTE;?>" id="bt_area_cliente" target="_blank">Área do Cliente</a></li>
                <li <?php echo ($body=="contato")? "class='ativo'":"";?>><a href="<?php echo PATH_ROOT;?>contato/" id="last_item">Contato</a></li>
              </ul>          
        </div><!-- /.navbar -->
</div>
