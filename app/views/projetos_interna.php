<?php
if(empty($view_proj)){
    header('Location: http://www.bigconstrutora.com.br/');
}

$body = "projetos";
//$cor1="#567d33";
//$cor2 = "#0a9110";
//$cor3 = "#5cff64";
function montaData($valor){
    $item = explode("-", $valor);
    return $result = $item[1]."/".$item[0];
}

foreach($view_proj as $prj){
    $id = $prj['id'];
     $titulo =  explode("<t>", $prj["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }
    $titleUrl = strtolower(preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($titulo_longo)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"), "aaaaeeiooouuncAAAAEEIOOOUUNC-")));
    $sub = $prj['subtitulo'];
    $area = $prj['area'];
    $dorms = $prj['dormitorio'];
    $endereco = $prj['endereco'];
    $lazer = $prj['lazer'];
    $inicio = montaData($prj['inicio']);
    $entrega = montaData($prj['entrega']);
    $descricao =$prj['descricao'];
    $chamada = explode("<t>",$prj['apresentacao']);
    $titulo_present = $chamada[0];
    $txt_present = $chamada[1];
    $txt_obs = $chamada[2];
    $tipo = $prj['tipoprj_nome'];
    switch($prj['estagio']){
        case 1:$status = "Pré-lançamento";
            break;
        case 2:$status = "Lançamento";
            break;
        case 3:$status = "Em construção";
            break;
        case 4:$status = "Pronto para morar";
            break;
    }
    $corTitulo = $prj['paleta_tituloprj'];
    $corSub = $prj['paleta_subtitulo'];
    $corActive = $prj['paleta_active'];
    $corHeader = $prj['paleta_header'];
    $corIcon = $prj['paleta_iconbg'];
    $corFinance = $prj['paleta_financiamento'];
    $corBG = $prj['paleta_bgprj'];
    $destaque = $prj['img_destaque'];
    $banner = $prj['banner'];
    $bg = $prj['bg'];
    $showroom = $prj['showroom'];
    $tourvirtual = $prj['tourvirtual'];
}
$title = " | ".$titulo_longo;
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral" id="interna_prj">
    
    <div id="container_interna_prj2">
        <div id="nav_guide" class="topo">
            <div id="box_guias">
                <div id="nav_lazer" class="nav_box"><img src="<?php echo PATH_ROOT?>web-files/img/navicon_lazer.png"/></div>
                <div id="nav_ficha" class="nav_box"><img src="<?php echo PATH_ROOT?>web-files/img/navicon_ficha.png"/></div>
                <div id="nav_galeria" class="nav_box"><img src="<?php echo PATH_ROOT?>web-files/img/navicon_galeria.png"/></div>
                <div id="nav_mapa" class="nav_box"><img src="<?php echo PATH_ROOT?>web-files/img/navicon_mapa.png"/></div>
                <div id="nav_obras" class="nav_box"><img src="<?php echo PATH_ROOT?>web-files/img/navicon_obras.png"/></div>
                <div id="nav_final" class="nav_box"><img src="<?php echo PATH_ROOT?>web-files/img/navicon_final.png"/></div>
            </div>
        </div>
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                <a href="<?php echo PATH_ROOT?>empreendimentos">Empreendimentos</a> >
                <?php echo $sub;?>
            </p>
            <h1 id="titulo_interna_prj"><?php echo $titulo_longo;?></h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/empreendimentos/imovel/c/<?php echo $id?>/n/<?php echo $titleUrl;?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/empreendimentos/imovel/c/<?php echo $id?>/n/<?php echo $titleUrl;?>" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div>
        <div id="box_destaque_interna" class="navigation">
            <div id="destaque_projeto">
                <img src="<?php echo PATH_ROOT?>web-files/img/imgprojeto/<?php echo $destaque;?>"/>
                <div id="legenda_destaque">
                    <p id="estagio_prj"><?php echo $status;?></p>
                    <div class="titulo-box-destaque">
                        <h1 id="titulo_legenda_destaque"><?php echo $titulo_curto?></h1>
                        <p id="subtitulo_destaque"><?php echo $sub;?></p>
                    </div>
                    <p id="itens_destaque"><?php echo $tipo?><br><?php echo ($dorms==1)?$dorms." Dorm":$dorms." Dorms";?><br><?php echo $area?>m²</p>
                </div>
            </div>
            <div id="info_destaque">
                <div id="info1" class="box_info">
                    <div class="interna_info">  
                        <h3 class="txtsmall_infobox">DATA DE ENTREGA*</h3>
                        <h1 class="txt_infobox"><?php echo ($entrega=="12/1969")?"EM BREVE":$entrega;?></h1>
                    </div>
                </div>
                <div id="info2" class="box_info">
                    <div class="interna_info">
                        <h1 class="txt_infobox">FINANCIAMENTOS</h1>
                        <?php if($id== 4):?>
                            <h3 class="txtsmall_infobox">CAIXA</h3>
                        <?php else:?>
                            <h3 class="txtsmall_infobox">MINHA CASA MINHA VIDA</h3>
                        <?php endif;?>
                    </div>
                </div>
                <div id="info3" class="box_info">
                    <div class="interna_info">
                        <h1 class="txt_infobox">ATENDIMENTO ON-LINE</h1>
                        <h3 class="txtsmall_infobox">ENVIE SUA DÚVIDA</h3>
                    </div>
                </div>
            </div>
        </div>
        <div id="itens_lazer" >
            <div id="box_descricao">
                <h1 id="chamada_desc"><?php echo $titulo_present;?></h1>
                <p id="txt_desc"><?php echo $txt_present;?></p>
                <?php if($showroom == "s"):?>
                    <div id="showroom-present">
                        <div id="blc-showroom2">
                            <h2>VISITE O SHOWROOM</h2>
                            <h3>Conheça Apto Decorado</h3>
                            <p>Avenida Brasil, 765, Centro, Ferraz de Vasconcelos, SP</p>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div id="box_lazer">
                <h1 id="titulo_boxlazer">Itens de lazer</h1>
                <ul class="container_lista_lazer">
                    <?php
                    if($lazer==""){
                        echo "<li>Em breve</li>";
                    }else{
                    $itens = explode(",",$lazer); 
                    $contador_item=0;
                    foreach($view_lazer as $item_lazer):
                        
                        for($i=0;$i < sizeof($itens);$i++):
                            if($contador_item<1){
                                $close_linha="";
                            }
                            else{
                                $close_linha='</ul><ul class="container_lista_lazer">';
                            }
                            if($itens[$i]== $item_lazer['lazer_id']):
                                $contador_item++;
                        ?>
                    <li>
                        <img src="<?php echo PATH_ROOT?>web-files/img/ico_lazer/<?php echo $item_lazer['lazer_img']?>"/>
                        <span><?php echo $item_lazer['lazer_titulo']?></span>
                    </li>
                    <?php echo $close_linha; 
                    if($contador_item==2){$contador_item=0;}
                    endif; 
                    endfor; 
                    endforeach;
                    }?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="box_ficha" class="navigation">
            <div class="header_interna">
                <div class="icone_header"><img src="<?php echo PATH_ROOT?>web-files/img/icon_ficha.png"/></div>
                <h1 class="titulo_header">Ficha Técnica</h1>
            </div>
            <div id="content_ficha">
                <ul id="lista_ficha">                   
                    <?php if(sizeof($view_ficha)==0):
                            echo "<li>Em breve</li>";
                    else:
                    foreach($view_ficha as $lista_ficha):
                        
                        ?>
                    <li><strong><?php echo $lista_ficha['item_nome']?>:</strong> <?php echo $lista_ficha['item_valor']?></li>
                    <?php endforeach;
                   endif;
                    ?>
                    
                </ul>
                <div id="desc_ficha"><?php echo $descricao;?></div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div id="galeria_img" class="navigation">
            <div class="header_interna">
                <div class="icone_header"><img src="<?php echo PATH_ROOT?>web-files/img/icon_galeria.png"/></div>
                <h1 class="titulo_header">Galeria de Imagens</h1>
                <div class="icone_header" id="icon_sel_tipoimg"><img src="<?php echo PATH_ROOT?>web-files/img/icon_tipoimg.png"/></div>
                <select id="tipo_img_sel">
                    <option value="1">Perspectivas</option>
                    <option value="2">Plantas</option>
                    <option value="3">Decorado</option>
                    <option value="4">Implantação</option>
                </select>
            </div>
            <div id="box_destaque_galeria">
                <div id="lista_galeria">
                    <?php $foco_img = 0; 
                    foreach($view_galeria as $galeriaImg):
                    
                    ?>
                    <img src="<?php echo PATH_ROOT?>web-files/img/<?php echo $galeriaImg["img_caminho"]?>" class="<?php echo ($foco_img==0)?"leg foco tipo".$galeriaImg['img_tipo']:"tipo".$galeriaImg['img_tipo'];?>" alt="<?php echo $galeriaImg['img_legenda']?>"/>
                    <?php $foco_img++; 
                    endforeach;?>
                </div>
                <div id="box_legenda_galeria"><p></p></div>
                <div id="bt_legenda"></div>                    
               <!-- <a id="bt_full" data-init="fullscreen" href="#" class="mbox" data-type="image"></a>-->
               <a id="bt_full" href="javascript:void(0)"></a>
                <?php foreach($view_galeria as $galerialinks):?>
                <a class="mbox full_escondido" data-init="fullscreen" data-type="image" href="<?php echo PATH_ROOT."web-files/img/".$galerialinks["img_caminho"]?>"></a>
                <?php endforeach;?>
            </div>
            <div id="boxthumbs_list">
                    <div id="nav_prev_galeria"></div>                    
                    <div id="nav_next_galeria"></div>
                    <ul id="list_thumbs">
                        <?php foreach($view_galeria as $thumbs_galeria):?>
                        <li class="tipo<?php echo $thumbs_galeria['img_tipo']?>"><img src="<?php echo PATH_ROOT?>web-files/img/<?php echo $thumbs_galeria["img_caminho"]?>" alt="" /></li>
                        <?php endforeach;?>
                
                    </ul>
            </div>
        </div>
        <div id="mapa" class="navigation">
            <div class="header_interna">
                <div class="icone_header" id="reset_mapa"><img src="<?php echo PATH_ROOT?>web-files/img/icon_local.png"/></div>
                <h1 class="titulo_header">Localização do Empreendimento</h1>
                <p id="endereco_mapa"><?php echo $endereco;?></p>
                <div class="icone_header" id="icon_tols_sel"></div>
                <select id="tols_sel">
                    <option value="1">Rota</option>
                    <option value="2">Proximidades</option>
                </select>
            </div>
            <div id="area_mapa_interna">
            
            </div>
            <div id="box_rota">
                <h1 class="titulo_header">Como chegar</h1><br>
                <form class="" id="form-route" method="post" action="">
                    <div class="control-group">
                      <label class="control-label" for="origem">Origem</label>
                      <div class="controls">
                        <input type="text" id="origem" placeholder="origem">
                      </div>
                    </div>
                    <input type="submit" class="btn bt_tracarota" value="traçar rota">
                </form>
                <?php if($showroom == "s"):?>
                    <div id="showroom-local">
                        <div id="blc-showroom">
                            <h2>VISITE O SHOWROOM</h2>
                            <h3>Conheça Apto Decorado</h3>
                            <p>Avenida Brasil, 765, Centro,<br>Ferraz de Vasconcelos, SP</p>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div id="box_proximidade">
                <ul>
                    <li rel="1"><img src="<?php echo PATH_ROOT?>web-files/img/icon_lazer.png"/><span class="legenda_tool">LAZER E ESPORTE</span> <em class="icon-white"></em></li>
                    <li rel="2"><img src="<?php echo PATH_ROOT?>web-files/img/icon_gastro.png"/><span class="legenda_tool">GASTRONOMIA</span> <em class="icon-white"></em></li>
                    <li rel="3"><img src="<?php echo PATH_ROOT?>web-files/img/icon_educacao.png"/><span class="legenda_tool">EDUCAÇÃO</span> <em class="icon-white"></em></li>
                    <li rel="4"><img src="<?php echo PATH_ROOT?>web-files/img/icon_saude.png"/><span class="legenda_tool">SAÚDE</span> <em class="icon-white"></em></li>
                    <li rel="5"><img src="<?php echo PATH_ROOT?>web-files/img/icon_comodidade.png"/><span class="legenda_tool">COMODIDADE</span> <em class="icon-white"></em></li>
                    <li rel="6"><img src="<?php echo PATH_ROOT?>web-files/img/icon_bancos.png"/><span class="legenda_tool">BANCOS E FINANCEIRAS</span> <em class="icon-white"></em></li>
                </ul>
            </div>
        </div>        
        <div id="obra" class="navigation">
            <div class="header_interna">
                <div class="icone_header"><img src="<?php echo PATH_ROOT?>web-files/img/icon_obra.png"/></div>
                <h1 class="titulo_header">Acompanhamento de Obra</h1>
                <?php if(sizeof($view_modulos)!=0):?>
                    <select id="obras_modulos">
                        <?php foreach($view_modulos as $modulos):?>
                            <option value="<?php echo $modulos['id_modulo'];?>"><?php echo $modulos['nome_modulo'];?></option>
                        <?php endforeach;?>
                    </select>
                    <select id="obras_sel_md"></select>
                <?php else:?>
                    <select id="obras_sel">
                    <?php foreach($view_data_obra as $datas):?>
                    <option value="<?php echo montaData($datas['img_data']);?>" data-modulo="<?php echo $datas['modulo_id']?>"><?php echo montaData($datas['img_data']);?></option>
                    <?php endforeach;?>
                </select>
            <?php endif;?>
            </div>
            <div id="slide_obra">
                 <?php $foco_obra = 0; 
                 if(sizeof($view_img_obras)==0):?>
                     <img src="<?php echo PATH_ROOT?>web-files/img/obra_default.jpg" class='foco' alt=""/>
                 <?php else:
                    foreach($view_img_obras as $obrasImg):?>
                    <img src="<?php echo PATH_ROOT?>web-files/img/<?php echo $obrasImg["img_caminho"]?>" <?php echo ($foco_obra==0)?"class='foco'":"";?> alt="" rel="<?php echo montadata($obrasImg['img_data']);?>" data-modulo="<?php echo $obrasImg['modulo_id']?>"/>
                    <?php $foco_obra++; 
                    endforeach;
                    endif;
                    ?>
            </div>
            <div id="andamentoA">
             
            <div id="andamento">
                <div id="seta_andamento"></div>
                <?php foreach($view_info_status as $status):?>
                <ul class="modulo-<?php echo $status['modulo_id']?>">
                    <li class="titulo_status">MOVIMENTO DE TERRA</li>
                    <li class="barra_status"><p class="porcentagem"><?php echo $status['status_mov_terra']?>%</p><span style="width:<?php echo $status['status_mov_terra']?>% "></span></li>
                    <li class="titulo_status">FUNDAÇÕES</li>
                    <li class="barra_status"><p class="porcentagem"><?php echo $status['status_fundacoes']?>%</p><span style="width:<?php echo $status['status_fundacoes']?>% "></span></li>
                    <li class="titulo_status">ESTRUTURA E PAREDES</li>
                    <li class="barra_status"><p class="porcentagem"><?php echo $status['status_estrut_parede']?>%</p><span style="width:<?php echo $status['status_estrut_parede']?>% "></span></li>
                    <li class="titulo_status">ACABAMENTOS E PINTURAS</li>
                    <li class="barra_status"><p class="porcentagem"><?php echo $status['status_acab_pint']?>%</p><span style="width:<?php echo $status['status_acab_pint']?>% "></span></li>
                    <li class="titulo_status">INSTALAÇÕES</li>
                    <li class="barra_status"><p class="porcentagem"><?php echo $status['status_instalacoes']?>%</p><span style="width:<?php echo $status['status_instalacoes']?>% "></span></li>
                    <li class="titulo_status">ÁREAS EXTERNAS</li>
                    <li class="barra_status"><p class="porcentagem"><?php echo $status['status_area_ext']?>%</p><span style="width:<?php echo $status['status_area_ext']?>% "></span></li>
                </ul>
                <?php endforeach;?>
            </div>
            
            </div>
            <div id="thumbs_obra">
                <div id="area_thumb_obra2">
                    <div id="area_thumb_obra">
                    <?php foreach($view_img_obras as $obrasThumb):?>
                        <img src="<?php echo PATH_ROOT?>web-files/img/<?php echo $obrasThumb['img_caminho']?>" width="108px" rel="<?php echo montadata($obrasThumb['img_data']);?>" data-modulo="<?php echo $obrasThumb['modulo_id']?>"/>
                    <?php endforeach;?>
                </div>
                </div>
                <div id="data_entrega">
                    <?php if(sizeof($view_modulos)!=0):?>
                        <?php foreach($view_modulos as $modulos):?>
                            <p class="titulo_data1" data-modulo="<?php echo $modulos['id_modulo']?>">DATA DE INÍCIO<br>
                                <span><?php echo ( montaData($modulos['inicio'])=="12/1969")?"EM BREVE":montaData($modulos['inicio']);?></span>
                            </p>
                            <p class="titulo_data2" data-modulo="<?php echo $modulos['id_modulo']?>">DATA DE ENTREGA*<br>
                                <span><?php echo (montaData($modulos['entrega'])=="12/1969")?"EM BREVE":montaData($modulos['entrega']);?></span>
                            </p>    
                    <?php endforeach;
                    else:?>    
                    <p class="titulo_data1">DATA DE INÍCIO<br>
                        <span><?php echo ($inicio=="12/1969")?"EM BREVE":$inicio;?></span>
                    </p>
                    <p class="titulo_data2">DATA DE ENTREGA*<br>
                        <span><?php echo ($entrega=="12/1969")?"EM BREVE":$entrega;?></span>
                    </p>
                <?php endif;?> 
                </div>
            </div>
        </div>
        <div id="bloco_final_interna">
        <div id="bt_topo"></div>
        <div id="bloco_cadastro">
            <p id="txt_cadastro">Realize já o seu sonho de morar aqui<br>agende uma visita</p>
            <form method="post" action="" name="form_interna" id="form_interna">
                <input type="hidden" value="<?php echo $titulo_longo;?>" id="titulo_infointerna"/>
                <input type="text" placeholder="seu nome" id="nome_infointerna"/>
                <input type="text" placeholder="seu e-mail" id="mail_infointerna"/>
                <input type="submit" value="OK" class="btn btn-inverse" id="bt_infointerna"/>
            </form>
        </div>
    </div>
        <p id="texto_asterisco"><?php echo $txt_obs;?></p>
    </div>
</div>
<?php
@include FOOTER;
 foreach($view_conf_map as $map_info){
    $latitude = $map_info['mapa_latitude'];
    $longitude = $map_info['mapa_longitude'];
    $icone = $map_info['mapa_thumb'];
}
?>
<script>    
    var map;
    var map2;
    var markers=[];
    var directionsService = new google.maps.DirectionsService();
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({    
        icon: '<?php echo PATH_ROOT."web-files/img/ico_mapa/pin_big_pequeno.png"?>',
        position: new google.maps.LatLng(<?php echo $latitude.', '.$longitude;?>)
        
    });
    var markerloja = new google.maps.Marker({
        icon:'<?php echo PATH_ROOT?>web-files/img/pin_big_pequeno.png',
        position: new google.maps.LatLng("-23.537434", "-46.362449")
    });
    function setAllMap(map){
        for(var i =0;i<markers.length;i++){
            markers[i].setMap(map);
        }
    }
    function geraPonto(valor){
        initialize();
        setAllMap(null);
        var request2 = {
            location: marker.position,
            radius: 4000,
            types: [valor]
        };
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request2, callback);
    }
    function callback(results, status2) {
        if (status2 == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                createMarker(results[i]);
            }
        }
    }
    function createMarker(place) {
        var marker2 = new google.maps.Marker({
            icon: '<?php echo PATH_ROOT."web-files/img/pin_cinza.png"?>',
            map: map,
            position: place.geometry.location
        });
        markers.push(marker2);
        google.maps.event.addListener(marker2, 'click', function() {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    }
    function initialize() { 
        var options = {
            zoom: 15,
            center: marker.position,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };    
        map = new google.maps.Map(document.getElementById("area_mapa_interna"), options);
    
        marker.setMap(map);   
    }
    function initialize2() {    
        var options = {
            zoom: 15,
            center: markerloja.position,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };    
        map2 = new google.maps.Map(document.getElementById("mapa_showroom"), options);
    
        markerloja.setMap(map2);   
    }

    google.maps.event.addDomListener(window, 'load', initialize);
    google.maps.event.addDomListener(window, 'load', initialize2);

    function titlePopup(texto){
        $("#header_popup > p").html(texto);
    }
    function montaData(valor){
        var item = valor.split("-");
        return item[1]+"/"+item[0];
    }

    $(document).ready(function(){
           
        $("#reset_mapa").click(function(){
            initialize();
        });

        var valor = $("#tipo_img_sel").val();      
        var qtd_galeria =  $("#list_thumbs .tipo"+valor+"").length;   
        var qtdareas = Math.ceil(qtd_galeria/5);
        var areavisivel = $("#boxthumbs_list").width()-64;
        $("#list_thumbs").width(areavisivel*qtdareas);
        var contadorPag=1;

        $("#nav_next_galeria").click(function(){
            var valor = $("#tipo_img_sel").val();      
            var qtd_galeria =  $("#list_thumbs .tipo"+valor+"").length;   
            var qtdareas = Math.ceil(qtd_galeria/5);
            var areavisivel = $("#boxthumbs_list").width()-64;
            $("#list_thumbs").width(areavisivel*qtdareas);
            if(contadorPag < qtdareas){
                contadorPag++;
                var pos_atual =$("#list_thumbs").css("left").replace("px","");
                TweenLite.to("#list_thumbs", 1, {'left':(pos_atual - areavisivel), ease: Expo.easeInOut});
            }
        });

        $("#nav_prev_galeria").click(function(){
            var valor = $("#tipo_img_sel").val();      
            var qtd_galeria =  $("#list_thumbs .tipo"+valor+"").length;   
            var qtdareas = Math.ceil(qtd_galeria/5);
            var areavisivel = $("#boxthumbs_list").width()-64;
            $("#list_thumbs").width(areavisivel*qtdareas);
            if(contadorPag >1){
                contadorPag--;
                var pos_atual =$("#list_thumbs").css("left").replace("px","");               
                var calculo =parseInt(pos_atual) + parseInt(areavisivel);
                TweenLite.to("#list_thumbs", 1, {'left':calculo, ease: Expo.easeInOut});
            }       
        });

        $(".nav_box").click(function(){          
            var indice = $(this).index(".nav_box");
            $(".nav_box").removeClass("ativobg");
            $(this).addClass("ativobg");
            if(indice == 5){
                var container = $('body');
                var local =container.offset();
                $('html,body').animate({scrollTop:local.top-118},600);
            }else{
                var container = $('.navigation').eq(indice);
                var local =container.offset();
                $('html,body').animate({scrollTop:local.top-118},600);
            }
        });

        $(window).scroll(function(){
            if($(this).scrollTop() > 590){
                var posicao = $("#nav_guide").offset().left;             
                $("#box_guias").css("left",posicao+"px").addClass("guiafixo");
            }       
            if($(this).scrollTop() < 590){
                $("#box_guias").css("left","0px").removeClass("guiafixo").removeClass("final");
            }
            if($(window).scrollTop()> $("#footer").offset().top-700){
                $("#box_guias").css("left","0px").removeClass("guiafixo").addClass("final");
            }
            if($(window).scrollTop()>= $(".navigation").eq(0).offset().top-118){
                $(".nav_box").removeClass("ativobg");
                $(".nav_box").eq(0).addClass("ativobg");
            }
            if($(window).scrollTop()>= $(".navigation").eq(1).offset().top-118){
                $(".nav_box").removeClass("ativobg");
                $(".nav_box").eq(1).addClass("ativobg");
            }
            if($(window).scrollTop()>= $(".navigation").eq(2).offset().top-118){
                $(".nav_box").removeClass("ativobg");
                $(".nav_box").eq(2).addClass("ativobg");
            }
            if($(window).scrollTop()>= $(".navigation").eq(3).offset().top-118){
                $(".nav_box").removeClass("ativobg");
                $(".nav_box").eq(3).addClass("ativobg");
            }
            if($(window).scrollTop()>= $(".navigation").eq(4).offset().top-118){
                $(".nav_box").removeClass("ativobg");
                $(".nav_box").eq(4).addClass("ativobg");
            }
        });

        $(window).load(function(){
            $("#area_thumb_obra2").mCustomScrollbar({
                scrollButtons: {
                    enable: true
                }
            });
        });
       
        $("#list_thumbs > li >img").click(function(){
            var idImg = $(this).index("#list_thumbs > li >img");
            $("#lista_galeria img").hide();
            var imgClicada = $("#lista_galeria  img").eq(idImg);
            var legendclicada = imgClicada.attr("alt");
           
            $("#lista_galeria img").removeClass("leg");
            imgClicada.addClass("leg");
            $("#box_legenda_galeria p").html(legendclicada);
            imgClicada.fadeIn(); 
        });
       
        $("#area_thumb_obra >img").click(function(){
            var idImg = $(this).index("#area_thumb_obra >img");
            $("#slide_obra img").hide();
            var imgClicada = $("#slide_obra  img").eq(idImg);
            imgClicada.fadeIn();
        });
       
        $("#bt_full").click(function(){
            var valorImg = $("#lista_galeria .leg").attr("src");
            var lista_botoes = $(".full_escondido").length;
          
            for(var i=0;i < lista_botoes;i++){
                if($(".full_escondido").eq(i).attr("href") == valorImg){
                    $(".full_escondido").eq(i).trigger("click");
                }
            }
        });

        var toggleLegenda=true;
        $("#bt_legenda").click(function(){
            toggleLegenda=!toggleLegenda;
            if(toggleLegenda){
                TweenLite.to("#box_legenda_galeria", 1, {'height': 0, ease: Expo.easeInOut});
                $("#box_legenda_galeria > p").hide();
            }else{
                TweenLite.to("#box_legenda_galeria", 1, {'height': 44, ease: Expo.easeInOut});
                $("#box_legenda_galeria > p").show(500);
                var texto_legenda = $(".leg").attr("alt");
                $("#box_legenda_galeria p").html(texto_legenda);
            }
        });
        $("#bg-alpha").css("height",$(document).height());
        <?php if($id != 4):?> 
        $("#info2").click(function(){
            var posicao = $(window).scrollTop();
            $("#bg-alpha").show();
            $("#box_popup").css("top",(posicao+120)+"px").show();
            $("#popup_nosligamos,#popup_telefone,#popup_areacliente,#popup_showroom").hide();
            $("#popup_financiamento").show();
            titlePopup("Financiamentos");
        });
        <?php endif;?>
        $("#showroom-local,#showroom-present").click(function(){
            var posicao = $(window).scrollTop();
            $("#bg-alpha").show();
            $("#box_popup").css("top",(posicao+120)+"px").show();
            $("#popup_nosligamos,#popup_telefone,#popup_areacliente,#popup_financiamento").hide();
            $("#popup_showroom").show();
            titlePopup("SHOWROOM - Visite apto decorado");
            var markerloja = new google.maps.Marker({
                icon:'<?php echo PATH_ROOT?>web-files/img/pin_big_pequeno.png',
                position: new google.maps.LatLng("-23.537434", "-46.362449")
            });
            var options = {
                zoom: 15,
                center: markerloja.position,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };    
            map2 = new google.maps.Map(document.getElementById("mapa_showroom"), options);
            markerloja.setMap(map2); 
        });
        $("#info3").click(function(){
            ga('send', 'event', 'button', 'click', 'bt_chat_interna');
            // $zopim(function(){
            //     $zopim.livechat.button.hide();
            // });
            chat();
        });
        
        //gera selects na pagina
        if($("#obras_sel").length >0){
            $("#obras_sel").selectbox();
        }
        if($("#obras_modulos").length >0){
            $("#obras_modulos").selectbox();
            $("#obras_modulos").next('.sbHolder').addClass("obras_modulos");
            var id_projeto = "<?php echo $id?>";
            var modulo_selecionado = $("#obras_modulos option:selected").val();
            $(".titulo_data1,.titulo_data2").hide();
            $(".titulo_data1").each(function(){ 
                if($(this).data("modulo")==modulo_selecionado){
                    $(this).show();
                }
            });
            $(".titulo_data2").each(function(){ 
                if($(this).data("modulo")==modulo_selecionado){
                    $(this).show();
                }
            });
            $("#andamento ul").hide();
            $("#andamento .modulo-"+modulo_selecionado).show();
            var urlDirect = "<?php echo PATH_ROOT?>empreendimentos/imovel/ajax/modulo";

            $.ajax({                                            
                type:"post",
                url:urlDirect,
                data:{modulo_selecionado:modulo_selecionado,id_projeto:id_projeto},
                success:function(data){
                    var dados = JSON.parse(data);
                    var valor = "";
                    for(var i=0;i < dados.length;i++){
                        valor +="<option value='"+montaData(dados[i].img_data)+"'>"+montaData(dados[i].img_data)+"</option>";
                    }
                    $("#obras_sel_md").html(valor);
                    $("#obras_sel_md").selectbox();
                    $("#obras_sel_md").next('.sbHolder').addClass("data_modulos");
                    var data_selecionada = $("#obras_sel_md option:selected").val();
                    $("#area_thumb_obra img").each(function(){
                        if($(this).attr('rel')== data_selecionada && $(this).data('modulo')== modulo_selecionado){
                            $(this).show();
                        }
                    });
                    $("#slide_obra img").each(function(){
                        if($(this).attr('rel')== data_selecionada && $(this).data('modulo')== modulo_selecionado){
                            $(this).show();
                        }
                    });
                    var selectobras = $(".data_modulos .sbOptions");
                    var botao_data = selectobras.find("a");

                    botao_data.click(function(){
                        $("#area_thumb_obra img,#slide_obra img").hide();
                        var valor = $(this).text();
                        $("#area_thumb_obra img").each(function(){
                            if($(this).attr('rel')== valor && $(this).data('modulo')== modulo_selecionado){
                                $(this).show();
                            }
                        });
                        $("#slide_obra img").each(function(){
                            if($(this).attr('rel')== valor && $(this).data('modulo')== modulo_selecionado){
                                $(this).show();
                            }
                        });
                        $("#area_thumb_obra2").mCustomScrollbar("update");
                    });
                }
            });

            var selectmodulos = $(".obras_modulos .sbOptions");
            var botao_modulos = selectmodulos.find("a");
            botao_modulos.click(function(){
                var id_projeto = "<?php echo $id?>";
                $("#area_thumb_obra img,#slide_obra img").hide();
                var modulo_selecionado = $(this).attr('rel');
                $("#andamento ul").hide();
                $("#andamento .modulo-"+modulo_selecionado).show();
                $(".titulo_data1,.titulo_data2").hide();
                $(".titulo_data1").each(function(){ 
                    if($(this).data("modulo")==modulo_selecionado){
                        $(this).show();
                    }
                });
                $(".titulo_data2").each(function(){ 
                    if($(this).data("modulo")==modulo_selecionado){
                        $(this).show();
                    }
                });
                var urlDirect = "<?php echo PATH_ROOT?>empreendimentos/imovel/ajax/modulo";
                $.ajax({                                            
                    type:"post",
                    url:urlDirect,
                    data:{modulo_selecionado:modulo_selecionado,id_projeto:id_projeto},
                    success:function(data){
                        var dados = JSON.parse(data);
                        var valor = "";
                        for(var i=0;i < dados.length;i++){
                            valor +="<option value='"+montaData(dados[i].img_data)+"'>"+montaData(dados[i].img_data)+"</option>";
                        }
                        $("#obras_sel_md").selectbox("detach");
                        $("#obras_sel_md").html(valor);
                        $("#obras_sel_md").selectbox("attach");
                        $("#obras_sel_md").next('.sbHolder').addClass("data_modulos");
                        var data_selecionada = $("#obras_sel_md option:selected").val();
                        $("#area_thumb_obra img").each(function(){
                            if($(this).attr('rel')== data_selecionada && $(this).data('modulo')== modulo_selecionado){
                                $(this).show();
                            }
                        });
                        $("#slide_obra img").each(function(){
                            if($(this).attr('rel')== data_selecionada && $(this).data('modulo')== modulo_selecionado){
                                $(this).show();
                            }
                        });
                        var selectobras = $(".data_modulos .sbOptions");
                        var botao_data = selectobras.find("a");

                        botao_data.click(function(){
                            $("#area_thumb_obra img,#slide_obra img").hide();
                            var valor = $(this).text();
                            $("#area_thumb_obra img").each(function(){
                                if($(this).attr('rel')== valor && $(this).data('modulo')== modulo_selecionado){
                                    $(this).show();
                                }
                            });
                            $("#slide_obra img").each(function(){
                                if($(this).attr('rel')== valor && $(this).data('modulo')== modulo_selecionado){
                                    $(this).show();
                                }
                            });
                            $("#area_thumb_obra2").mCustomScrollbar("update");
                        });
                    }
                });        
            });
        }

        $("#tipo_img_sel,#tols_sel").selectbox();
        //

        
        
        
        $("#box_proximidade > ul > li").click(function(){
            var linha_tol  = $(this).attr("rel");
            var icone = $(this).children("em");
            $(".icon-white").removeClass("icon-ok");
            icone.addClass("icon-ok");
            if(linha_tol =="1"){
                geraPonto('amusement_park','art_gallery','museum','stadium,zoo','park,shopping_mall');
            }
            else if(linha_tol =="2"){
                    geraPonto('food','meal_delivery','resturant');
            }
            else if(linha_tol =="3"){
                geraPonto('school','university');
            }
            else if(linha_tol=="4"){
                geraPonto('hospital','pharmacy','health','doctor','dentist');
            }
            else if(linha_tol=="5"){
                geraPonto('bar','cafe,meal_delivery');
            }
            else if(linha_tol=="6"){
                geraPonto('bank','finace');
            }
        });
        $("#form-route").submit(function(e){
            e.preventDefault();
            setAllMap(null);
            marker.setMap(null);
            var directionsDisplay = new google.maps.DirectionsRenderer();
            
            var request = {
                origin: $("#origem").val(),
                destination: marker.position,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request,function(response,status){
                if(status == google.maps.DirectionsStatus.OK){
                   directionsDisplay.setDirections(response);
                   directionsDisplay.setMap(map);
                }
            });      
        });

        var selectgaleria = $(".sbOptions").eq(0);
        var ativaperspectiva = selectgaleria.find("a").eq(0);
        var ativaplantas = selectgaleria.find("a").eq(1);
        var ativadecorado = selectgaleria.find("a").eq(2);
        var ativaimplantacao = selectgaleria.find("a").eq(3);
        $(".tipo1").eq(0).show();
        if($(".tipo1").length==0){
            $("#lista_galeria").append("<img src='<?php echo PATH_ROOT?>web-files/img/galeria_default.jpg' class='tipo1'>");
        }

        ativaperspectiva.click(function(){ 
            $(".tipo1").show();
            if($(".tipo1").length==0){
                $("#lista_galeria").append("<img src='<?php echo PATH_ROOT?>web-files/img/galeria_default.jpg' class='tipo1'>");
            }
            $("#list_thumbs").css("left","32px");
            contadorPag=1;
            $("#lista_galeria .tipo1").hide();
            $("#lista_galeria .tipo1").eq(0).show();
            $(".tipo1").eq(0).addClass("leg"); 
            $(".tipo2,.tipo3,.tipo4").removeClass("leg");
             var texto_legenda = $(".tipo1").eq(0).attr("alt");
             $("#box_legenda_galeria p").html(texto_legenda);
            $(".tipo2,.tipo3,.tipo4").hide();
             var qtd_galeria =  $("#list_thumbs .tipo1").length;      
             var areavisivel = $("#boxthumbs_list").width()-64;
             var qtdareas = Math.ceil(qtd_galeria/5);
             $("#list_thumbs").width(areavisivel*qtdareas);
        });
        ativaplantas.click(function(){
            $(".tipo2").show();
            if($(".tipo2").length==0){
                $("#lista_galeria").append("<img src='<?php echo PATH_ROOT?>web-files/img/galeria_default.jpg' class='tipo2'>");
            }
            contadorPag=1;
            $("#list_thumbs").css("left","32px");
            $("#lista_galeria .tipo2").hide();
            $("#lista_galeria .tipo2").eq(0).show();
            $(".tipo2").eq(0).addClass("leg"); 
            $(".tipo1,.tipo3,.tipo4").removeClass("leg");
            var texto_legenda = $(".tipo2").eq(0).attr("alt");
            $("#box_legenda_galeria p").html(texto_legenda);
            $(".tipo1,.tipo3,.tipo4").hide();          
            var qtd_galeria =  $("#list_thumbs .tipo2").length;   
            var areavisivel = $("#boxthumbs_list").width()-64;
            var qtdareas = Math.ceil(qtd_galeria/5);
            $("#list_thumbs").width(areavisivel*qtdareas);
        });
        ativadecorado.click(function(){
            $(".tipo3").show();
            if($(".tipo3").length==0){
                <?php if($tourvirtual=="s"):    
                $protocolo = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === false) ? 'http://' : 'https://';
                ?>
                $("#lista_galeria").append("<a href='<?php echo $protocolo.$_SERVER['HTTP_HOST']."/tourvirtual/".str_replace(" ","",$titleUrl)."/tourvirtual.html";?>' target='_blank'><img src='<?php echo PATH_ROOT?>web-files/img/galeria_tourvitual.jpg' class='tipo3' id='bt_tour'></a>");
                <?php else:?>
                $("#lista_galeria").append("<img src='<?php echo PATH_ROOT?>web-files/img/galeria_default.jpg' class='tipo3'>");
                <?php endif;?>  
            }
            $("#list_thumbs").css("left","32px");
            contadorPag=1;
            $("#lista_galeria .tipo3").hide();
            $("#lista_galeria .tipo3").eq(0).show();
            $(".tipo3").eq(0).addClass("leg"); 
            $(".tipo1,.tipo2,.tipo4").removeClass("leg");
            var texto_legenda = $(".tipo3").eq(0).attr("alt");
            $("#box_legenda_galeria p").html(texto_legenda);
            $(".tipo2,.tipo1,.tipo4").hide();
            var qtd_galeria =  $("#list_thumbs .tipo3").length;  
            var areavisivel = $("#boxthumbs_list").width()-64;
            var qtdareas = Math.ceil(qtd_galeria/5);
            $("#list_thumbs").width(areavisivel*qtdareas);
        });
        ativaimplantacao.click(function(){
            $(".tipo4").show();
            if($(".tipo4").length==0){
                $("#lista_galeria").append("<img src='<?php echo PATH_ROOT?>web-files/img/galeria_default.jpg' class='tipo4'>");
            }
            $("#list_thumbs").css("left","32px");
            contadorPag=1;
            $("#lista_galeria .tipo4").hide();
            $("#lista_galeria .tipo4").eq(0).show();
            $(".tipo4").eq(0).addClass("leg"); 
            $(".tipo1,.tipo2,.tipo3").removeClass("leg");
            var texto_legenda = $(".tipo4").eq(0).attr("alt");             
            $("#box_legenda_galeria p").html(texto_legenda);
            $(".tipo2,.tipo3,.tipo1").hide();
            var qtd_galeria =  $("#list_thumbs .tipo4").length;
            var areavisivel = $("#boxthumbs_list").width()-64;
            var qtdareas = Math.ceil(qtd_galeria/5);
            $("#list_thumbs").width(areavisivel*qtdareas);
        });

        var selectmapa = $(".sbOptions").eq(1);
        var ativa_rota = selectmapa.find("a").eq(0);
        var ativa_proximidade = selectmapa.find("a").eq(1);

        ativa_rota.click(function(){
            $("#icon_tols_sel").css("background-image","url('<?php echo PATH_ROOT?>web-files/img/icon_rota.png')");
            $("#box_rota").show();
            $("#box_proximidade").hide();
        });
        ativa_proximidade.click(function(){
            $("#icon_tols_sel").css("background-image","url('<?php echo PATH_ROOT?>web-files/img/icon_proximidade.png')");
             $("#box_rota").hide();
             $("#box_proximidade").show();
        });

        var flag_andamento =false;
        $("#seta_andamento").click(function(){
            flag_andamento=!flag_andamento;
            if(flag_andamento){
                TweenLite.to("#andamento", 1, {'left': -243, ease: Expo.easeInOut});
                $(this).css("background-image","url('<?php echo PATH_ROOT?>web-files/img/seta_andamento_dir.jpg')");
            }else{
                TweenLite.to("#andamento", 1, {'left': 0, ease: Expo.easeInOut});
                $(this).css("background-image","url('<?php echo PATH_ROOT?>web-files/img/seta_andamento_esq.jpg')");
            }
        });
        if($("#obras_sel").length > 0){
            var data_selecionada = $("#obras_sel option:selected").val();
        
            $("#area_thumb_obra img[rel='"+data_selecionada+"'],#slide_obra img[rel='"+data_selecionada+"']").show();
            var selectobras = $(".sbOptions").eq(2);
            var botao_data = selectobras.find("a");

            botao_data.click(function(){
                $("#area_thumb_obra img,#slide_obra img").hide();
                var valor = $(this).text();
                $("#area_thumb_obra img[rel='"+valor+"'],#slide_obra img[rel='"+valor+"']").show();
                $("#area_thumb_obra2").mCustomScrollbar("update");
            });
        }

        $("#bt_infointerna").click(function(e){
            e.preventDefault();
           var nome_info =  $("#nome_infointerna").val().trim();
           var email_info = $("#mail_infointerna").val().trim();
           var titulo = $("#titulo_infointerna").val();
           
           var urlDirect = "<?php echo PATH_ROOT?>contato/infointerna/";
           if(nome_info==""){
               alert("Digite seu nome");
           }
           else if(email_info=="" || email_info.indexOf('@')== -1 || email_info.indexOf('.')== -1){
               alert("Digite um email válido");
           }
           else{
                $.ajax({                                            
                    type:"post",
                    url:urlDirect,
                    data:{nome_info:nome_info,email_info:email_info,titulo:titulo},
                    success:function(data){
                        if(data == "ok"){
                            <?php if($view_conversao =="2"):?>
                                       
                            <?php endif;?>
                           
                            ga('send', 'event', 'formulario', 'envio', 'ok_form_interna');
                            alert("Enviado com sucesso");
                            document.form_interna.reset();
                        }
                        else{
                            alert("Erron ao enviar");
                        }
                    }
                });
           }
        });
        function reajusteGuia(){
             if($(this).scrollTop() > 590){
                var posicao = $("#nav_guide").offset().left;
                $("#box_guias").css("left",posicao+"px").addClass("guiafixo");
             }
       }
       $(window).resize(reajusteGuia);
   });
</script>