<?php
$title = "";
$body = "home";
$cor3 = "#5cff64";
@include HEADER;
@include MENU;
@include BANNER_SEARCH;
$estagio1 = 0;
$estagio2 = 0;
$estagio3 = 0;
$estagio4 = 0;
$estagioall = 0;
foreach($view_projeto as $estagio){
    $estagioall++;
    if($estagio['estagio']=="1"){
        $estagio1++;
    }elseif ($estagio['estagio']=="2") {
        $estagio2++;
    }elseif ($estagio['estagio']=="3") {
        $estagio3++;
    }elseif ($estagio['estagio']=="4") {
        $estagio4++;
    }
}
?>
<div class="content_geral">
    <div id="bloco_prj">
        <ul id="box_filtro">
            <li>Todos <span class="badge badge-quadrado"><?php echo $estagioall;?></span></li>
            <li>Pré-lançamento <span class="badge badge-quadrado"><?php echo $estagio1;?></span></li>
            <li>Lançamento <span class="badge badge-quadrado"><?php echo $estagio2;?></span></li>
            <li>Em construção <span class="badge badge-quadrado"><?php echo $estagio3;?></span></li>
            <li>Pronto para Morar <span class="badge badge-quadrado"><?php echo $estagio4;?></span></li>
        </ul>
        <?php foreach($view_projeto as $thumbs):
            
            $titulo =  explode("<t>", $thumbs["titulo"]);
            $titulo_longo = $titulo[0]." ".$titulo[1];
            if($titulo[1]!=""){
            $titulo_curto = $titulo[1];
            }else{
                $titulo_curto = $titulo[0];
            }

            $sem_acento =  trim(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $titulo_longo ) ));

            $variavel_1 = strtolower(preg_replace('/\s/', '-', $sem_acento));?>
        <a class="container_projeto estagio<?php echo $thumbs['estagio']?>" href="<?php echo PATH_ROOT."empreendimentos/imovel/c/".$thumbs['id']."/n/".$variavel_1?>">
            <div class="box_thumb">
                <img class="thumb_projeto" src="<?php echo PATH_ROOT?>web-files/img/imgprojeto/<?php echo $thumbs['thumb']?>"/>
                <div class="legenda_thumb_prj">
                    <h1 class="titulo_prj_th"><?php echo $titulo_curto;?><br><?php echo $thumbs['subtitulo']?></h1>
                    <p class="ficha_th"><?php echo $thumbs['tipoprj_nome']?><br><?php echo ($thumbs['dormitorio']==1)?$thumbs['dormitorio']." Dorm":$thumbs['dormitorio']." Dorms"?><br>Área: <?php echo $thumbs['area']?>m²</p>
                    <p class="bt_th">CLIQUE E CONHEÇA</p>
                </div>
            </div>
        </a>
        <?php endforeach;?>
    </div>
    <div id="bloco_info">
        <div id="container_news">
            <h1 class="titulo_info">Notícias e eventos</h1>
            <iframe src="http://bigconstrutora.com.br/blog/?page_id=9" width="630" height="408" seamless frameborder="0" scrolling="no"></iframe>

        </div>
        <div id="sidebar_info">
            <h1 class="titulo_info">Área do cliente</h1>
            <div id="area_cliente">
				<a href="<?php echo AREA_CLIENTE;?>" target="_blank">
					<img alt="Área do Cliente" title="Área do Cliente" src="<?php echo PATH_ROOT?>web-files/img/bt_area_cliente.gif"/>
				</a>
            </div>
            <div id="area_financiamento">
                <a href="<?php echo PATH_ROOT?>financiamentos">
                    <div id="titulo_mcmv"><img src="<?php echo PATH_ROOT?>web-files/img/icon_mcmv.jpg"/><span>Financiamentos</span></div>
                    <img src="<?php echo PATH_ROOT?>web-files/img/img_mcmv.jpg"/>
                </a>
            </div>
        </div>
    </div>
    <div id="bloco_final">
        <div id="bt_topo"></div>
        <div id="bloco_cadastro">
            <p id="txt_cadastro">Cadastre-se e fique por dentro das novidades<br>e lançamentos da Big Construtora</p>
            <form method="post" action="" name="form_home" id="form_home">
                <input type="text" placeholder="seu nome" id="nome_cadastrohome"/>
                <input type="text" placeholder="seu e-mail" id="mail_cadastrohome"/>
                <input type="submit" value="OK" class="btn btn-inverse" id="btn_cadastrohome"/>
            </form>
        </div>
    </div>
</div>
<?php @include FOOTER;?>
<script>
    
    function carrega_interna(caminho){
        document.location.href="<?php echo PATH_ROOT?>empreendimentos/imovel/"+caminho;
    }
    $(document).ready(function(){
       /* var filtro1_badge = $("#box_filtro > li .badge").eq(0).html();
        var filtro2_badge = $("#box_filtro > li .badge").eq(1).html();
        var filtro3_badge = $("#box_filtro > li .badge").eq(2).html();
        var filtro4_badge = $("#box_filtro > li .badge").eq(3).html();
        if(filtro1_badge =="0"){
            if(filtro2_badge =="0"){
                if(filtro3_badge=="0"){
                    if(filtro4_badge=="0"){                       
                    }else{
                        $("#box_filtro > li").eq(3).addClass("filtrado filtro_hover");
                        $("#box_filtro > li .badge").eq(3).addClass("badge-important");
                    }
                }else{
                    $("#box_filtro > li").eq(2).addClass("filtrado filtro_hover");
                    $("#box_filtro > li .badge").eq(2).addClass("badge-important");
                }
            }else{$("#box_filtro > li").eq(1).addClass("filtrado filtro_hover");$("#box_filtro > li .badge").eq(1).addClass("badge-important");}
        }else{
            $("#box_filtro > li").eq(0).addClass("filtrado filtro_hover");
            $("#box_filtro > li .badge").eq(0).addClass("badge-important");
        }*/
        $("#btn_cadastrohome").click(function(e){
            e.preventDefault();
           var nome_news =  $("#nome_cadastrohome").val().trim();
           var email_news = $("#mail_cadastrohome").val().trim();
           console.log(nome_news);
           var urlDirect = "<?php echo PATH_ROOT?>contato/news/";
           if(nome_news==""){
               alert("Digite seu nome");
           }
           else if(email_news=="" || email_news.indexOf('@')== -1 || email_news.indexOf('.')== -1){
               alert("Digite um email válido");
           }
           else{
               $.ajax({                        					
		    type:"post",
		    url:urlDirect,
                    data:{nome_news:nome_news,email_news:email_news},
                    success:function(data){
                        if(data == "ok"){
                          hc_envia_mensagem(0, nome_news, email_news,'', '','');
                          ga('send', 'event', 'formulario', 'envio', 'ok_form_newsletter_home');
                            alert("Enviado com sucesso");
                            document.form_home.reset();
                        }
                        else{
                            alert("Erron ao enviar");
                        }
                    }
                    });
           }
        });
        $("#bt_topo").click(function(){
            var container = $('body');
            var local =container.offset();
            $('html,body').animate({scrollTop:local.top-118},600);
        });
        $(".container_projeto").mouseover(function(){
            var bloco = $(this).children(".box_thumb");
            var legenda = bloco.children(".legenda_thumb_prj");
            TweenLite.to(bloco, 0.5, {'height':"319px"});
            TweenLite.to(legenda, 0.5, {'top':"120px"});
        });
        $(".container_projeto").mouseout(function(){
            var bloco = $(this).children(".box_thumb");
            var legenda = bloco.children(".legenda_thumb_prj");
            TweenLite.to(bloco, 0.5, {'height':"241px"});
            TweenLite.to(legenda, 0.5, {'top':"175px"});
        });
        $("#box_filtro li").mouseover(function(){                               
            $(this).children('.badge-quadrado').addClass('badge-important');
        });
        $("#box_filtro li").mouseout(function(){                        
            if($(this).hasClass("filtrado")){                
            }else{
                $(this).children('.badge-quadrado').removeClass('badge-important');
            }
        });
        $("#box_filtro li").click(function(){
            var badge = $(this).children("span.badge").text();
            
           if(badge==0){}else{
            var indiceFiltro = $(this).index("#box_filtro li");
            $(".container_projeto").hide();
            if(indiceFiltro==0){
                $(".estagio1,.estagio2,.estagio3,.estagio4").show();
            }else if(indiceFiltro==1){
                $(".estagio1").show();
            }else if(indiceFiltro==2){
                $(".estagio2").show();
            }else if(indiceFiltro==3){
                $(".estagio3").show();
            }
            else if(indiceFiltro==4){
                $(".estagio4").show();
            }
            $("#box_filtro li").removeClass('filtrado');
            $(this).addClass("filtrado");
            $("#box_filtro li").removeClass('filtro_hover');
            $("#box_filtro li .badge-quadrado").removeClass('badge-important');
            $(this).addClass('filtro_hover');
             $(this).children('.badge-quadrado').addClass('badge-important');
         }  
        });
      /*======== FUNÇÕES SLIDER =========*/ 
       $(".slides").eq(0).clone().appendTo("#banner_slider_b");
       var qtd = $(".slides").length;
       var contador=1;      
       $(window).resize(tamanho);
       tamanho();
    
       var pare = false;
       $(".slides").removeClass("next");
       $(".slides").removeClass("prev");
       $(".slides").eq(contador).addClass("next");
    
       $(".slides").eq(qtd-2).addClass("prev");
  
     
       $("#prev_slide").click(function(e){
          if(pare){
            e.preventDefault();
          } else{
            pare=true;
            var posicao = $("#banner_slider_b").css('left').replace("px", "");
            var tamanho_atual = $("#banner_slider").width();
            if(posicao==0 && contador==1){
                contador=qtd-1;
                var calculo = (qtd-1)*tamanho_atual;
                $("#banner_slider_b").css('left',-calculo);
                var posicao_atual=$("#banner_slider_b").css('left').replace("px", "");
                var calc_final = parseInt(posicao_atual)+parseInt(tamanho_atual);
                TweenLite.to("#banner_slider_b", 1, {'left': calc_final, ease: Expo.easeInOut,onComplete:desab});
            
                $(".slides").removeClass("next");
                $(".slides").removeClass("next");
                $(".slides").removeClass("prev");
                $(".slides").eq(contador).addClass("next");
                $(".slides").eq(contador-2).addClass("prev");
            }else{
                contador--;
                var posicao_atual=$("#banner_slider_b").css('left').replace("px", "");
                var calc_final = parseInt(posicao_atual)+parseInt(tamanho_atual);
                TweenLite.to("#banner_slider_b", 1, {'left': calc_final, ease: Expo.easeInOut,onComplete:desab});
                $(".slides").removeClass("next");
                $(".slides").removeClass("prev");
                $(".slides").eq(contador).addClass("next");
                if(contador!=1){
                    $(".slides").eq(contador-2).addClass("prev");
                }else{
                    $(".slides").eq(qtd-2).addClass("prev");
                }                
              }
            }
            $(".nav_prev_h").css({display:"none"});
        });
      
       $("#next_slide").click(function(e){
            if (pare) {
               e.preventDefault();
            } else {
               pare=true;
               contador++;         
               var posicao = $("#banner_slider_b").css('left').replace("px", "");        
               var tamanho_atual = $("#banner_slider").width();        
               TweenLite.to("#banner_slider_b", 1, {'left': (posicao-tamanho_atual), ease: Expo.easeInOut,onComplete:zera});
               $(".slides").removeClass("next");
               $(".slides").removeClass("prev");
               $(".slides").eq(contador).addClass("next");
               $(".slides").eq(contador-2).addClass("prev");
                $(this).children(".nav_next_h").css("display","none");
            }
        });
      
       $("#prev_slide").mouseover(function(){
            var img = $("#banner_slider_b").find(".prev").css("background-image");
            $("#bt_prev").css("display","none");
            $(".nav_prev_h").css({display:"block",backgroundImage:img});
        }); 
       $("#prev_slide").mouseout(function(){ 
            $(".nav_prev_h").css({display:"none"});       
            $("#bt_prev").css("display","block");           
        });
    
       $("#next_slide").hover(
            function(){
               $(this).children("#bt_next").css("display","none");
               var img = $("#banner_slider_b").find(".next").css("background-image");           
               $(".nav_next_h").css({display:"block",backgroundImage:img});
        }, 
            function(){ 
              $(this).children(".nav_next_h").css("display","none");
              $(this).children("#bt_next").css("display","block");
        });
      var tween = TweenLite.to("#barra_progresso", 15, {'width':"100%",ease:Linear.easeNone,onComplete:disparaProgress});
       function disparaProgress(){
        $("#barra_progresso").css("width","0%");
        $("#next_slide").trigger("click");
        tween.restart();
        }
       function desab(){
          pare=false; 
        }
       function zera(){
          pare=false;
          if(contador==qtd){
            contador=1;
            $("#banner_slider_b").css('left',"0");
            $(".slides").removeClass("next");
            $(".slides").removeClass("prev");
            $(".slides").eq(contador).addClass("next");
            $(".slides").eq(qtd-2).addClass("prev");
          } 
        }
       function tamanho(){
           var larguramedia =$("#content_header").css("left");
           $(".box_legenda_destaque").css("left",larguramedia);
           var tamanho = $("#banner_slider").width();
           for(var i=0; i<qtd; i++){
              $(".slides").eq(i).css("width",tamanho);
           }
           $("#banner_slider_b").css("width",tamanho* qtd);
           if(contador!=1){
               var calc_resize = (contador-1)*tamanho;
               $("#banner_slider_b").css("left",-calc_resize);
           }
        }   
       
       /*======== FIM FUNÇÕES SLIDER =========*/ 
       
   });   
</script>


