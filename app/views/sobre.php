<?php
$title = " | Sobre a Big";
$body = "sobre";
@include HEADER;?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    !function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};

    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<?php @include MENU;
@include BANNER_SEARCH;?>
<div class="content_geral">
    <div id="container_interna_prj">
        <div id="header_titulo_prj">
            <p id="breadcrumb_prj"><a href="<?php echo PATH_ROOT?>">Home</a> > 
                Sobre a Big
            </p>
            <h1 id="titulo_interna_prj">Sobre a Big</h1>
            <img src="<?php echo PATH_ROOT?>web-files/img/divisor_title_interna.jpg" id="divisor_interna"/>
            <div id="line_likes">
                <div class="fb-like" data-href="http://www.bigconstrutora.com.br/sobre/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <div class="g-plusone gplus"  data-width="300" data-size="medium"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.bigconstrutora.com.br/sobre/" data-via="" data-lang="pt">Tweetar</a>               
            </div>
        </div> 
        <div id="bloco_chamada_destaque">
            <h1 id="txt_chamada_destaque">Big Construtora<br>Construindo o seu grande sonho.</h1>
        </div>
        <div id="container_sobre">
        <div class="sidebar_esq">
            <h3 class="header_sidebar_esq">Sobre a Big</h3>
            <div id="filtro_estagio">
                <a href="#missao" class="ancora_sobre">Missão</a>
                <a href="#visao" class="ancora_sobre">Visão</a>
                <a href="#valores" class="ancora_sobre">Valores</a>
            </div>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>sobre/certificacoes">Certificações</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>sobre/qualidade">Política da Qualidade Big</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>noticias/">Notícias</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>localizacao/">Localização</a>
            <a class="titulo_filtros link_sidebar" href="<?php echo PATH_ROOT?>contato/">Fale Conosco</a>
           
        </div>
        <div id="content_sobre">
            <p class="desc_sobre">A <strong>Big Construtora e Incorporadora S/A</strong>, surgiu do sonho de oferecer ao público 
                                empreendimentos diferenciados com qualidade e preço justo. Foi com o 
                                ingresso no Programa do Governo Federal <strong>MCMV</strong> (Minha Casa, Minha Vida), 
                                parceria com investidores, bancos e agentes de financiamento que esse sonho 
                                tornou-se realidade.
            </p>
            <img src="<?php echo PATH_ROOT?>web-files/img/destaque_sobre.jpg"/>
            <p class="desc_sobre">Desde sua fundação em 2008 a Big Construtora e Incorporadora desenvolveu,
                                entre unidades concluídas, em construção e em lançamento, <strong>962 unidades 
                                    habitacionais</strong> na cidade de São Paulo, nos bairros do Tremembé (Zona Norte) e 
                                Tatuapé (Zona Leste) e na cidade vizinha de Ferraz de Vasconcelos, nos bairros 
                                do Cambiri e Vila São Paulo.<br><br>
                                Contamos com uma equipe de profissionais comprometidos e altamente 
                                capacitados tecnicamente nas áreas: administrativa, incorporação, vendas, 
                                jurídica, correspondente bancário, engenharia e construção, que acompanham e dão 
                                suporte desde a aquisição do terreno, financiamento, até o período pós-entrega final do imóvel ao cliente.<br><br>
                                A <strong>Big Construtora</strong> faz uso de um sitema integrado de gestão que visa a interligação 
                                de todos os processos da empresa, com o objetivo de gerar agilidade, confiabilidade e transparência nas informações, sistema esse 
                                reconhecido com a obtenção das certificações: Programa de Qualidade e 
                                Produtividade no Habitat, <strong>PBQP-H nível A e ISO 9001:2015</strong>.
            </p>            
            <h3 class="subtitulo_sobre" id="missao">Missão</h3>
            <p class="desc_sub">Satisfazer aos anseios dos nossos clientes, colaboradores e acionistas, com
muito respeito e transparência nas escolhas e decisões.</p>
            <h3 class="subtitulo_sobre" id="visao">Visão</h3>
            <p class="desc_sub">Ocupar um lugar de destaque no mercado imobiliário e ser reconhecida
por garantir qualidade de vida aos nossos clientes, desenvolver nossos 
colaboradores e gerar rentabilidade para nossos acionistas.</p>
            <h3 class="subtitulo_sobre" id="valores">Valores</h3>
            <p class="desc_sub">Ética, comprometimento e respeito à vida e ao meio ambiente.</p>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
@include FOOTER;
?>
<script>         
   $(document).ready(function(){               

   });
</script>
