<?php

class ProjetoModel extends Model{
     public $_tabela = "projetos";
    public function listaProjetos($orderby){
        return $this->read("ativo = 1",null,null,$orderby);
    }
    public function listaByTipo($where,$orderby){
        return $this->read($where,null,null,$orderby);
    }
    public function listaProjetosTipo(){
       $sql =  $this->db->query(
        "SELECT * FROM projetos  p 
          left join tipo_projeto  tp on p.tipo = tp.tipoprj_id
          left join dados_paleta dp on p.paleta = dp.paleta_id
          where p.ativo = 1 
          order by p.estagio desc"
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaProjetosTipo2(){
       $sql =  $this->db->query(
        "SELECT * FROM projetos p 
          left join tipo_projeto tp on p.tipo = tp.tipoprj_id 
          left join dados_paleta dp on p.paleta = dp.paleta_id 
          where p.ativo = 1
          order by p.posicao asc"
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaProjetosTipoDestaque(){
       $sql =  $this->db->query("
        SELECT * FROM projetos p 
          left join tipo_projeto  tp on p.tipo = tp.tipoprj_id
          left join dados_paleta dp on p.paleta = dp.paleta_id
          where p.flag = 's' AND p.ativo = 1 
          order by p.id desc"
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaProjetosTipoLike($valor){
       $sql =  $this->db->query(
        "SELECT * FROM projetos p
          left join tipo_projeto tp on p.tipo = tp.tipoprj_id 
          left join dados_paleta dp on p.paleta = dp.paleta_id 
          where tags like '%{$valor}%' AND p.ativo = 1 "
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaProjetosTipoLikeDE($d,$e){
       $sql =  $this->db->query(
        "SELECT * FROM projetos p 
          left join tipo_projeto tp on p.tipo = tp.tipoprj_id 
          left join dados_paleta dp on p.paleta = dp.paleta_id 
          where p.dormitorio = {$d} and p.estagio={$e} AND p.ativo = 1 
          order by p.posicao asc"
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaProjetosTipoLikeD($d){
       $sql =  $this->db->query(
        "SELECT * FROM projetos p 
          left join tipo_projeto tp on p.tipo = tp.tipoprj_id 
          left join dados_paleta dp on p.paleta = dp.paleta_id 
          where p.dormitorio = {$d} AND p.ativo = 1 
          order by p.posicao asc"
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaProjetosTipoLikeE($e){
       $sql =  $this->db->query(
        "SELECT * FROM projetos p 
          left join tipo_projeto tp on p.tipo = tp.tipoprj_id 
          left join dados_paleta dp on p.paleta = dp.paleta_id 
          where p.estagio = {$e} AND p.ativo = 1 
          order by p.posicao asc"
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaProjetosTipoByid($id){
       $sql =  $this->db->query(
        "SELECT * FROM projetos p 
          left join tipo_projeto tp on p.tipo = tp.tipoprj_id 
          left join dados_paleta dp on p.paleta = dp.paleta_id 
          where p.id = {$id} AND p.ativo = 1"
        );
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
}

