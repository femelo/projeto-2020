<?php

class StatusModel extends Model{
     public $_tabela = "status";
    public function listaStatus($where,$orderby){
        return $this->read($where,null,null,$orderby);
    }
    public function listaStatusModulo($where){
    	$sql =  $this->db->query("SELECT * FROM status left join modulos_projeto on modulo_id=id_modulo where status.projeto_id={$where} order by modulo_id desc");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        return $sql->fetchAll();
    }
    
    
}

