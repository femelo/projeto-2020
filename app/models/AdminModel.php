<?php

class AdminModel extends Model{
    
    public function listaDados($orderby){
        return $this->read(null,null,null,$orderby);
    }
    public function listaProjetosModulos(){
        $sql =  $this->db->query("SELECT * FROM projetos where fase = 2");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        return $sql->fetchAll();
    }
    public function listaProjetosJoinModulos(){
        $sql = $this->db->query("SELECT * FROM projetos left join modulos_projeto on id=projeto_id order by projeto_id");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        return $sql->fetchAll();
    }
    public function listaEstagios($oderby){
       $sql =  $this->db->query("SELECT * FROM estagios order by {$oderby}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaTipoImg($oderby){
       $sql =  $this->db->query("SELECT * FROM tipo_imagem order by {$oderby}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaTipoImgById($where){
       $sql =  $this->db->query("SELECT * FROM tipo_imagem where tipoimg_id={$where}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaTipo($oderby){
       $sql =  $this->db->query("SELECT * FROM tipo_projeto order by {$oderby}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaStatus(){
       $sql =  $this->db->query("SELECT * FROM status left join projetos on projeto_id=id left join modulos_projeto on modulo_id=id_modulo");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaModulos(){
       $sql =  $this->db->query("SELECT * FROM modulos_projeto left join projetos on projeto_id=id order by projeto_id");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaFicha(){
       $sql =  $this->db->query("SELECT * FROM itens_ficha");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaFichaByID($id){
       $sql =  $this->db->query("SELECT * FROM itens_ficha where id_projeto={$id}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaImagens(){
       $sql =  $this->db->query("SELECT * FROM imagens_prj");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaImagensById($id){
       $sql =  $this->db->query("SELECT * FROM imagens_prj where projeto_id={$id}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaDataById($id){
       $sql =  $this->db->query("SELECT DISTINCT img_data FROM imagens_prj where projeto_id={$id}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
     public function listaLazer($oderby){
       $sql =  $this->db->query("SELECT * FROM area_lazer order by {$oderby}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaPaleta($oderby){
       $sql =  $this->db->query("SELECT * FROM dados_paleta order by {$oderby}");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function dadosAtuais($id){
        return $this->read($id,null,null,null);
    }
    public function deletaDados($id){
        return $this->delete($id);
    }
    public function insereProjetos($cp1,$cp2,$cp3,$cp4,$cp5,$cp6,$cp7,$cp8,$cp9,$cp10,$cp11,$cp12,$cp13,$cp14,$cp15,$cp16,$cp17,$cp18,$cp19){
        $data = array();
        $data['titulo'] = $cp1;
        $data['subtitulo'] = $cp2;
        $data['tipo'] = $cp3;
        $data['dormitorio'] = $cp4;
        $data['area'] = $cp5;
        $data['valor'] = $cp6;
        $data['endereco'] = $cp7;
        $data['tags'] = $cp8;
        $data['lazer'] = $cp9;
        $data['inicio'] = $cp10;
        $data['entrega'] = $cp11;
        $data['descricao'] = $cp12;
        $data['apresentacao'] = $cp13;
        $data['estagio'] = $cp14;
        $data['paleta'] = $cp15;
        $data['flag'] = $cp16;
        $data['showroom'] = $cp17;
        $data['tourvirtual'] =$cp18;
        $data['fase'] =$cp19;
       return $this->insert($data);
    }
    public function alteraProjetos($cp0,$cp1,$cp2,$cp3,$cp4,$cp5,$cp6,$cp7,$cp8,$cp9,$cp10,$cp11,$cp12,$cp13,$cp14,$cp15,$cp16,$cp17,$cp18,$cp19){
        $data = array();
        $where = $cp0;
        $data['titulo'] = $cp1;
        $data['subtitulo'] = $cp2;
        $data['tipo'] = $cp3;
        $data['dormitorio'] = $cp4;
        $data['area'] = $cp5;
        $data['valor'] = $cp6;
        $data['endereco'] = $cp7;
        $data['tags'] = $cp8;
        $data['lazer'] = $cp9;
        $data['inicio'] = $cp10;
        $data['entrega'] = $cp11;
        $data['descricao'] = $cp12;
        $data['apresentacao'] = $cp13;
        $data['estagio'] = $cp14;
        $data['paleta'] = $cp15;
        $data['flag'] = $cp16;
        $data['showroom'] = $cp17;
        $data['tourvirtual'] = $cp18;
        $data['fase'] =$cp19;
       return $this->update($data, "id=".$where);
    }
    public function alteraModulo($cp1,$cp2,$cp3,$cp4){
        $data = array();
        $where = $cp4;
        $data['nome_modulo'] = $cp1;
        $data['inicio'] = $cp2;
        $data['entrega'] = $cp3;
        return $this->update($data, "id_modulo=".$where);
    }
    public function alteraStatus($cp1,$cp2,$cp3,$cp4,$cp5,$cp6,$cp7){
        $data = array();
        $where = $cp7;
        $data['status_mov_terra'] = $cp1;
        $data['status_fundacoes'] = $cp2;
        $data['status_estrut_parede'] = $cp3;
        $data['status_acab_pint'] = $cp4;
        $data['status_instalacoes'] = $cp5;
        $data['status_area_ext'] = $cp6;
        
       return $this->update($data, "status_id=".$where);
    }
    public function insereModulo($cp1,$cp2,$cp3,$cp4){
        $data = array();
        $data['nome_modulo'] = $cp1;
        $data['inicio'] = $cp2;
        $data['entrega'] = $cp3;
        $data['projeto_id'] = $cp4;
        return $this->insert($data);
    }
    public function insereTipoProjeto($cp1){
        $data = array();
        $data['tipoprj_nome'] = $cp1;
        return $this->insert($data);
    }
    public function insereFicha($cp1,$cp2,$cp3){
        $data = array();
        $data['item_nome'] = $cp1;
        $data['item_valor'] = $cp2;
        $data['id_projeto'] = $cp3;
        return $this->insert($data);
    }
    public function apagaProjeto($cp1){
        $data = array();
        $data['status'] = "projeto_id=".$cp1;
        $data['modulos_projeto'] = "projeto_id=".$cp1;
        $data['mapa'] = "projeto_id=".$cp1;
        $data['itens_ficha'] = "id_projeto=".$cp1;
        $data['imagens_prj'] = "projeto_id=".$cp1;
        $data['projetos'] = "id=".$cp1;

        return $this->deleteSeguro($data);
    }
    public function insereImgAdicional($cp1,$cp2,$cp3,$cp4,$cp5,$cp6){
        $data = array();
        $data['img_caminho'] = $cp1;
        $data['img_legenda'] = $cp2;
        $data['img_tipo'] = $cp3;
        $data['img_data'] = $cp4;
        $data['projeto_id'] = $cp5;
        $data['modulo_id'] = $cp6;
        return $this->insert($data);
    }
    public function alteraFicha($cp1,$cp2,$cp3){
        $data = array();
        $where = $cp1;
        $data['item_nome'] = $cp2;
        $data['item_valor'] = $cp3;
             
       return $this->update($data, "item_id=".$where);
    }
    public function alteraMapa($cp1,$cp2,$cp3,$cp4){
        $data = array();
        $where = $cp1;
        $data['mapa_latitude'] = $cp2;
        $data['mapa_longitude'] = $cp3;
        if($cp4!=""){
            $data['mapa_thumb'] = $cp4;
        }
             
       return $this->update($data, "mapa_id=".$where);
    }
    public function alteraImagens($cp1,$cp2,$cp3,$cp4,$cp5){
        $data = array();
        $where = $cp1;
        if($cp2!=""){
            $data['img_destaque'] = $cp2;
        }
        if($cp3!=""){
            $data['thumb'] = $cp3;
        }  
        if($cp4!=""){
            $data['banner'] = $cp4;
        }
        if($cp5!=""){
            $data['bg'] = $cp5;
        }
       return $this->update($data, "id=".$where);
    }
    public function alteraImgAdicional($cp1,$cp2,$cp3,$cp4){
        $data = array();
        $where = $cp1;
        if($cp2 != ""){
            $data['img_caminho'] = $cp2;
        }        
            $data['img_legenda'] = $cp3;  
            $data['img_data'] = $cp4;
       return $this->update($data, "img_id=".$where);
    }
    public function insertStatus(Array $dados){
        
             $campos = implode(", ", array_keys($dados));
             $valores = "'".implode("', '", array_values($dados))."'";
           
            $res =  $this->db->query("INSERT INTO status ({$campos}) VALUES({$valores})");
            return $res->rowCount();
    }
    public function insereStatus($id,$modulo){
        $data = array();
        $data['projeto_id'] = $id;
        $data['modulo_id'] = $modulo;
        return $this->insertStatus($data);
    }
    public function insertMaps(Array $dados){
        
             $campos = implode(", ", array_keys($dados));
             $valores = "'".implode("', '", array_values($dados))."'";
           
            $res =  $this->db->query("INSERT INTO mapa ({$campos}) VALUES({$valores})");
            return $res->rowCount();
    }
    public function insereMapa($id){
        $data = array();
        $data['mapa_latitude'] = "-23.548912";
        $data['mapa_longitude'] = "-46.562337";
        $data['projeto_id'] = $id;
        return $this->insertMaps($data);
    }
    public function insereAreaLazer($cp1,$cp2){
        $data = array();
        $data['lazer_titulo'] = $cp1;
        $data['lazer_img'] = $cp2;
        return $this->insert($data);
    }
    public function inserePaleta($cp1,$cp2,$cp3,$cp4,$cp5,$cp6,$cp7,$cp8,$cp9){
        $data = array();
        $data['paleta_cor'] = $cp1;
        $data['paleta_img'] = $cp2;
        $data['paleta_tituloprj'] = $cp3;
        $data['paleta_subtitulo'] = $cp4;
        $data['paleta_active'] = $cp5;
        $data['paleta_header'] = $cp6;
        $data['paleta_iconbg'] = $cp7;
        $data['paleta_financiamento'] = $cp8;
        $data['paleta_bgprj'] = $cp9;
        return $this->insert($data);
    }
    public function alteraTipoProjeto($cp1,$cp2){
        $data = array();
        $id = $cp1;
        $data['tipoprj_nome'] = $cp2;
        return $this->update($data,"tipoprj_id=".$id);
    }
    public function alteraAreaLazer($cp1,$cp2,$cp3){
        $data = array();
        $id = $cp1;
        $data['lazer_titulo'] = $cp2;
        if($cp3 !=""){
            $data['lazer_img'] = $cp3;
        }
        return $this->update($data,"lazer_id=".$id);
    }
    public function alteraPaleta($cp0,$cp1,$cp2,$cp3,$cp4,$cp5,$cp6,$cp7,$cp8,$cp9){
        $data = array();
        $id = $cp0;
        $data['paleta_cor'] = $cp1;
        if($cp2!=""){
            $data['paleta_img'] = $cp2;
        }
        $data['paleta_tituloprj'] = $cp3;
        $data['paleta_subtitulo'] = $cp4;
        $data['paleta_active'] = $cp5;
        $data['paleta_header'] = $cp6;
        $data['paleta_iconbg'] = $cp7;
        $data['paleta_financiamento'] = $cp8;
        $data['paleta_bgprj'] = $cp9;
        return $this->update($data,"paleta_id=".$id);
    }
    
}