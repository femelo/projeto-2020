<?php

class ImagensModel extends Model{
     public $_tabela = "imagens_prj";
    public function listaImagensGaleria($where,$orderby){
        return $this->read($where,null,null,$orderby);
    }
    public function listaImagensObra($where,$orderby){
        return $this->read($where,null,null,$orderby);
    }
    public function listaImagemModuloObra($where){
      $sql =  $this->db->query("SELECT * FROM imagens_prj left join modulos_projeto on modulo_id=id_modulo where imagens_prj.projeto_id={$where} and img_tipo = 5 order by img_data desc");
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      return $sql->fetchAll();
    }
    public function listaDataObra($id){
         $sql =  $this->db->query("SELECT DISTINCT img_data FROM imagens_prj where img_data <> '0000-00-00' and projeto_id={$id} order by img_data desc");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
    public function listaDataObraModulo($id,$modulo){
        $sql =  $this->db->query("SELECT DISTINCT img_data,modulo_id,inicio FROM imagens_prj left join modulos_projeto on modulo_id=id_modulo where img_data <> '0000-00-00' and imagens_prj.projeto_id={$id} and imagens_prj.modulo_id={$modulo} order by img_data desc");
       $sql->setFetchMode(PDO::FETCH_ASSOC);
       return $sql->fetchAll();
    }
}

